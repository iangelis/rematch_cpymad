# %%
from rematch_hl import *
import matplotlib.pyplot as plt
import xdeps as xd
import numpy as np

try:
    get_ipython().run_line_magic("matplotlib", "inline")
except:
    pass


def cycle_madx(mad, name="IP1"):
    mad.input(f"cycle,start={name};")


# %%
prefix = "/home/iangelis/Projects/hllhc_optics/"
# optics_filename = f"{prefix}/acc-models-lhc/strengths/round/opt_round_150_1500.madx"
optics_filename = f"{prefix}/summer_studies/opt_round_1100_1500.madx"


full_filename = os.path.abspath(optics_filename)

mad = Madx()
mad.chdir("../")

load_stuff(mad)

mad.input("""exec,mk_beam(7000);""")


# %%
mad.call(f"{full_filename}")
# %%
mad.input("exec, check_ip(b1);")
# %%
# beti = np.flip(np.arange(0.075, 0.15, 0.01))
# bet = beti[0]

bet = 1.1
mad.globals.betx_IP1 = bet
mad.globals.bety_IP1 = bet
mad.globals.betx_IP5 = bet
mad.globals.bety_IP5 = bet

bet0 = 0.9
mad.globals.betx0_IP1 = bet0
mad.globals.bety0_IP1 = bet0
mad.globals.betx0_IP5 = bet0
mad.globals.bety0_IP5 = bet0

# %%
rematch_hllhc(
    mad,
    betx_ip1=bet,
    bety_ip1=bet,
    betx_ip5=bet,
    bety_ip5=bet,
    betx0_ip1=bet0,
    bety0_ip1=bet0,
    betx0_ip5=bet0,
    bety0_ip5=bet0,
)

# %%

print_disp_tars(mad)
print_tar_irs(mad)
print_tars(mad)
print_xing15_tars(mad)

# %%
filename_out = f"rematch_cpymad/opt_test_round_{bet:0.2f}_1500.madx"
save_optics_hllhc(mad, filename_out)

# %%
