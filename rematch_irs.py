def rematch_ir15b12(
    mad,
    match_on_triplet=None,
    ir5q4sym=None,
    ir5q5sym=None,
    ir5q6sym=None,
    match_inj_tunes=None,
    no_match_beta=None,
):
    if match_on_triplet is None:
        mad.input("match_on_triplet=0;")
    else:
        mad.input(f"match_on_triplet={match_on_triplet};")

    if ir5q4sym is None:
        mad.input("ir5q4sym = 0;")
    else:
        mad.input(f"ir5q4sym = {ir5q4sym};")

    if ir5q5sym is None:
        mad.input("ir5q5sym = 0;")
    else:
        mad.input(f"ir5q5sym = {ir5q5sym};")

    if ir5q6sym is None:
        mad.input("ir5q6sym = 0;")
    else:
        mad.input(f"ir5q6sym = {ir5q6sym};")

    if match_inj_tunes is None:
        mad.input("match_inj_tunes = 0;")
    else:
        mad.input(f"match_inj_tunes = {match_inj_tunes};")

    if no_match_beta is None:
        mad.input("no_match_beta = 0;")
    else:
        mad.input(f"no_match_beta = {no_match_beta};")

    mad.input('set,format="22.15g";')
    mad.input("select,flag=twiss,clear;")
    mad.input(
        "select,flag=twiss,column=name,s,l,angle,k1l,k2l,hkick,vkick,kick,betx,bety,alfx,alfy,dx,dpx,dy,dpy,mux,muy,x,y,px,py,wx,wy,n1;"
    )
    mad.input(
        "select, flag=aperture, column=name,s,n1,aper_1,aper_2,aper_3,aper_4,rtol,xtol,ytol,betx,bety,dx,dy,x,y;"
    )

    # mad.input("!jac_calls=15; bisec=3; tol=1e-21;")

    mad.input("scale = 23348.89927;")
    mad.input("if(grad==0){ grad=132.2; };")
    mad.input("if(scl==0){ scl=0.10; };")
    mad.input("if(sch==0){ sch=0.96; };")
    mad.input("if(sc79==0){ sc79=0.99; };")
    mad.input("if(bmaxds==0){ bmaxds=500; };")
    mad.input("if(imb==0){ imb=1.30; };")

    mad.input("qtlim1 = grad/scale;")
    mad.input("qtlim2 = 160.0/scale;")
    mad.input("qtlimq5= 160.0/scale;")
    mad.input("qtlim3 = 200.0/scale;")
    mad.input("qtlim4 = 125.0/scale;")
    mad.input("qtlim5 = 120.0/scale;")
    mad.input("qtlimq4= 160.0/scale;")

    mad.input("value,qtlim1,qtlim2,qtlim3,qtlim4,qtlim5;")

    mad.input("kqx1.r5 :=-kqx1.l5;")
    mad.input("kqx2.l5  =kqx2a.l5;")
    mad.input("kqx2a.l5:=kqx2.l5;")
    mad.input("kqx2b.l5:=kqx2.l5;")
    mad.input("kqx2a.r5:=-kqx2.l5;")
    mad.input("kqx2b.r5:=-kqx2.l5;")
    mad.input("kqx3.r5 :=-kqx3.l5;")

    if mad.globals.on_holdselect == 0:
        mad.input("exec,selectIR15(5,45,56,b1);")
        mad.input("exec,selectIR15(5,45,56,b2);")

    mad.input("use,period=lhcb1,range=s.ds.l5.b1/e.ds.r5.b1;")
    mad.input("use,period=lhcb2,range=s.ds.l5.b2/e.ds.r5.b2;")

    mad.input("MATCH, sequence=lhcb1,lhcb2, beta0= bir5b1,bir5b2,mux=0,0,muy=0,0;")

    if mad.globals.no_match_beta == 0:
        mad.input("constraint,sequence=lhcb1,range=IP5,betx=betxip5b1,bety=betyip5b1;")
        mad.input("constraint,sequence=lhcb2,range=IP5,betx=betxip5b2,bety=betyip5b2;")

    mad.input("constraint,sequence=lhcb1,range=IP5,alfx=0,alfy=0;")
    mad.input("constraint,sequence=lhcb2,range=IP5,alfx=0,alfy=0;")
    mad.input("constraint,sequence=lhcb2,range=IP5,dx=0,dpx=0;")
    mad.input("constraint,sequence=lhcb1,range=IP5,dx=0.0,dpx=0.0;")

    mad.input(
        "constraint,sequence=lhcb1,range=e.ds.r5.b1,alfx=eir5b1->alfx,alfy=eir5b1->alfy;"
    )
    mad.input(
        "constraint,sequence=lhcb1,range=e.ds.r5.b1,dx=eir5b1->dx,dpx=eir5b1->dpx;"
    )
    mad.input(
        "constraint,sequence=lhcb1,range=e.ds.r5.b1,betx=eir5b1->betx,bety=eir5b1->bety;"
    )
    mad.input(
        "constraint,sequence=lhcb2,range=e.ds.r5.b2,alfx=eir5b2->alfx,alfy=eir5b2->alfy;"
    )
    mad.input(
        "constraint,sequence=lhcb2,range=e.ds.r5.b2,dx=eir5b2->dx,dpx=eir5b2->dpx;"
    )
    mad.input(
        "constraint,sequence=lhcb2,range=e.ds.r5.b2,betx=eir5b2->betx,bety=eir5b2->bety;"
    )

    if mad.globals.match_inj_tunes == 0:
        mad.input("muxIP5b1=muxIP5b1_l+muxIP5b1_r;")
        mad.input("muyIP5b1=muyIP5b1_l+muyIP5b1_r;")
        mad.input("muxIP5b2=muxIP5b2_l+muxIP5b2_r;")
        mad.input("muyIP5b2=muyIP5b2_l+muyIP5b2_r;")
        mad.input("constraint,sequence=lhcb1,range=IP5,mux=muxIP5b1_l,muy=muyIP5b1_l;")
        mad.input("constraint,sequence=lhcb2,range=IP5,mux=muxip5b2_l;")
        mad.input("constraint,sequence=lhcb2,range=IP5,muy=muyip5b2_l;")
        mad.input(
            "constraint,sequence=lhcb1,range=e.ds.r5.b1,mux=muxIP5b1,muy=muyIP5b1;"
        )
        mad.input(
            "constraint,sequence=lhcb2,range=e.ds.r5.b2,mux=muxip5b2,muy=muyip5b2;"
        )
    else:
        mad.input(
            "constraint,sequence=lhcb1,range=e.ds.r5.b1,mux=muxIP5b1,muy=muyIP5b1;"
        )
        mad.input(
            "constraint,sequence=lhcb2,range=e.ds.r5.b2,mux=muxip5b2,muy=muyip5b2;"
        )

    if mad.globals.ir5q4sym == 0:
        mad.input("imq4l=-KQ4.L5B2/KQ4.L5B1/100;")
        mad.input("imq4r=-KQ4.R5B2/KQ4.R5B1/100;")
        mad.input("KQ4.L5B2:=-KQ4.L5B1*imq4l*100;")
        mad.input("KQ4.R5B2:=-KQ4.R5B1*imq4r*100;")
        mad.input("vary,name=imq4l, UPPER=imb/100, LOWER=1/imb/100;")
        mad.input("vary,name=imq4r, UPPER=imb/100, LOWER=1/imb/100;")
        mad.input("vary,name=KQ4.L5B1, UPPER= sch*qtlimq4, LOWER= scl*qtlimq4;")
        mad.input("vary,name=KQ4.R5B1, UPPER=-scl*qtlimq4, LOWER=-sch*qtlimq4;")

    if mad.globals.ir5q5sym == 0:
        mad.input("Kq5.L5B1=Kq5.L5B1;Kq5.R5B1=Kq5.R5B1;")
        mad.input("imq5l=-KQ5.L5B2/KQ5.L5B1/100;")
        mad.input("imq5r=-KQ5.R5B2/KQ5.R5B1/100;")
        mad.input("Kq5.L5B2:=-Kq5.L5B1*imq5l*100;")
        mad.input("Kq5.R5B2:=-Kq5.R5B1*imq5r*100;")
        mad.input("vary,name=imq5l, UPPER=imb/100, LOWER=1/imb/100;")
        mad.input("vary,name=imq5r, UPPER=imb/100, LOWER=1/imb/100;")
        mad.input("vary,name=KQ5.L5B1, UPPER=-scl*qtlimq5, LOWER=-sch*qtlimq5;")
        mad.input("vary,name=KQ5.R5B1, UPPER= sch*qtlimq5, LOWER= scl*qtlimq5;")

    if mad.globals.ir5q6sym == 0:
        mad.input("imq6l=-KQ6.L5B2/KQ6.L5B1/100;")
        mad.input("imq6r=-KQ6.R5B2/KQ6.R5B1/100;")
        mad.input("Kq6.L5B2:=-Kq6.L5B1*imq6l*100;")
        mad.input("Kq6.R5B2:=-Kq6.R5B1*imq6r*100;")
        mad.input("vary,name=imq6l, UPPER=imb/100, LOWER=1/imb/100;")
        mad.input("vary,name=imq6r, UPPER=imb/100, LOWER=1/imb/100;")
        mad.input("vary,name=Kq6.L5B1, UPPER= sch*qtlim2, LOWER= scl*qtlim2;")
        mad.input("vary,name=Kq6.R5B1, UPPER=-scl*qtlim2, LOWER=-sch*qtlim2;")

    if mad.globals.match_on_triplet == 1:  # q1 q2 q3 free
        mad.input("mqxw=1000;")
        mad.input("KQX1.L5_100=KQX1.L5/mqxw;")
        mad.input("KQX2.L5_100=KQX2.L5/mqxw;")
        mad.input("KQX3.L5_100=KQX3.L5/mqxw;")
        mad.input("KQX1.L5:=KQX1.L5_100*mqxw;")
        mad.input("KQX2.L5:=KQX2.L5_100*mqxw;")
        mad.input("KQX3.L5:=KQX3.L5_100*mqxw;")
        mad.input("vary,name=KQX1.L5_100, LOWER=-qtlim1/mqxw, UPPER=-0.90*qtlim1/mqxw;")
        mad.input("vary,name=KQX2.L5_100, LOWER=-qtlim1/mqxw, UPPER=-0.90*qtlim1/mqxw;")
        mad.input("vary,name=KQX3.L5_100, LOWER=-qtlim1/mqxw, UPPER=-0.90*qtlim1/mqxw;")

    if mad.globals.match_on_triplet == 2:  # q1 q2 linked
        mad.input("!KQX1.L5:=KQX2.L5;")
        mad.input("vary,name=KQX2.L5, LOWER=-1.000*qtlim1, UPPER=-0.00*qtlim1;")

    if mad.globals.match_on_triplet == 3:  # only beta in q3 and cc region constrained
        # mad.input("!KQX1.L5:=KQX2.L5;")
        # mad.input("!vary,name=KQX1.L5, LOWER=-1.000*qtlim1,UPPER=-0.00*qtlim1;")
        mad.input("vary,name=KQX2.L5, LOWER=-1.000*qtlim1, UPPER=-0.00*qtlim1;")
        mad.input("vary,name=KQX3.L5, LOWER=-1.000*qtlim1, UPPER=-0.00*qtlim1;")
        # mad.input("!constraint,sequence=lhcb2,range=M.ACFC.CDR5.B2,betx=betx_acf;")
        # mad.input("!constraint,sequence=lhcb2,range=M.ACFC.CDR5.B2,bety=bety_acf;")
        # mad.input("!constraint,sequence=lhcb2,range=MCBYYH.4R5.B2,betx=betx_mcby;")
        # mad.input("!constraint,sequence=lhcb2,range=MCBYYH.4R5.B2,bety=bety_mcby;")
        # mad.input("!if (bety_q3>0){")
        # mad.input("!  constraint,sequence=lhcb2,range=MQXFA.B3R5,bety=bety_q3;")
        # mad.input("!};")

    if mad.globals.match_on_triplet == 4:  # q1 q2 q3 linked
        # mad.input("!KQX1.L5:=KQX2.L5;")
        mad.input("KQX3.L5:=KQX1.L5;")
        mad.input("vary,name=KQX2.L5, LOWER=-1.000*qtlim1, UPPER=-0.00*qtlim1;")
        mad.input("vary,name=KQX1.L5, LOWER=-1.000*qtlim1, UPPER=-0.00*qtlim1;")

    if mad.globals.match_on_triplet == 5:  # only beta in q3 and cc region constrained
        # mad.input("!vary,name=KQX1.L5    ,LOWER=-1.000*qtlim1,UPPER=-0.00*qtlim1;")
        mad.input("vary,name=KQX2.L5, LOWER=-1.000*qtlim1, UPPER=-0.00*qtlim1;")
        mad.input("vary,name=KQX3.L5, LOWER=-1.000*qtlim1, UPPER=-0.00*qtlim1;")
        mad.input(
            "if(betx_acf>0){ constraint,sequence=lhcb2,range=ACFCA.BR5.B2,betx=betx_acf;};"
        )
        mad.input(
            "if(bety_acf>0){ constraint,sequence=lhcb2,range=ACFCA.BR5.B2,bety=bety_acf;};"
        )
        mad.input(
            "if(bety_d2>0){ constraint,sequence=lhcb2, range=MBRD.4R5.B2,bety=bety_d2;};"
        )

    mad.input("vary,name=KQ7.L5B1,    UPPER=-scl*qtlim3, LOWER=-sc79*qtlim3;")
    mad.input("vary,name=KQ7.R5B1,    UPPER= sc79*qtlim3,LOWER= scl*qtlim3;")
    mad.input("vary,name=KQ7.L5b2,    UPPER= sc79*qtlim3,LOWER= scl*qtlim3;")
    mad.input("vary,name=KQ7.R5b2,    UPPER=-scl*qtlim3, LOWER=-sc79*qtlim3;")
    mad.input("vary,name=KQ8.L5B1,    UPPER= sc79*qtlim3,LOWER= scl*qtlim3;")
    mad.input("vary,name=KQ9.L5B1,    UPPER=-scl*qtlim3, LOWER=-sc79*qtlim3;")
    mad.input("vary,name=KQ10.L5B1,   UPPER= sch*qtlim3, LOWER= scl*qtlim3;")
    mad.input("vary,name=KQ8.R5B1,    UPPER=-scl*qtlim3, LOWER=-sc79*qtlim3;")
    mad.input("vary,name=KQ9.R5B1,    UPPER= sc79*qtlim3,LOWER= scl*qtlim3;")
    mad.input("vary,name=KQ10.R5B1,   UPPER=-scl*qtlim3, LOWER=-sch*qtlim3;")
    mad.input("vary,name=KQ8.L5b2,    UPPER=-scl*qtlim3, LOWER=-sc79*qtlim3;")
    mad.input("vary,name=KQ9.L5b2,    UPPER= sc79*qtlim3,LOWER= scl*qtlim3;")
    mad.input("vary,name=KQ10.L5b2,   UPPER=-scl*qtlim3, LOWER=-sch*qtlim3;")
    mad.input("vary,name=KQ8.R5b2,    UPPER= sc79*qtlim3,LOWER= scl*qtlim3;")
    mad.input("vary,name=KQ9.R5b2,    UPPER=-scl*qtlim3, LOWER=-sc79*qtlim3;")
    mad.input("vary,name=KQ10.R5b2,   UPPER= sch*qtlim3, LOWER= scl*qtlim3;")
    mad.input("vary,name=KQTL11.L5B1, LOWER=-sch*qtlim4, UPPER= sch*qtlim4;")
    mad.input("vary,name=KQTL11.R5B1, LOWER=-sch*qtlim4, UPPER= sch*qtlim4;")
    mad.input("vary,name=KQTL11.L5b2, LOWER=-sch*qtlim4, UPPER= sch*qtlim4;")
    mad.input(
        "vary,name=KQTL11.R5b2, LOWER=-sch*qtlim4*500/550, UPPER= sch*qtlim4*500/550;"
    )  # non-conformity in IR1"
    mad.input("vary,name=KQT12.L5B1,  LOWER=-sch*qtlim5, UPPER= sch*qtlim5;")
    mad.input("vary,name=KQT12.L5b2,  LOWER=-sch*qtlim5, UPPER= sch*qtlim5;")
    mad.input("vary,name=KQT12.R5B1,  LOWER=-sch*qtlim5, UPPER= sch*qtlim5;")
    mad.input("vary,name=KQT12.R5b2,  LOWER=-sch*qtlim5, UPPER= sch*qtlim5;")
    mad.input("vary,name=KQT13.L5B1,  LOWER=-sch*qtlim5, UPPER= sch*qtlim5;")
    mad.input("vary,name=KQT13.L5b2,  LOWER=-sch*qtlim5, UPPER= sch*qtlim5;")
    mad.input("vary,name=KQT13.R5b2,  LOWER=-sch*qtlim5, UPPER= sch*qtlim5;")
    mad.input("vary,name=KQT13.R5B1,  LOWER=-sch*qtlim5, UPPER= sch*qtlim5;")
    # mad.input("!if (jac_calls>0){lmdif,calls=500,tolerance=tol;};")
    mad.input("jacobian,calls=jac_calls,tolerance=jac_tol,bisec=jac_bisec;")
    # mad.input("!lmdif,calls=1000,tolerance=tol;")
    mad.input("endmatch;")

    mad.input("value,twiss(table,IP5,mux);")

    mad.input("exec,mk_irtwiss(5,b1);")
    mad.input("exec,mk_irtwiss(5,b2);")

    mad.input(
        """get_betmax(name,xy): macro= {"""
        """bet=table(twiss,name,betxy);"""
        """alf=table(twiss,name,alfxy);"""
        """k1= table(twiss,name,k1l)/table(twiss,name,l);"""
        """betxymax_name=bet+alf^2/bet/abs(k1);"""
        """value,betxymax_name;"""
        """};"""
    )

    mad.input("exec,get_betmax(MQXFA.B3L5,x);")
    mad.input("exec,get_betmax(MQXFB.B2L5,y);")
    mad.input("exec,get_betmax(MQXFB.A2L5,x);")
    mad.input("exec,get_betmax(MQXFA.A3L5,y);")

    mad.input("betx_acf_ref=table(twiss,ACFCA.BR5.B2,betx);")
    mad.input("bety_acf_ref=table(twiss,ACFCA.BR5.B2,bety);")
    mad.input("betx_mcby_ref=table(twiss,MCBYYH.4R5.B2,betx);")
    mad.input("bety_mcby_ref=table(twiss,MCBYYH.4R5.B2,bety);")
    mad.input("bety_q3_ref=table(twiss,MQXFA.B3R5,bety);")

    mad.input(
        "value,betx_acf_ref,bety_acf_ref,betx_mcby_ref,bety_mcby_ref,bety_q3_ref;"
    )

    mad.input("refbeta=(refbetxip5b1+refbetyip5b1+refbetxip5b2+refbetyip5b2)/4;")

    mad.input("value,tar;")
    mad.input("value,KQX1.L5*scale,KQX2.L5*scale,KQX3.L5*scale;")

    mad.input("if (tar > 1e-12){return;} ;")

    mad.input("value,-KQ4.L5B1/KQ4.R5B1;")
    mad.input("value,-KQ5.L5B1/KQ5.R5B1;")
    mad.input("value,-KQ4.L5B2/KQ4.R5B2;")
    mad.input("value,-KQ5.L5B2/KQ5.R5B2;")
    mad.input("value,-KQ4.L5B2/KQ4.L5B1;")
    mad.input("value,-KQ4.R5B2/KQ4.R5B1;")
    mad.input("value,-KQ5.L5B2/KQ5.L5B1;")
    mad.input("value,-KQ5.R5B2/KQ5.R5B1;")
    mad.input("value,-KQ6.L5B2/KQ6.L5B1;")
    mad.input("value,-KQ6.R5B2/KQ6.R5B1;")
    mad.input("value, KQ4.L5B1/qtlimq4;")
    mad.input("value, KQ4.R5B1/qtlimq4;")
    mad.input("value, KQ4.L5b2/qtlimq4;")
    mad.input("value, KQ4.R5b2/qtlimq4;")
    mad.input("value, KQ5.L5B1/qtlimq5;")
    mad.input("value, KQ5.R5B1/qtlimq5;")
    mad.input("value, KQ5.L5b2/qtlimq5;")
    mad.input("value, KQ5.R5b2/qtlimq5;")
    mad.input("value, KQ6.L5B1/qtlim2;")
    mad.input("value, KQ6.R5B1/qtlim2;")
    mad.input("value, KQ6.L5b2/qtlim2;")
    mad.input("value, KQ6.R5b2/qtlim2;")
    mad.input("value, KQ7.L5B1   /qtlim3;")
    mad.input("value, KQ7.R5B1   /qtlim3;")
    mad.input("value, KQ7.L5b2   /qtlim3;")
    mad.input("value, KQ7.R5b2   /qtlim3;")
    mad.input("value, KQ8.L5B1   /qtlim3;")
    mad.input("value, KQ8.R5B1   /qtlim3;")
    mad.input("value, KQ8.L5b2   /qtlim3;")
    mad.input("value, KQ8.R5b2   /qtlim3;")
    mad.input("value, KQ9.L5B1   /qtlim3;")
    mad.input("value, KQ9.R5B1   /qtlim3;")
    mad.input("value, KQ9.L5b2   /qtlim3;")
    mad.input("value, KQ9.R5b2   /qtlim3;")
    mad.input("value, KQ10.L5B1  /qtlim3;")
    mad.input("value, KQ10.L5b2  /qtlim3;")
    mad.input("value, KQ10.R5B1  /qtlim3;")
    mad.input("value, KQ10.R5b2  /qtlim3;")
    mad.input("value, KQTL11.L5B1/qtlim4;")
    mad.input("value, KQTL11.R5B1/qtlim4;")
    mad.input("value, KQTL11.R5b2/qtlim4;")
    mad.input("value, KQTL11.L5b2/qtlim4;")
    mad.input("value, KQT12.L5B1 /qtlim5;")
    mad.input("value, KQT12.R5B1 /qtlim5;")
    mad.input("value, KQT12.L5b2 /qtlim5;")
    mad.input("value, KQT12.R5b2 /qtlim5;")
    mad.input("value, KQT13.L5B1 /qtlim5;")
    mad.input("value, KQT13.L5b2 /qtlim5;")
    mad.input("value, KQT13.R5B1 /qtlim5;")
    mad.input("value, KQT13.R5b2 /qtlim5;")
    mad.input("value, imq5l,imq5r;")

    mad.input("tarir5b1   = tar;")
    mad.input("tarir5b2   = tar;")
    mad.input("tarir15b12 = tar;")
    mad.input("value,tarir15b12;")

    mad.input("kqx1.r5  = kqx1.r5  ;")
    mad.input("kqx2a.l5 = kqx2a.l5 ;")
    mad.input("kqx2b.l5 = kqx2b.l5 ;")
    mad.input("kqx2a.r5 = kqx2a.r5 ;")
    mad.input("kqx2b.r5 = kqx2b.r5 ;")
    mad.input("kqx3.r5  = kqx3.r5  ;")

    mad.input("kqx1.l5     = kqx1.l5     ;")
    mad.input("kqx2a.l5    = kqx2a.l5    ;")
    mad.input("kqx2b.l5    = kqx2b.l5    ;")
    mad.input("kqx3.l5     = kqx3.l5     ;")
    mad.input("kqx1.r5     = kqx1.r5     ;")
    mad.input("kqx2a.r5    = kqx2a.r5    ;")
    mad.input("kqx2b.r5    = kqx2b.r5    ;")
    mad.input("kqx3.r5     = kqx3.r5     ;")
    mad.input("kq4.l5b1    = kq4.l5b1    ;")
    mad.input("kq4.r5b1    = kq4.r5b1    ;")
    mad.input("kq5.l5b1    = kq5.l5b1    ;")
    mad.input("kq5.r5b1    = kq5.r5b1    ;")
    mad.input("kq6.l5b1    = kq6.l5b1    ;")
    mad.input("kq6.r5b1    = kq6.r5b1    ;")
    mad.input("kq7.l5b1    = kq7.l5b1    ;")
    mad.input("kq7.r5b1    = kq7.r5b1    ;")
    mad.input("kq8.l5b1    = kq8.l5b1    ;")
    mad.input("kq8.r5b1    = kq8.r5b1    ;")
    mad.input("kq9.l5b1    = kq9.l5b1    ;")
    mad.input("kq9.r5b1    = kq9.r5b1    ;")
    mad.input("kq10.l5b1   = kq10.l5b1   ;")
    mad.input("kq10.r5b1   = kq10.r5b1   ;")
    mad.input("kqtl11.l5b1 = kqtl11.l5b1 ;")
    mad.input("kqtl11.r5b1 = kqtl11.r5b1 ;")
    mad.input("kqt12.l5b1  = kqt12.l5b1  ;")
    mad.input("kqt12.r5b1  = kqt12.r5b1  ;")
    mad.input("kqt13.l5b1  = kqt13.l5b1  ;")
    mad.input("kqt13.r5b1  = kqt13.r5b1  ;")
    mad.input("kq4.l5b2    = kq4.l5b2    ;")
    mad.input("kq4.r5b2    = kq4.r5b2    ;")
    mad.input("kq5.l5b2    = kq5.l5b2    ;")
    mad.input("kq5.r5b2    = kq5.r5b2    ;")
    mad.input("kq6.l5b2    = kq6.l5b2    ;")
    mad.input("kq6.r5b2    = kq6.r5b2    ;")
    mad.input("kq7.l5b2    = kq7.l5b2    ;")
    mad.input("kq7.r5b2    = kq7.r5b2    ;")
    mad.input("kq8.l5b2    = kq8.l5b2    ;")
    mad.input("kq8.r5b2    = kq8.r5b2    ;")
    mad.input("kq9.l5b2    = kq9.l5b2    ;")
    mad.input("kq9.r5b2    = kq9.r5b2    ;")
    mad.input("kq10.l5b2   = kq10.l5b2   ;")
    mad.input("kq10.r5b2   = kq10.r5b2   ;")
    mad.input("kqtl11.l5b2 = kqtl11.l5b2 ;")
    mad.input("kqtl11.r5b2 = kqtl11.r5b2 ;")
    mad.input("kqt12.l5b2  = kqt12.l5b2  ;")
    mad.input("kqt12.r5b2  = kqt12.r5b2  ;")
    mad.input("kqt13.l5b2  = kqt13.l5b2  ;")
    mad.input("kqt13.r5b2  = kqt13.r5b2  ;")

    mad.input("betxip1b1=betxip5b1;")
    mad.input("betyip1b1=betyip5b1;")
    mad.input("betxip1b2=betxip5b2;")
    mad.input("betyip1b2=betyip5b2;")

    mad.input("kqx1.l1     = kqx1.l5     ;")
    mad.input("kqx2a.l1    = kqx2a.l5    ;")
    mad.input("kqx2b.l1    = kqx2b.l5    ;")
    mad.input("kqx3.l1     = kqx3.l5     ;")
    mad.input("kqx1.r1     = kqx1.r5     ;")
    mad.input("kqx2a.r1    = kqx2a.r5    ;")
    mad.input("kqx2b.r1    = kqx2b.r5    ;")
    mad.input("kqx3.r1     = kqx3.r5     ;")
    mad.input("kq4.l1b1    = kq4.l5b1    ;")
    mad.input("kq4.r1b1    = kq4.r5b1    ;")
    mad.input("kq5.l1b1    = kq5.l5b1    ;")
    mad.input("kq5.r1b1    = kq5.r5b1    ;")
    mad.input("kq6.l1b1    = kq6.l5b1    ;")
    mad.input("kq6.r1b1    = kq6.r5b1    ;")
    mad.input("kq7.l1b1    = kq7.l5b1    ;")
    mad.input("kq7.r1b1    = kq7.r5b1    ;")
    mad.input("kq8.l1b1    = kq8.l5b1    ;")
    mad.input("kq8.r1b1    = kq8.r5b1    ;")
    mad.input("kq9.l1b1    = kq9.l5b1    ;")
    mad.input("kq9.r1b1    = kq9.r5b1    ;")
    mad.input("kq10.l1b1   = kq10.l5b1   ;")
    mad.input("kq10.r1b1   = kq10.r5b1   ;")
    mad.input("kqtl11.l1b1 = kqtl11.l5b1 ;")
    mad.input("kqtl11.r1b1 = kqtl11.r5b1 ;")
    mad.input("kqt12.l1b1  = kqt12.l5b1  ;")
    mad.input("kqt12.r1b1  = kqt12.r5b1  ;")
    mad.input("kqt13.l1b1  = kqt13.l5b1  ;")
    mad.input("kqt13.r1b1  = kqt13.r5b1  ;")
    mad.input("kq4.l1b2    = kq4.l5b2    ;")
    mad.input("kq4.r1b2    = kq4.r5b2    ;")
    mad.input("kq5.l1b2    = kq5.l5b2    ;")
    mad.input("kq5.r1b2    = kq5.r5b2    ;")
    mad.input("kq6.l1b2    = kq6.l5b2    ;")
    mad.input("kq6.r1b2    = kq6.r5b2    ;")
    mad.input("kq7.l1b2    = kq7.l5b2    ;")
    mad.input("kq7.r1b2    = kq7.r5b2    ;")
    mad.input("kq8.l1b2    = kq8.l5b2    ;")
    mad.input("kq8.r1b2    = kq8.r5b2    ;")
    mad.input("kq9.l1b2    = kq9.l5b2    ;")
    mad.input("kq9.r1b2    = kq9.r5b2    ;")
    mad.input("kq10.l1b2   = kq10.l5b2   ;")
    mad.input("kq10.r1b2   = kq10.r5b2   ;")
    mad.input("kqtl11.l1b2 = kqtl11.l5b2 ;")
    mad.input("kqtl11.r1b2 = kqtl11.r5b2 ;")
    mad.input("kqt12.l1b2  = kqt12.l5b2  ;")
    mad.input("kqt12.r1b2  = kqt12.r5b2  ;")
    mad.input("kqt13.l1b2  = kqt13.l5b2  ;")
    mad.input("kqt13.r1b2  = kqt13.r5b2  ;")

    if mad.globals.no_match_beta == 1:
        mad.input("betavg=(refbetxip5b1+refbetxip5b2+refbetyip5b1+refbetyip5b2)/4;")
        mad.input("betxip5b1=betavg;")
        mad.input("betyip5b1=betavg;")
        mad.input("betxip5b2=betavg;")
        mad.input("betyip5b2=betavg;")

        mad.input("value,betavg;")

    return mad.globals.tarir15b12


def rematch_ir3b1(mad):
    mad.call("acc-models-lhc/toolkit/rematch_ir3b1.madx")


def rematch_ir3b2(mad):
    mad.call("acc-models-lhc/toolkit/rematch_ir3b2.madx")


def rematch_ir3b12(mad):
    mad.input("scale = 23348.89927*0.99;")
    mad.input("scmin := 0.03*7000./nrj;")
    mad.input("qtlimitx28 := 1.0*225.0/scale;")
    mad.input("qtlimitx15 := 1.0*205.0/scale;")
    mad.input("qtlimit2 := 1.0*160.0/scale;")
    mad.input("qtlimit3 := 1.0*200.0/scale;")
    mad.input("qtlimit4 := 1.0*125.0/scale;")
    mad.input("qtlimit5 := 1.0*120.0/scale;")
    mad.input("qtlimit6 := 1.0*90.0/scale;")

    for beam in ["b1", "b2"]:
        mad.input(f"if(on_holdselect==0){{ exec,select(3,23,34,{beam}); }};")

        mad.input(f"value, muxip3{beam}, betxip3{beam}, alfxip3{beam};")
        mad.input(f"value, muyip3{beam}, betyip3{beam}, alfyip3{beam};")
        mad.input(f"value, dxip3{beam}, dpxip3{beam};")

        mad.input(f"use,sequence=lhc{beam},range=s.ds.l3.{beam}/e.ds.r3.{beam};")

        mad.input(f"match, sequence=lhc{beam}, beta0=bir3{beam};")
        mad.input(f"weight, mux=10, muy=10;")
        mad.input(
            f"constraint, sequence=lhc{beam}, range=ip3, dx=dxip3{beam}, dpx=dpxip3{beam};"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=ip3, betx=betxip3{beam},bety=betyip3{beam};"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=ip3, alfx=alfxip3{beam},alfy=alfyip3{beam};"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r3.{beam}, alfx=eir3{beam}->alfx, alfy=eir3{beam}->alfy;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r3.{beam}, betx=eir3{beam}->betx, bety=eir3{beam}->bety;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r3.{beam}, dx=eir3{beam}->dx, dpx=eir3{beam}->dpx;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r3.{beam}, mux=muxip3{beam}+eir3{beam}->mux;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r3.{beam}, muy=muyip3{beam}+eir3{beam}->muy;"
        )

        # vary for magnet strengths
        mad.input(
            f"vary, name=kqt13.l3{beam},  step=1.0E-9, LOWER=-qtlimit5, UPPER=qtlimit5;"
        )
        mad.input(
            f"vary, name=kqt12.l3{beam},  step=1.0E-9, LOWER=-qtlimit5, UPPER=qtlimit5;"
        )
        mad.input(
            f"vary, name=kqtl11.l3{beam}, step=1.0E-9, LOWER=-qtlimit4*400./550., UPPER=qtlimit4*400./550.;"
        )
        mad.input(
            f"vary, name=kqtl10.l3{beam}, step=1.0E-9, LOWER=-qtlimit4, UPPER=qtlimit4;"
        )
        mad.input(
            f"vary, name=kqtl9.l3{beam},  step=1.0E-9, LOWER=-qtlimit4, UPPER=qtlimit4;"
        )
        mad.input(
            f"vary, name=kqtl8.l3{beam},  step=1.0E-9, LOWER=-qtlimit4*450./550., UPPER=qtlimit4*450./550.;"
        )
        mad.input(
            f"vary, name=kqtl7.l3{beam},  step=1.0E-9, LOWER=-qtlimit4, UPPER=qtlimit4;"
        )
        mad.input(
            f"vary, name=kq6.l3{beam},    step=1.0E-9, LOWER=-qtlimit6, UPPER=qtlimit6;"
        )
        mad.input(
            f"vary, name=kq6.r3{beam},    step=1.0E-9, LOWER=-qtlimit6, UPPER=qtlimit6;"
        )
        mad.input(
            f"vary, name=kqtl7.r3{beam},  step=1.0E-9, LOWER=-qtlimit4, UPPER=qtlimit4;"
        )
        mad.input(
            f"vary, name=kqtl8.r3{beam},  step=1.0E-9, LOWER=-qtlimit4, UPPER=qtlimit4;"
        )
        mad.input(
            f"vary, name=kqtl9.r3{beam},  step=1.0E-9, LOWER=-qtlimit4*500./550., UPPER=qtlimit4*500./550.;"
        )
        mad.input(
            f"vary, name=kqtl10.r3{beam}, step=1.0E-9, LOWER=-qtlimit4*450./550., UPPER=qtlimit4*450./550.;"
        )
        mad.input(
            f"vary, name=kqtl11.r3{beam}, step=1.0E-9, LOWER=-qtlimit4*520./550., UPPER=qtlimit4*520./550.;"
        )
        mad.input(
            f"vary, name=kqt12.r3{beam},  step=1.0E-9, LOWER=-qtlimit5, UPPER=qtlimit5;"
        )
        mad.input(
            f"vary, name=kqt13.r3{beam},  step=1.0E-9, LOWER=-qtlimit5, UPPER=qtlimit5;"
        )
        mad.input(f"jacobian,calls=jac_calls, tolerance=jac_tol, bisec=jac_bisec;")
        mad.input(f"endmatch;")

        mad.input(f"tarir3{beam}=tar;")

        mad.input(f"exec,mk_irtwiss(3, {beam});")

        mad.input("kq4.lr3 = kq4.lr3;")
        mad.input("kqt4.l3 = kqt4.l3;")
        mad.input("kqt4.r3 = kqt4.r3;")
        mad.input("kq5.lr3 = kq5.lr3;")
        mad.input("kqt5.l3 = kqt5.l3;")
        mad.input("kqt5.r3 = kqt5.r3;")

        mad.input(
            f"""value, kq5.lr3, kq4.lr3, kqt5.l3, kqt4.l3, kqt4.r3, kqt5.r3, """
            """kqt13.l3{beam}, kqt12.l3{beam}, kqtl11.l3{beam}, """
            """kqtl10.l3{beam}, kqtl9.l3{beam}, kqtl8.l3{beam}, kqtl7.l3{beam}, kq6.l3{beam}, """
            """kq6.r3{beam}, kqtl7.r3{beam}, kqtl8.r3{beam}, kqtl9.r3{beam}, kqtl10.r3{beam}, """
            """kqtl11.r3{beam}, kqt12.r3{beam}, kqt13.r3{beam};"""
        )

        mad.input(f"value,tarir3{beam};")
    return mad.globals.tarir3b1, mad.globals.tarir3b2


def rematch_ir7b1(mad):
    mad.call("acc-models-lhc/toolkit/rematch_ir7b1.madx")


def rematch_ir7b2(mad):
    mad.call("acc-models-lhc/toolkit/rematch_ir7b2.madx")


def rematch_ir7b12(mad):
    mad.input("scale = 23348.89927;")
    mad.input("scmin := 0.03*7000./nrj;")
    mad.input("qtlimitx28 := 1.0*225.0/scale;")
    mad.input("qtlimitx15 := 1.0*205.0/scale;")
    mad.input("qtlimit2 := 1.0*160.0/scale;")
    mad.input("qtlimit3 := 1.0*200.0/scale;")
    mad.input("qtlimit4 := 1.0*125.0/scale;")
    mad.input("qtlimit5 := 1.0*120.0/scale;")
    mad.input("qtlimit6 := 1.0*90.0/scale;")

    for beam in ["b1", "b2"]:
        mad.input(f"if(on_holdselect==0){{ exec,select(7,67,78,{beam}); }};")

        mad.input(f"value, muxip7{beam}, betxip7{beam}, alfxip7{beam};")
        mad.input(f"value, muyip7{beam}, betyip7{beam}, alfyip7{beam};")
        mad.input(f"value, dxip7{beam}, dpxip7{beam};")

        mad.input(f"use, sequence=lhc{beam}, range=s.ds.l7.{beam}/e.ds.r7.{beam};")
        mad.input(f"match, sequence=lhc{beam}, beta0=bir7{beam};")
        mad.input("weight, mux=10, muy=10;")
        mad.input(
            """if (nomatch_ipbeta==0){{ """
            f"""constraint, sequence=lhc{beam}, range=ip7, betx=betxip7{beam}, bety=betyip7{beam}; """
            f"""constraint, sequence=lhc{beam}, range=ip7, alfx=alfxip7{beam}, alfy=alfyip7{beam}; """
            """}}; """
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=ip7, dx=dxip7{beam}, dpx =dpxip7{beam};"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r7.{beam}, alfx=eir7{beam}->alfx, alfy=eir7{beam}->alfy;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r7.{beam}, betx=eir7{beam}->betx, bety=eir7{beam}->bety;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r7.{beam}, dx=eir7{beam}->dx, dpx=eir7{beam}->dpx;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r7.{beam}, mux=muxip7{beam}+eir7{beam}->mux;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r7.{beam}, muy=muyip7{beam}+eir7{beam}->muy;"
        )

        # vary for magnet strengths
        mad.input(
            f"vary, name=kqt13.l7{beam},  step=1.0E-9, LOWER=-qtlimit5, UPPER=qtlimit5;"
        )
        mad.input(
            f"vary, name=kqt12.l7{beam},  step=1.0E-9, LOWER=-qtlimit5, UPPER=qtlimit5;"
        )
        mad.input(
            f"vary, name=kqtl11.l7{beam}, step=1.0E-9, LOWER=-qtlimit4*300./550., UPPER=qtlimit4*300./550.;"
        )
        mad.input(
            f"vary, name=kqtl10.l7{beam}, step=1.0E-9, LOWER=-qtlimit4*500./550., UPPER=qtlimit4*500./550.;"
        )
        mad.input(
            f"vary, name=kqtl9.l7{beam},  step=1.0E-9, LOWER=-qtlimit4*400./550., UPPER=qtlimit4*400./550.;"
        )
        mad.input(
            f"vary, name=kqtl8.l7{beam},  step=1.0E-9, LOWER=-qtlimit4*300./550., UPPER=qtlimit4*300./550.;"
        )
        mad.input(
            f"vary, name=kqtl7.l7{beam},  step=1.0E-9, LOWER=-qtlimit4, UPPER=qtlimit4;"
        )
        mad.input(
            f"vary, name=kq6.l7{beam},    step=1.0E-9, LOWER=-qtlimit6, UPPER=qtlimit6;"
        )
        mad.input(
            f"vary, name=kq6.r7{beam},    step=1.0E-9, LOWER=-qtlimit6, UPPER=qtlimit6;"
        )
        mad.input(
            f"vary, name=kqtl7.r7{beam},  step=1.0E-9, LOWER=-qtlimit4, UPPER=qtlimit4;"
        )
        mad.input(
            f"vary, name=kqtl8.r7{beam},  step=1.0E-9, LOWER=-qtlimit4*550./550., UPPER=qtlimit4*550./550.;"
        )
        mad.input(
            f"vary, name=kqtl9.r7{beam},  step=1.0E-9, LOWER=-qtlimit4*500./550., UPPER=qtlimit4*500./550.;"
        )
        mad.input(
            f"vary, name=kqtl10.r7{beam}, step=1.0E-9, LOWER=-qtlimit4, UPPER=qtlimit4;"
        )
        mad.input(
            f"vary, name=kqtl11.r7{beam}, step=1.0E-9, LOWER=-qtlimit4, UPPER=qtlimit4;"
        )
        mad.input(
            f"vary, name=kqt12.r7{beam},  step=1.0E-9, LOWER=-qtlimit5, UPPER=qtlimit5;"
        )
        mad.input(
            f"vary, name=kqt13.r7{beam},  step=1.0E-9, LOWER=-qtlimit5, UPPER=qtlimit5;"
        )

        mad.input("jacobian,calls=jac_calls, tolerance=jac_tol, bisec=jac_bisec;")
        mad.input("endmatch;")

        mad.input(f"exec, mk_irtwiss(7, {beam});")

        mad.input(f"tarir7{beam} = tar;")
        mad.input(f"value, tarir7{beam};")
        mad.input(f"kq5.lr7 = kq5.lr7;")
        mad.input(f"kq4.lr7 = kq4.lr7;")
        mad.input(f"kqt5.l7 = kqt5.l7;")
        mad.input(f"kqt4.l7 = kqt4.l7;")
        mad.input(f"kqt4.r7 = kqt4.r7;")
        mad.input(f"kqt5.r7 = kqt5.r7;")

        mad.input(
            """value, kq5.lr7, kq4.lr7, kqt5.l7, kqt4.l7, kqt4.r7, kqt5.r7,"""
            f"""kqt13.l7{beam}, kqt12.l7{beam}, kqtl11.l7{beam}, kqtl10.l7{beam},"""
            f"""kqtl9.l7{beam}, kqtl8.l7{beam}, kqtl7.l7{beam}, kq6.l7{beam},"""
            f"""kq6.r7{beam}, kqtl7.r7{beam}, kqtl8.r7{beam}, kqtl9.r7{beam},"""
            f"""kqtl10.r7{beam}, kqtl11.r7{beam}, kqt12.r7{beam}, kqt13.r7{beam};"""
        )

    return mad.globals.tarir7b1, mad.globals.tarir7b2


def rematch_ir2b12(mad, match_inj_tunes=None):
    if match_inj_tunes is not None:
        mad.input(f"match_inj_tunes = {match_inj_tunes};")

    # mad.call("acc-models-lhc/toolkit/rematch_ir2b12.madx")

    mad.input("scale = 23348.89927;")
    mad.input("scmin := 0.03*7000./nrj;")
    mad.input("qtlimitx28inj := 1.0*225.0/scale;")
    mad.input("qtlimitx28 := 1.0*205.0/scale;")
    mad.input("qtlimitx15 := 1.0*205.0/scale;")
    mad.input("qtlimit2 := 0.99*160.0/scale;")
    mad.input("qtlimit3 := 0.99*200.0/scale;")
    mad.input("qtlimit4 := 0.99*125.0/scale;")
    mad.input("qtlimit5 := 0.99*120.0/scale;")
    mad.input("qtlimit6 := 0.99*90.0/scale;")

    mad.input(
        """if(on_holdselect==0){"""
        """ exec,select(2,12,23,b1);"""
        """ exec,select(2,12,23,b2);"""
        """};"""
    )

    mad.input("value, muxip2b1, betxip2b1, alfxip2b1;")
    mad.input("value, muyip2b1, betyip2b1, alfyip2b1;")
    mad.input("value, dxip2b1, dpxip2b1;")
    mad.input("value, muxip2b2, betxip2b2, alfxip2b2;")
    mad.input("value, muyip2b2, betyip2b2, alfyip2b2;")
    mad.input("value, dxip2b2, dpxip2b2;")

    mad.input("kqx.r2:=-kqx.l2;")

    if mad.globals.match_inj_tunes == 1:
        mad.input("KQX.L2=0.950981581300E-02;")

    mad.input("use,sequence=lhcb1,range=s.ds.l2.b1/e.ds.r2.b1;")
    mad.input("use,sequence=lhcb2,range=s.ds.l2.b2/e.ds.r2.b2;")
    mad.input("match,     sequence=lhcb1,lhcb2,beta0=bir2b1,bir2b2;")
    mad.input("constraint,sequence=lhcb1,range=ip2,dx=dxip2b1,dpx =dpxip2b1;")
    mad.input("constraint,sequence=lhcb1,range=ip2,betx=betxip2b1,bety=betyip2b1;")
    mad.input("constraint,sequence=lhcb1,range=ip2,alfx=alfxip2b1,alfy=alfyip2b1;")
    mad.input("constraint,sequence=lhcb2,range=ip2,dx=dxip2b2,dpx =dpxip2b2;")
    mad.input("constraint,sequence=lhcb2,range=ip2,betx=betxip2b2,bety=betyip2b2;")
    mad.input("constraint,sequence=lhcb2,range=ip2,alfx=alfxip2b2,alfy=alfyip2b2;")
    mad.input(
        "constraint,sequence=lhcb1,range=e.ds.r2.b1,alfx=eir2b1->alfx,alfy=eir2b1->alfy;"
    )
    mad.input(
        "constraint,sequence=lhcb1,range=e.ds.r2.b1,betx=eir2b1->betx,bety=eir2b1->bety;"
    )
    mad.input(
        "constraint,sequence=lhcb1,range=e.ds.r2.b1,dx=eir2b1->dx,dpx=eir2b1->dpx;"
    )
    mad.input("constraint,sequence=lhcb1,range=e.ds.r2.b1,   mux=muxip2b1+eir2b1->mux;")
    mad.input("constraint,sequence=lhcb1,range=e.ds.r2.b1,   muy=muyip2b1+eir2b1->muy;")
    mad.input(
        "constraint,sequence=lhcb2,range=e.ds.r2.b2,alfx=eir2b2->alfx,alfy=eir2b2->alfy;"
    )
    mad.input(
        "constraint,sequence=lhcb2,range=e.ds.r2.b2,betx=eir2b2->betx,bety=eir2b2->bety;"
    )
    mad.input(
        "constraint,sequence=lhcb2,range=e.ds.r2.b2,dx=eir2b2->dx,dpx=eir2b2->dpx;"
    )
    mad.input("constraint,sequence=lhcb2,range=e.ds.r2.b2,   mux=muxip2b2+eir2b2->mux;")
    mad.input("constraint,sequence=lhcb2,range=e.ds.r2.b2,   muy=muyip2b2+eir2b2->muy;")
    if mad.globals.match_inj_tunes == 0:
        mad.input(
            "vary,name=kq5.l2b1,    step=1.0E-6, lower= qtlimit2*scmin, upper= qtlimit2;"
        )
        mad.input(
            "vary,name=kq4.l2b1,    step=1.0E-6, lower=-qtlimit2, upper=-qtlimit2*scmin;"
        )
        mad.input(
            "vary,name=kq4.r2b1,    step=1.0E-6, lower= qtlimit2*scmin, upper= qtlimit2;"
        )
        mad.input(
            "vary,name=kq5.r2b1,    step=1.0E-6, lower=-qtlimit2, upper=-qtlimit2*scmin;"
        )

    mad.input(
        "vary,name=kq4.l2b2,    step=1.0E-6, LOWER= qtlimit2*scmin, UPPER= qtlimit2;"
    )
    mad.input(
        "vary,name=kq4.r2b2,    step=1.0E-6, LOWER=-qtlimit2, UPPER=-qtlimit2*scmin;"
    )
    mad.input(
        "vary,name=kq5.l2b2,    step=1.0E-6, LOWER=-qtlimit2, UPPER=-qtlimit2*scmin;"
    )
    mad.input(
        "vary,name=kq5.r2b2,    step=1.0E-6, LOWER= qtlimit2*scmin, UPPER= qtlimit2;"
    )
    mad.input(
        "vary,name=kq6.l2b1,    step=1.0E-6, LOWER=-qtlimit2, UPPER=-qtlimit2*scmin;"
    )
    mad.input(
        "vary,name=kq6.r2b1,    step=1.0E-6, LOWER= qtlimit2*scmin, UPPER= qtlimit2;"
    )
    mad.input(
        "vary,name=kq6.l2b2,    step=1.0E-6, LOWER= qtlimit2*scmin, UPPER= qtlimit2;"
    )
    mad.input(
        "vary,name=kq6.r2b2,    step=1.0E-6, LOWER=-qtlimit2, UPPER=-qtlimit2*scmin;"
    )
    mad.input(
        "vary,name=kq7.l2b1,    step=1.0E-6, LOWER= qtlimit3*scmin, UPPER= qtlimit3;"
    )
    mad.input(
        "vary,name=kq7.r2b1,    step=1.0E-6, LOWER=-qtlimit3, UPPER=-qtlimit3*scmin;"
    )
    mad.input(
        "vary,name=kq7.l2b2,    step=1.0E-6, LOWER=-qtlimit3, UPPER=-qtlimit3*scmin;"
    )
    mad.input(
        "vary,name=kq7.r2b2,    step=1.0E-6, LOWER= qtlimit3*scmin, UPPER= qtlimit3;"
    )
    mad.input(
        "vary,name=kq8.l2b1,    step=1.0E-6, LOWER=-qtlimit3, UPPER=-qtlimit3*scmin*0.9;"
    )
    mad.input(
        "vary,name=kq8.r2b1,    step=1.0E-6, LOWER= qtlimit3*scmin, UPPER= qtlimit3;"
    )
    mad.input(
        "vary,name=kq8.l2b2,    step=1.0E-6, LOWER= qtlimit3*scmin, UPPER= qtlimit3;"
    )
    mad.input(
        "vary,name=kq8.r2b2,    step=1.0E-6, LOWER=-qtlimit3, UPPER=-qtlimit3*scmin*0.9;"
    )
    mad.input(
        "vary,name=kq9.l2b1,    step=1.0E-6, LOWER= qtlimit3*scmin, UPPER= qtlimit3;"
    )
    mad.input(
        "vary,name=kq9.l2b2,    step=1.0E-6, LOWER=-qtlimit3, UPPER=-qtlimit3*scmin;"
    )
    mad.input(
        "vary,name=kq9.r2b1,    step=1.0E-6, LOWER=-qtlimit3, UPPER=-qtlimit3*scmin;"
    )
    mad.input(
        "vary,name=kq9.r2b2,    step=1.0E-6, LOWER= qtlimit3*scmin, UPPER= qtlimit3;"
    )
    mad.input(
        "vary,name=kq10.l2b1,   step=1.0E-6, LOWER=-qtlimit3, UPPER=-qtlimit3*scmin;"
    )
    mad.input(
        "vary,name=kq10.r2b1,   step=1.0E-6, LOWER= qtlimit3*scmin, UPPER= qtlimit3;"
    )
    mad.input(
        "vary,name=kq10.l2b2,   step=1.0E-6, LOWER= qtlimit3*scmin, UPPER= qtlimit3;"
    )
    mad.input(
        "vary,name=kq10.r2b2,   step=1.0E-6, LOWER=-qtlimit3, UPPER=-qtlimit3*scmin;"
    )
    mad.input("vary,name=kqtl11.l2b1, step=1.0E-6, LOWER=-qtlimit4, UPPER= qtlimit4;")
    mad.input("vary,name=kqtl11.r2b1, step=1.0E-6, LOWER=-qtlimit4, UPPER= qtlimit4;")
    mad.input("vary,name=kqtl11.l2b2, step=1.0E-6, LOWER=-qtlimit4, UPPER= qtlimit4;")
    mad.input("vary,name=kqtl11.r2b2, step=1.0E-6, LOWER=-qtlimit4, UPPER= qtlimit4;")
    mad.input("vary,name=kqt12.l2b1,  step=1.0E-6, LOWER=-qtlimit5, UPPER= qtlimit5;")
    mad.input("vary,name=kqt12.r2b1,  step=1.0E-6, LOWER=-qtlimit5, UPPER= qtlimit5;")
    mad.input("vary,name=kqt12.l2b2,  step=1.0E-6, LOWER=-qtlimit5, UPPER= qtlimit5;")
    mad.input("vary,name=kqt12.r2b2,  step=1.0E-6, LOWER=-qtlimit5, UPPER= qtlimit5;")
    mad.input("vary,name=kqt13.l2b1,  step=1.0E-6, LOWER=-qtlimit5, UPPER= qtlimit5;")
    mad.input("vary,name=kqt13.l2b2,  step=1.0E-6, LOWER=-qtlimit5, UPPER= qtlimit5;")
    mad.input("vary,name=kqt13.r2b2,  step=1.0E-6, LOWER=-qtlimit5, UPPER= qtlimit5;")
    mad.input("vary,name=kqt13.r2b1,  step=1.0E-6, LOWER=-qtlimit5, UPPER= qtlimit5;")
    mad.input("jacobian,calls=jac_calls, tolerance=jac_tol, bisec=jac_bisec;")
    mad.input("endmatch;")

    mad.input("exec,mk_irtwiss(2,b1);")
    mad.input(
        """value,kqx.l2-0.009509815813000,kq5.l2b1-0.004826784383253,"""
        """kq4.l2b1+0.005492745228706,kq4.r2b1-0.004923088314758,"""
        """kq5.r2b1+0.004744222678546;"""
    )
    mad.input("value,table(twiss,MKI.A5L2.B1,betx),table(twiss,MKI.A5L2.B1,bety);")
    mad.input("value,table(twiss,MSIA.A6L2.B1,betx),table(twiss,MSIA.A6L2.B1,bety);")
    mad.input("value,table(twiss,MSIA.A6L2.B1,alfx),table(twiss,MSIA.A6L2.B1,alfy);")
    # to be close to 360
    mad.input("value,(table(twiss,TDI.4L2.B1,muy)-table(twiss,MKI.A5L2.B1,muy))*360;")
    mad.input("value,(table(twiss,TDI.4L2.B1,muy)-table(twiss,MKI.D5L2.B1,muy))*360;")
    mad.input("value,(table(twiss,TCLIB.6R2.B1,muy)-table(twiss,TDI.4L2.B1,muy))*360;")
    mad.input(
        "tcltordi=(table(twiss,TCLIB.6R2.B1,muy)-table(twiss,TDI.4L2.B1,muy))*360;"
    )
    mad.input("exec,mk_irtwiss(2,b2);")

    mad.input("tarir2b1 = tar;")
    mad.input("tarir2b2 = tar;")
    mad.input("tarir2b12 = tar;")
    mad.input("value, tarir2b12;")

    mad.input(
        """value,kqx.l2,kqx.r2,kqt13.l2b1,kqt12.l2b1,kqtl11.l2b1,kq10.l2b1,kq9.l2b1,kq8.l2b1,kq7.l2b1,"""
        """kq6.l2b1,kq5.l2b1,kq4.l2b1,kq4.r2b1,kq5.r2b1,kq6.r2b1,kq7.r2b1,kq8.r2b1,kq9.r2b1,kq10.r2b1,"""
        """kqtl11.r2b1,kqt12.r2b1,kqt13.r2b1;"""
    )
    mad.input(
        """value,kqx.l2,kqx.r2,kqt13.l2b2,kqt12.l2b2,kqtl11.l2b2,kq10.l2b2,kq9.l2b2,kq8.l2b2,kq7.l2b2,"""
        """kq6.l2b2,kq5.l2b2,kq4.l2b2,kq4.r2b2,kq5.r2b2,kq6.r2b2,kq7.r2b2,kq8.r2b2,kq9.r2b2,kq10.r2b2,"""
        """kqtl11.r2b2,kqt12.r2b2,kqt13.r2b2;"""
    )

    mad.input("kqx.r2   = kqx.r2    ;")
    mad.input("kqx.l2   = kqx.l2    ;")
    mad.input("ktqx1.l2 = ktqx1.l2  ;")
    mad.input("ktqx2.l2 = ktqx2.l2  ;")
    mad.input("ktqx1.r2 = ktqx1.r2  ;")
    mad.input("ktqx2.r2 = ktqx2.r2  ;")

    mad.input("value,-kq4.r2b1/kq4.r2b2;")
    mad.input("value,-kq4.l2b1/kq4.l2b2;")
    mad.input("value,-kq5.l2b1/kq5.l2b2;")
    mad.input("value,-kq5.r2b1/kq5.r2b2;")
    mad.input("value,-kq6.l2b1/kq6.l2b2;")
    mad.input("value,-kq6.r2b1/kq6.r2b2;")
    mad.input("value,-kq7.l2b1/kq7.l2b2;")
    mad.input("value,-kq7.r2b1/kq7.r2b2;")
    mad.input("value,-kq8.l2b1/kq8.l2b2;")
    mad.input("value,-kq8.r2b1/kq8.r2b2;")
    mad.input("value,-kq9.l2b1/kq9.l2b2;")
    mad.input("value,-kq9.r2b1/kq9.r2b2;")
    mad.input("value,-kq10.r2b1/kq10.r2b2;")
    mad.input("value,-kq10.l2b1/kq10.l2b2;")
    mad.input("value,tarir2b1;")

    mad.input("value,tcltordi;")
    return mad.globals.tarir2b12


def rematch_ir8b12(mad, match_inj_tunes=None):
    if match_inj_tunes is not None:
        mad.input(f"match_inj_tunes = {match_inj_tunes};")
    # mad.call("acc-models-lhc/toolkit/rematch_ir8b12.madx")

    mad.input("scale = 23348.89927;")
    mad.input("qtlimitx28 := 1.0*225.0/scale;")
    mad.input("qtlimitx15 := 1.0*205.0/scale;")
    mad.input("qtlimit2 := 1.0*160.0/scale;")
    mad.input("qtlimit3 := 1.0*200.0/scale;")
    mad.input("qtlimit4 := 1.0*125.0/scale;")
    mad.input("qtlimit5 := 1.0*120.0/scale;")
    mad.input("qtlimit6 := 1.0*90.0/scale;")
    mad.input("if(scl==0){ scl=0.10; };")
    mad.input("if(sch==0){ sch=0.99; };")

    if mad.globals.on_holdselect == 0:
        mad.input("exec,select(8,78,81,b1);")
        mad.input("exec,select(8,78,81,b2);")

    mad.input("value,muxip8b1,betxip8b1,alfxip8b1;")
    mad.input("value,muyip8b1,betyip8b1,alfyip8b1;")
    mad.input("value,dxip8b1,dpxip8b1;")
    mad.input("value,muxip8b2,betxip8b2,alfxip8b2;")
    mad.input("value,muyip8b2,betyip8b2,alfyip8b2;")
    mad.input("value,dxip8b2,dpxip8b2;")

    mad.input("kqx.r8:=-kqx.l8;")

    if mad.globals.match_inj_tunes == 1:
        mad.input("KQX.L8=0.950981581300E-02;")

    mad.input("use, sequence=lhcb1, range=s.ds.l8.b1/e.ds.r8.b1;")
    mad.input("use, sequence=lhcb2, range=s.ds.l8.b2/e.ds.r8.b2;")
    mad.input("match, sequence=lhcb1,lhcb2, beta0=bir8b1,bir8b2;")
    mad.input("weight,mux=10,muy=10;")
    if mad.globals.no_match_beta == 0:
        mad.input("constraint,sequence=lhcb1,range=ip8,betx=betxip8b1,bety=betyip8b1;")
        mad.input("constraint,sequence=lhcb2,range=ip8,betx=betxip8b2,bety=betyip8b2;")

    mad.input("constraint, sequence=lhcb1, range=ip8,dx=dxip8b1,dpx =dpxip8b1;")
    mad.input("constraint, sequence=lhcb1, range=ip8,alfx=alfxip8b1,alfy=alfyip8b1;")
    mad.input("constraint, sequence=lhcb2, range=ip8,dx=dxip8b2,dpx =dpxip8b2;")
    mad.input("constraint, sequence=lhcb2, range=ip8,alfx=alfxip8b2,alfy=alfyip8b2;")
    mad.input(
        "constraint, sequence=lhcb1, range=e.ds.r8.b1,alfx=eir8b1->alfx,alfy=eir8b1->alfy;"
    )
    mad.input(
        "constraint, sequence=lhcb1, range=e.ds.r8.b1,betx=eir8b1->betx,bety=eir8b1->bety;"
    )
    mad.input(
        "constraint, sequence=lhcb1, range=e.ds.r8.b1,dx=eir8b1->dx,dpx=eir8b1->dpx;"
    )
    mad.input(
        "constraint, sequence=lhcb1, range=e.ds.r8.b1,   mux=muxip8b1+eir8b1->mux;"
    )
    mad.input(
        "constraint, sequence=lhcb1, range=e.ds.r8.b1,   muy=muyip8b1+eir8b1->muy;"
    )
    mad.input(
        "constraint, sequence=lhcb2, range=e.ds.r8.b2,alfx=eir8b2->alfx,alfy=eir8b2->alfy;"
    )
    mad.input(
        "constraint, sequence=lhcb2, range=e.ds.r8.b2,betx=eir8b2->betx,bety=eir8b2->bety;"
    )
    mad.input(
        "constraint, sequence=lhcb2, range=e.ds.r8.b2,dx=eir8b2->dx,dpx=eir8b2->dpx;"
    )
    mad.input(
        "constraint, sequence=lhcb2, range=e.ds.r8.b2,   mux=muxip8b2+eir8b2->mux;"
    )
    mad.input(
        "constraint, sequence=lhcb2, range=e.ds.r8.b2,   muy=muyip8b2+eir8b2->muy;"
    )

    if mad.globals.match_on_triplet == 1:
        mad.input(
            "vary,name=kqx.l8, step=1.0E-6, lower= qtlimitx28*scl, upper=qtlimitx28;"
        )

    if mad.globals.match_inj_tunes == 0:
        mad.input(
            "vary,name=kq5.l8b2,    step=1.0E-6, lower=-sch*qtlimit2, upper=-qtlimit2*scl;"
        )
        mad.input(
            "vary,name=kq4.l8b2,    step=1.0E-6, lower= qtlimit2*scl, upper= sch*qtlimit2;"
        )
        mad.input(
            "vary,name=kq4.r8b2,    step=1.0E-6, lower=-sch*qtlimit2, upper=-qtlimit2*scl;"
        )
        mad.input(
            "vary,name=kq5.r8b2,    step=1.0E-6, lower= qtlimit2*scl, upper= sch*qtlimit2;"
        )

    mad.input(
        "vary,name=kq4.l8b1,    step=1.0E-6, lower=-sch*qtlimit2, upper=-qtlimit2*scl;"
    )
    mad.input(
        "vary,name=kq4.r8b1,    step=1.0E-6, lower= qtlimit2*scl, upper= sch*qtlimit2;"
    )
    mad.input(
        "vary,name=kq5.l8b1,    step=1.0E-6, lower= qtlimit2*scl, upper= sch*qtlimit2;"
    )
    mad.input(
        "vary,name=kq5.r8b1,    step=1.0E-6, lower=-sch*qtlimit2, upper=-qtlimit2*scl;"
    )
    mad.input(
        "vary,name=kq6.l8b1,    step=1.0E-6, lower=-sch*qtlimit2, upper=-qtlimit2*scl;"
    )
    mad.input(
        "vary,name=kq6.r8b1,    step=1.0E-6, lower= qtlimit2*scl, upper= sch*qtlimit2;"
    )
    mad.input(
        "vary,name=kq6.l8b2,    step=1.0E-6, lower= qtlimit2*scl, upper= sch*qtlimit2;"
    )
    mad.input(
        "vary,name=kq6.r8b2,    step=1.0E-6, lower=-sch*qtlimit2, upper=-qtlimit2*scl;"
    )
    mad.input(
        "vary,name=kq7.l8b1,    step=1.0E-6, lower= qtlimit3*scl, upper= sch*qtlimit3;"
    )
    mad.input(
        "vary,name=kq7.r8b1,    step=1.0E-6, lower=-sch*qtlimit3, upper=-qtlimit3*scl;"
    )
    mad.input(
        "vary,name=kq7.l8b2,    step=1.0E-6, lower=-sch*qtlimit3, upper=-qtlimit3*scl;"
    )
    mad.input(
        "vary,name=kq7.r8b2,    step=1.0E-6, lower= qtlimit3*scl, upper= sch*qtlimit3;"
    )
    mad.input(
        "vary,name=kq8.l8b1,    step=1.0E-6, lower=-sch*qtlimit3, upper=-qtlimit3*scl;"
    )
    mad.input(
        "vary,name=kq8.r8b1,    step=1.0E-6, lower= qtlimit3*scl, upper= sch*qtlimit3;"
    )
    mad.input(
        "vary,name=kq8.l8b2,    step=1.0E-6, lower= qtlimit3*scl, upper= sch*qtlimit3;"
    )
    mad.input(
        "vary,name=kq8.r8b2,    step=1.0E-6, lower=-sch*qtlimit3, upper=-qtlimit3*scl;"
    )
    mad.input(
        "vary,name=kq9.l8b1,    step=1.0E-6, lower= qtlimit3*scl, upper= sch*qtlimit3;"
    )
    mad.input(
        "vary,name=kq9.r8b1,    step=1.0E-6, lower=-sch*qtlimit3, upper=-qtlimit3*scl;"
    )
    mad.input(
        "vary,name=kq9.l8b2,    step=1.0E-6, lower=-sch*qtlimit3, upper=-qtlimit3*scl;"
    )
    mad.input(
        "vary,name=kq9.r8b2,    step=1.0E-6, lower= qtlimit3*scl, upper= sch*qtlimit3;"
    )
    mad.input(
        "vary,name=kq10.l8b1,   step=1.0E-6, lower=-sch*qtlimit3, upper=-qtlimit3*scl;"
    )
    mad.input(
        "vary,name=kq10.r8b1,   step=1.0E-6, lower= qtlimit3*scl, upper= sch*qtlimit3;"
    )
    mad.input(
        "vary,name=kq10.l8b2,   step=1.0E-6, lower= qtlimit3*scl, upper= sch*qtlimit3;"
    )
    mad.input(
        "vary,name=kq10.r8b2,   step=1.0E-6, lower=-sch*qtlimit3, upper=-qtlimit3*scl;"
    )
    mad.input(
        "vary,name=kqtl11.l8b1, step=1.0E-6, lower=-sch*qtlimit4, upper= sch*qtlimit4;"
    )
    mad.input(
        "vary,name=kqtl11.r8b1, step=1.0E-6, lower=-sch*qtlimit4, upper= sch*qtlimit4;"
    )
    mad.input(
        "vary,name=kqtl11.l8b2, step=1.0E-6, lower=-sch*qtlimit4, upper= sch*qtlimit4;"
    )
    mad.input(
        "vary,name=kqtl11.r8b2, step=1.0E-6, lower=-sch*qtlimit4, upper= sch*qtlimit4;"
    )
    mad.input(
        "vary,name=kqt12.l8b1,  step=1.0E-6, lower=-sch*qtlimit5, upper= sch*qtlimit5;"
    )
    mad.input(
        "vary,name=kqt12.r8b1,  step=1.0E-6, lower=-sch*qtlimit5, upper= sch*qtlimit5;"
    )
    mad.input(
        "vary,name=kqt12.l8b2,  step=1.0E-6, lower=-sch*qtlimit5, upper= sch*qtlimit5;"
    )
    mad.input(
        "vary,name=kqt12.r8b2,  step=1.0E-6, lower=-sch*qtlimit5, upper= sch*qtlimit5;"
    )
    mad.input(
        "vary,name=kqt13.l8b1,  step=1.0E-6, lower=-sch*qtlimit5, upper= sch*qtlimit5;"
    )
    mad.input(
        "vary,name=kqt13.r8b1,  step=1.0E-6, lower=-sch*qtlimit5, upper= sch*qtlimit5;"
    )
    mad.input(
        "vary,name=kqt13.l8b2,  step=1.0E-6, lower=-sch*qtlimit5, upper= sch*qtlimit5;"
    )
    mad.input(
        "vary,name=kqt13.r8b2,  step=1.0E-6, lower=-sch*qtlimit5, upper= sch*qtlimit5;"
    )
    mad.input("jacobian,calls=jac_calls, tolerance=jac_tol, bisec=jac_bisec;")
    mad.input("endmatch;")

    mad.input("exec,mk_irtwiss(8,b1);")
    mad.input("exec,mk_irtwiss(8,b2);")

    mad.input("muxip8b1_l=refmuxip8b1_l;")
    mad.input("muyip8b1_l=refmuyip8b1_l;")
    mad.input("muxip8b2_l=refmuxip8b2_l;")
    mad.input("muyip8b2_l=refmuyip8b2_l;")
    mad.input("muxip8b1_r=refmuxip8b1-eir8b1->mux-refmuxip8b1_l;")
    mad.input("muyip8b1_r=refmuyip8b1-eir8b1->muy-refmuyip8b1_l;")
    mad.input("muxip8b2_r=refmuxip8b2-eir8b2->mux-refmuxip8b2_l;")
    mad.input("muyip8b2_r=refmuyip8b2-eir8b2->muy-refmuyip8b2_l;")

    mad.input("value,refmuxip8b1-eir8b1->mux;")
    mad.input("value,refmuyip8b1-eir8b1->muy;")
    mad.input("value,refmuxip8b2-eir8b2->mux;")
    mad.input("value,refmuyip8b2-eir8b2->muy;")

    mad.input(
        """value,kqx.l8-0.00950981581300000024,"""
        """kq4.l8b2-0.00472469192800000024,"""
        """  kq5.l8b2+0.00549004210299999969,"""
        """kq4.r8b2+0.00447368899600000033,"""
        """kq5.r8b2-0.00425682473399999987 ;"""
    )
    mad.input("value,table(twiss,MKI.A5R8.B2,betx),table(twiss,MKI.A5R8.B2,bety);")
    mad.input("value,table(twiss,MSIA.A6R8.B2,betx),table(twiss,MSIA.A6R8.B2,bety);")
    mad.input("value,table(twiss,MSIA.A6R8.B2,alfx),table(twiss,MSIA.A6R8.B2,alfy);")
    mad.input(
        "value,-(table(twiss,TDISB.A4R8.B2,muy)-table(twiss,MKI.A5R8.B2,muy))*360;"
    )
    mad.input(
        "value,-(table(twiss,TDISB.A4R8.B2,muy)-table(twiss,MKI.D5R8.B2,muy))*360;"
    )
    mad.input(
        "value,-(table(twiss,TCLIB.6L8.B2,muy)-table(twiss,TDISB.A4R8.B2,muy))*360;"
    )

    mad.input("value,kqx.l8*scale;")

    mad.input(
        """value,kqx.l8,kqx.r8,kqt13.l8b1,kqt12.l8b1,kqtl11.l8b1,"""
        """kq10.l8b1,kq9.l8b1,kq8.l8b1,kq7.l8b1,"""
        """kq6.l8b1,kq5.l8b1,kq4.l8b1,kq4.r8b1,"""
        """kq5.r8b1,kq6.r8b1,kq7.r8b1,kq8.r8b1,kq9.r8b1,kq10.r8b1,"""
        """kqtl11.r8b1,kqt12.r8b1,kqt13.r8b1;"""
    )

    mad.input(
        """value,kqx.l8,kqx.r8,kqt13.l8b2,kqt12.l8b2,kqtl11.l8b2,"""
        """kq10.l8b2,kq9.l8b2,kq8.l8b2,kq7.l8b2,"""
        """kq6.l8b2,kq5.l8b2,kq4.l8b2,kq4.r8b2,"""
        """kq5.r8b2,kq6.r8b2,kq7.r8b2,kq8.r8b2,kq9.r8b2,kq10.r8b2,"""
        """kqtl11.r8b2,kqt12.r8b2,kqt13.r8b2;"""
    )

    mad.input("value,-kq4.r8b1/kq4.r8b2;")
    mad.input("value,-kq5.r8b1/kq5.r8b2;")
    mad.input("value,-kq6.r8b1/kq6.r8b2;")
    mad.input("value,-kq7.r8b1/kq7.r8b2;")
    mad.input("value,-kq8.r8b1/kq8.r8b2;")
    mad.input("value,-kq9.r8b1/kq9.r8b2;")
    mad.input("value,-kq10.r8b1/kq10.r8b2;")
    mad.input("value,-kq4.l8b1/kq4.l8b2;")
    mad.input("value,-kq5.l8b1/kq5.l8b2;")
    mad.input("value,-kq6.l8b1/kq6.l8b2;")
    mad.input("value,-kq7.l8b1/kq7.l8b2;")
    mad.input("value,-kq8.l8b1/kq8.l8b2;")
    mad.input("value,-kq9.l8b1/kq9.l8b2;")
    mad.input("value,-kq10.l8b1/kq10.l8b2;")

    if mad.globals.no_match_beta == 1:
        mad.input("betavg=(refbetxip8b1+refbetxip8b2+refbetyip8b1+refbetyip8b2)/4;")
        mad.input("betxip8b1=betavg;")
        mad.input("betyip8b1=betavg;")
        mad.input("betxip8b2=betavg;")
        mad.input("betyip8b2=betavg;")

        mad.input("value,betavg;")

    mad.input("tarir8b12=tar;")
    mad.input("tarir8b1=tar;")
    mad.input("tarir8b2=tar;")
    mad.input("value,tarir8b12;")
    mad.input(
        "value,-(table(twiss,TCLIB.6L8.B2,muy)-table(twiss,TDISB.A4R8.B2,muy))*360;"
    )

    return mad.globals.tarir8b12


def rematch_ir4b12(mad, beams=["b1", "b2"]):
    mad.input("scale = 23348.89927;")
    mad.input("scmin := 0.03*7000./nrj;")
    mad.input("sch=0.99;")
    mad.input("qtlimit2 := sch*160.0/scale;")
    mad.input("qtlimit3 := sch*200.0/scale;")
    mad.input("qtlimit4 := sch*125.0/scale;")
    mad.input("qtlimit5 := sch*120.0/scale;")
    mad.input("qtlimit6 := sch*90.0/scale;")

    scmin = mad.globals.scmin
    qtlimit2 = mad.globals.qtlimit2
    qtlimit3 = mad.globals.qtlimit3
    qtlimit4 = mad.globals.qtlimit4
    qtlimit5 = mad.globals.qtlimit5
    qtlimit6 = mad.globals.qtlimit6

    lims_b12 = {
        "kqt13.l4b1": {"limits": (-qtlimit5, qtlimit5)},
        "kqt12.l4b1": {"limits": (-qtlimit5, qtlimit5)},
        "kqtl11.l4b1": {"limits": (-qtlimit4, qtlimit4)},
        "kq10.l4b1": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq9.l4b1": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq8.l4b1": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq7.l4b1": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq6.l4b1": {"limits": (-qtlimit2, qtlimit2 * scmin)},
        "kq5.l4b1": {"limits": (qtlimit2 * scmin, qtlimit2)},
        "kq5.r4b1": {"limits": (-qtlimit2, qtlimit2 * scmin)},
        "kq6.r4b1": {"limits": (qtlimit2 * scmin, qtlimit2)},
        "kq7.r4b1": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq8.r4b1": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq9.r4b1": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq10.r4b1": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kqtl11.r4b1": {"limits": (-qtlimit4, qtlimit4)},
        "kqt12.r4b1": {"limits": (-qtlimit5, qtlimit5)},
        "kqt13.r4b1": {"limits": (-qtlimit5, qtlimit5)},
        "kqt13.l4b2": {"limits": (-qtlimit5, qtlimit5)},
        "kqt12.l4b2": {"limits": (-qtlimit5, qtlimit5)},
        "kqtl11.l4b2": {"limits": (-qtlimit4, qtlimit4)},
        "kq10.l4b2": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq9.l4b2": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq8.l4b2": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq7.l4b2": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq6.l4b2": {"limits": (qtlimit2 * scmin, qtlimit2)},
        "kq5.l4b2": {"limits": (-qtlimit2, qtlimit2 * scmin)},
        "kq5.r4b2": {"limits": (qtlimit2 * scmin, qtlimit2)},
        "kq6.r4b2": {"limits": (-qtlimit2, qtlimit2 * scmin)},
        "kq7.r4b2": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq8.r4b2": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq9.r4b2": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq10.r4b2": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kqtl11.r4b2": {"limits": (-qtlimit4, qtlimit4)},
        "kqt12.r4b2": {"limits": (-qtlimit5, qtlimit5)},
        "kqt13.r4b2": {"limits": (-qtlimit5, qtlimit5)},
    }

    for beam in beams:
        if mad.globals.on_holdselect == 0:
            mad.input(f"exec,select(4,34,45, {beam});")

        mad.input(f"value,muxip4{beam},betxip4{beam},alfxip4{beam};")
        mad.input(f"value,muyip4{beam},betyip4{beam},alfyip4{beam};")
        mad.input(f"value,dxip4{beam},dpxip4{beam};")

        mad.input(f"use, sequence=lhc{beam}, range=s.ds.l4.{beam}/e.ds.r4.{beam};")
        mad.input(f"match, sequence=lhc{beam}, beta0=bir4{beam};")
        # mad.input("weight,mux=10,muy=10;")
        mad.input(
            f"constraint, sequence=lhc{beam}, range=ip4, dx=dxip4{beam}, dpx=dpxip4{beam};"
        )
        # TODO: fix
        mad.input("nomatch_ip=0;")
        if mad.globals.nomatch_ip == 0:
            mad.input(
                f"constraint, sequence=lhc{beam}, range=ip4, betx=betxip4{beam},bety=betyip4{beam};"
            )
            mad.input(
                f"constraint, sequence=lhc{beam}, range=ip4, alfx=alfxip4{beam},alfy=alfyip4{beam};"
            )

        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r4.{beam},alfx=eir4{beam}->alfx,alfy=eir4{beam}->alfy;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r4.{beam},betx=eir4{beam}->betx,bety=eir4{beam}->bety;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r4.{beam},dx=eir4{beam}->dx,dpx=eir4{beam}->dpx;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r4.{beam},   mux=muxip4{beam}+eir4{beam}->mux;"
        )
        mad.input(
            f"constraint, sequence=lhc{beam}, range=e.ds.r4.{beam},   muy=muyip4{beam}+eir4{beam}->muy;"
        )

        # vary
        mad.input(
            f"vary,name=kqt13.l4{beam},  step=1.0E-6, LOWER={lims_b12[f'kqt13.l4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kqt13.l4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqt12.l4{beam},  step=1.0E-6, LOWER={lims_b12[f'kqt12.l4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kqt12.l4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqtl11.l4{beam}, step=1.0E-6, LOWER={lims_b12[f'kqtl11.l4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kqtl11.l4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq10.l4{beam},   step=1.0E-6, LOWER={lims_b12[f'kq10.l4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq10.l4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq9.l4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq9.l4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq9.l4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq8.l4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq8.l4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq8.l4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq7.l4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq7.l4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq7.l4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq6.l4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq6.l4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq6.l4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq5.l4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq5.l4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq5.l4{beam}']['limits'][1]};"
        )

        mad.input(
            f"vary,name=kq5.r4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq5.r4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq5.r4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq6.r4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq6.r4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq6.r4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq7.r4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq7.r4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq7.r4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq8.r4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq8.r4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq8.r4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq9.r4{beam},    step=1.0E-6, LOWER={lims_b12[f'kq9.r4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq9.r4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq10.r4{beam},   step=1.0E-6, LOWER={lims_b12[f'kq10.r4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kq10.r4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqtl11.r4{beam}, step=1.0E-6, LOWER={lims_b12[f'kqtl11.r4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kqtl11.r4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqt12.r4{beam},  step=1.0E-6, LOWER={lims_b12[f'kqt12.r4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kqt12.r4{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqt13.r4{beam},  step=1.0E-6, LOWER={lims_b12[f'kqt13.r4{beam}']['limits'][0]}, UPPER= {lims_b12[f'kqt13.r4{beam}']['limits'][1]};"
        )
        mad.input("jacobian,calls=jac_calls, tolerance=jac_tol, bisec=jac_bisec;")
        mad.input("endmatch;")

        mad.input(f"exec,mk_irtwiss(4,{beam});")

        lr_elens = {"b1": "l", "b2": "r"}
        lr_mu = {"b1": "r", "b2": "l"}
        mad.input(
            f"refbetx_el4{beam}=table(twiss,elens.5{lr_elens[beam]}4.{beam},betx);"
        )
        mad.input(
            f"refbety_el4{beam}=table(twiss,elens.5{lr_elens[beam]}4.{beam},bety);"
        )
        mad.input(f"refbetx_bsrt{beam}=table(twiss,MU.A5{lr_mu[beam]}4.{beam},betx);")
        mad.input(f"refbety_bsrt{beam}=table(twiss,MU.A5{lr_mu[beam]}4.{beam},bety);")

        mad.input(f"tarir4{beam}=tar;")
        mad.input(
            f"""value,kqt13.l4{beam},kqt12.l4{beam},kqtl11.l4{beam},"""
            f"""kq10.l4{beam},kq9.l4{beam},kq8.l4{beam},kq7.l4{beam},"""
            f"""kq6.l4{beam},kq5.l4{beam},kq5.r4{beam},kq6.r4{beam},"""
            f"""kq7.r4{beam},kq8.r4{beam},kq9.r4{beam},kq10.r4{beam},"""
            f"""kqtl11.r4{beam},kqt12.r4{beam},kqt13.r4{beam};"""
        )
        mad.input(f"value,tarir4{beam};")

    mad.input("value, tarir4b1, tarir4b2;")
    return mad.globals.tarir4b1, mad.globals.tarir4b2


def rematch_ir6b12(
    mad,
    beams=["b1", "b2"],
    nomatch_beta=None,
    nomatch_dx=None,
    nomatch_dpx=None,
    no_match_q5l6=None,
):
    if nomatch_beta is None:
        nomatch_beta = 0
    if nomatch_dx is None:
        nomatch_dx = 0
    if nomatch_dpx is None:
        nomatch_dpx = 0
    if no_match_q5l6 is None:
        no_match_q5l6 = 0

    mad.input(f"nomatch_beta = {nomatch_beta};")
    mad.input(f"nomatch_dx = {nomatch_dx};")
    mad.input(f"nomatch_dpx = {nomatch_dpx};")
    mad.input(f"no_match_q5l6 = {no_match_q5l6};")

    mad.input("scale = 23348.89927;")
    mad.input("scmin := 0.03*7000./nrj;")
    mad.input("sch=0.99; if (is_thin==1){sch=1;};")
    mad.input("qtlimit2 := sch*160.0/scale;")
    mad.input("qtlimit3 := sch*200.0/scale;")
    mad.input("qtlimit4 := sch*125.0/scale;")
    mad.input("qtlimit5 := sch*120.0/scale;")
    mad.input("qtlimit6 := sch*90.0/scale;")
    mad.input("if (q5l6_lim==0) { q5l6_lim=173.2;}; !was 160*7.5/7")
    mad.input("if (q5r6_lim==0) { q5r6_lim=173.2;}; !was 160*7.5/7")
    mad.input("qtlimit5l := sch*q5l6_lim/scale;")
    mad.input("qtlimit5r := sch*q5r6_lim/scale;")

    mad.input("chi6_nsig = 10.1;")
    mad.input("chi6_dPoverP = 2e-4;")
    mad.input("chi6_emitx=2.5e-6/(7000/0.9382720814);")
    mad.input("chi6_maxorbitdrift = 0.6e-3;")
    mad.input("al_dump=761;")

    scmin = mad.globals.scmin
    qtlimit2 = mad.globals.qtlimit2
    qtlimit3 = mad.globals.qtlimit3
    qtlimit4 = mad.globals.qtlimit4
    qtlimit5 = mad.globals.qtlimit5
    qtlimit6 = mad.globals.qtlimit6

    lims_b12 = {
        "kqt13.l6b1": {"limits": (-qtlimit5, qtlimit5)},
        "kqt12.l6b1": {"limits": (-qtlimit5, qtlimit5)},
        "kqtl11.l6b1": {
            "limits": (-qtlimit4 * 400.0 / 550.0, qtlimit4 * 400.0 / 550.0)
        },
        "kq10.l6b1": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq9.l6b1": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq8.l6b1": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq5.l6b1": {"limits": (qtlimit2 * scmin, qtlimit2)},
        "kq4.l6b1": {"limits": (-qtlimit2, qtlimit2 * scmin)},
        "kq4.r6b1": {"limits": (qtlimit2 * scmin, qtlimit2)},
        "kq5.r6b1": {"limits": (-qtlimit2, qtlimit2 * scmin)},
        "kq8.r6b1": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq9.r6b1": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq10.r6b1": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kqtl11.r6b1": {
            "limits": (-qtlimit4 * 300.0 / 550.0, qtlimit4 * 300.0 / 550.0)
        },
        "kqt12.r6b1": {"limits": (-qtlimit5, qtlimit5)},
        "kqt13.r6b1": {"limits": (-qtlimit5, qtlimit5)},
        "kqt13.l6b2": {"limits": (-qtlimit5, qtlimit5)},
        "kqt12.l6b2": {"limits": (-qtlimit5, qtlimit5)},
        "kqtl11.l6b2": {
            "limits": (-qtlimit4 * 400.0 / 550.0, qtlimit4 * 400.0 / 550.0)
        },
        "kq10.l6b2": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq9.l6b2": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq8.l6b2": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq5.l6b2": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq4.l6b2": {"limits": (qtlimit2 * scmin, qtlimit2)},
        "kq4.r6b2": {"limits": (-qtlimit2, qtlimit2 * scmin)},
        "kq5.r6b2": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq8.r6b2": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kq9.r6b2": {"limits": (qtlimit3 * scmin, qtlimit3)},
        "kq10.r6b2": {"limits": (-qtlimit3, qtlimit3 * scmin)},
        "kqtl11.r6b2": {
            "limits": (-qtlimit4 * 300.0 / 550.0, qtlimit4 * 300.0 / 550.0)
        },
        "kqt12.r6b2": {"limits": (-qtlimit5, qtlimit5)},
        "kqt13.r6b2": {"limits": (-qtlimit5, qtlimit5)},
    }
    for beam in beams:
        if mad.globals.on_holdselect == 0:
            mad.input(f"exec,select(6, 56, 67, {beam});")

        mad.input(f"value,muxip6{beam},betxip6{beam},alfxip6{beam};")
        mad.input(f"value,muyip6{beam},betyip6{beam},alfyip6{beam};")
        mad.input(f"value,dxip6{beam},dpxip6{beam};")

        mad.input(f"use,sequence=lhc{beam},range=s.ds.l6.{beam}/e.ds.r6.{beam};")
        mad.input(f"match, sequence=lhc{beam}, beta0=bir6{beam};")
        mad.input("weight,mux=10,muy=10;")
        if mad.globals.nomatch_beta == 0:
            mad.input(f"constraint,sequence=lhc{beam},range=ip6,betx=betxip6{beam};")
            mad.input(f"constraint,sequence=lhc{beam},range=ip6,alfx=alfxip6{beam};")
            mad.input(f"constraint,sequence=lhc{beam},range=ip6,bety=betyip6{beam};")
            mad.input(f"constraint,sequence=lhc{beam},range=ip6,alfy=alfyip6{beam};")

        if mad.globals.nomatch_dx == 0:
            mad.input(f"constraint,sequence=lhc{beam},range=ip6,dx =dxip6{beam};")

        if mad.globals.nomatch_dpx == 0:
            mad.input(f"constraint,sequence=lhc{beam},range=ip6,dpx=dpxip6{beam};")

        mad.input(
            f"constraint,sequence=lhc{beam},range=e.ds.r6.{beam},alfx=eir6{beam}->alfx,alfy=eir6{beam}->alfy;"
        )
        mad.input(
            f"constraint,sequence=lhc{beam},range=e.ds.r6.{beam},betx=eir6{beam}->betx,bety=eir6{beam}->bety;"
        )
        mad.input(
            f"constraint,sequence=lhc{beam},range=e.ds.r6.{beam},dx=eir6{beam}->dx,dpx=eir6{beam}->dpx;"
        )
        mad.input(
            f"constraint,sequence=lhc{beam},range=e.ds.r6.{beam},   mux=muxip6{beam}+eir6{beam}->mux;"
        )
        mad.input(
            f"constraint,sequence=lhc{beam},range=e.ds.r6.{beam},   muy=muyip6{beam}+eir6{beam}->muy;"
        )

        mad.input(
            f"vary,name=kqt13.l6{beam},  step=1.0E-6, LOWER={lims_b12[f'kqt13.l6{beam}']['limits'][0]}, UPPER={lims_b12[f'kqt13.l6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqt12.l6{beam},  step=1.0E-6, LOWER={lims_b12[f'kqt12.l6{beam}']['limits'][0]}, UPPER={lims_b12[f'kqt12.l6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqtl11.l6{beam}, step=1.0E-6, LOWER={lims_b12[f'kqtl11.l6{beam}']['limits'][0]}, UPPER={lims_b12[f'kqtl11.l6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq10.l6{beam},   step=1.0E-6, LOWER={lims_b12[f'kq10.l6{beam}']['limits'][0]}, UPPER={lims_b12[f'kq10.l6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq9.l6{beam},    step=1.0E-6, LOWER={lims_b12[f'kq9.l6{beam}']['limits'][0]}, UPPER={lims_b12[f'kq9.l6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq8.l6{beam},    step=1.0E-6, LOWER={lims_b12[f'kq8.l6{beam}']['limits'][0]}, UPPER={lims_b12[f'kq8.l6{beam}']['limits'][1]};"
        )
        if mad.globals.no_match_q5l6 == 0:
            mad.input(
                f"vary,name=kq5.l6{beam},    step=1.0E-6, LOWER={lims_b12[f'kq5.l6{beam}']['limits'][0]}, UPPER={lims_b12[f'kq5.l6{beam}']['limits'][1]};"
            )

        mad.input(
            f"vary,name=kq4.r6{beam},    step=1.0E-6, LOWER={lims_b12[f'kq4.r6{beam}']['limits'][0]}, UPPER={lims_b12[f'kq4.r6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq5.r6{beam},    step=1.0E-6, LOWER={lims_b12[f'kq5.r6{beam}']['limits'][0]}, UPPER={lims_b12[f'kq5.r6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq8.r6{beam},    step=1.0E-6, LOWER={lims_b12[f'kq8.r6{beam}']['limits'][0]}, UPPER={lims_b12[f'kq8.r6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq9.r6{beam},    step=1.0E-6, LOWER={lims_b12[f'kq9.r6{beam}']['limits'][0]}, UPPER={lims_b12[f'kq9.r6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kq10.r6{beam},   step=1.0E-6, LOWER={lims_b12[f'kq10.r6{beam}']['limits'][0]}, UPPER={lims_b12[f'kq10.r6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqtl11.r6{beam}, step=1.0E-6, LOWER={lims_b12[f'kqtl11.r6{beam}']['limits'][0]}, UPPER={lims_b12[f'kqtl11.r6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqt12.r6{beam},  step=1.0E-6, LOWER={lims_b12[f'kqt12.r6{beam}']['limits'][0]}, UPPER={lims_b12[f'kqt12.r6{beam}']['limits'][1]};"
        )
        mad.input(
            f"vary,name=kqt13.r6{beam},  step=1.0E-6, LOWER={lims_b12[f'kqt13.r6{beam}']['limits'][0]}, UPPER={lims_b12[f'kqt13.r6{beam}']['limits'][1]};"
        )
        mad.input("jacobian,calls=jac_calls, tolerance=jac_tol, bisec=jac_bisec;")
        mad.input("endmatch;")

        mad.input(f"exec,mk_irtwiss(6,{beam});")

        mad.input(f"refbetxip6{beam}=table(twiss,IP6,betx);")
        mad.input(f"refalfxip6{beam}=table(twiss,IP6,alfx);")
        mad.input(f"refbetyip6{beam}=table(twiss,IP6,bety);")
        mad.input(f"refalfyip6{beam}=table(twiss,IP6,alfy);")
        mad.input(f"refdxip6{beam}=table(twiss,IP6,dx);")
        mad.input(f"refdpxip6{beam}=table(twiss,IP6,dpx);")

        if beam == "b1":
            mad.input(
                "refdmuxkickb1_tcsg=table(twiss,TCSP.A4R6.B1,mux)- table(twiss,MKD.H5L6.B1,mux);"
            )
            mad.input(
                "refdmuxkickb1_tcdqc=table(twiss,TCDQA.C4R6.B1,mux)-table(twiss,MKD.H5L6.B1,mux);"
            )
            mad.input(
                "refdmuxkickb1_tcdqb=table(twiss,TCDQA.B4R6.B1,mux)-table(twiss,MKD.H5L6.B1,mux);"
            )
            mad.input(
                "refdmuxkickb1_tcdqa=table(twiss,TCDQA.A4R6.B1,mux)-table(twiss,MKD.H5L6.B1,mux);"
            )
            mad.input("refdmuxkickb1=refdmuxkickb1_tcdqc;")
            mad.input(
                """if(abs(refdmuxkickb1_tcdqa-0.25)>abs(refdmuxkickb1-0.25)){"""
                """refdmuxkickb1=refdmuxkickb1_tcdqa;};"""
            )
            mad.input(
                """if(abs(refdmuxkickb1_tcdqb-0.25)>abs(refdmuxkickb1-0.25)){"""
                """refdmuxkickb1=refdmuxkickb1_tcdqb;};"""
            )

            mad.input("refdxq5l6b1=abs(table(twiss,MQY.5L6.B1,dx));")
            mad.input("refdxq4r6b1=abs(table(twiss,MQY.4R6.B1,dx));")

            mad.input("refdxtcdqb1=table(twiss,TCDQA.A4R6.B1,dx);")
            mad.input("refbetxtcdqb1=table(twiss,TCDQA.A4R6.B1,betx);")
            mad.input("refbetytcdqb1=table(twiss,TCDQA.A4R6.B1,bety);")
            mad.input("refbetxtcdsb1=table(twiss,TCDSA.4L6.B1,betx);")
            mad.input("refbetytcdsb1=table(twiss,TCDSA.4L6.B1,bety);")
            mad.input("refbetxtcsgb1=table(twiss,TCSP.A4R6.B1,betx);")
            mad.input("refbetytcsgb1=table(twiss,TCSP.A4R6.B1,bety);")
            mad.input("refbetxmkdb1 =table(twiss,MKD.H5L6.B1,betx);")
            mad.input("refbetymkdb1 =table(twiss,MKD.H5L6.B1,bety);")

            mad.input(
                "refbxdumpb1=refbetxip6b1 - 2*al_dump*refalfxip6b1 + al_dump^2*(1+refalfxip6b1^2)/refbetxip6b1;"
            )
            mad.input(
                "refbydumpb1=refbetyip6b1 - 2*al_dump*refalfyip6b1 + al_dump^2*(1+refalfyip6b1^2)/refbetyip6b1;"
            )
            mad.input("refbdumpb1=sqrt(refbxdumpb1*refbydumpb1);")

            mad.input("refdmuxkickb1_bds=table(twiss,MKD.O5L6.B1,mux);")
            mad.input("refdmuxkickb1_bdsa=table(twiss,MKD.A5L6.B1,mux);")
            mad.input(
                "refdmuxkickb1_eds=table(twiss,e.ds.r6.b1,mux)- table(twiss,MKD.O5L6.B1,mux);"
            )

            mad.input("kq4.l6b1=kq4.l6b1;")

        if beam == "b2":
            mad.input(
                "refdmuxkickb2_tcsg=table(twiss,MKD.H5R6.B2,mux)-table(twiss,TCSP.A4L6.B2,mux);"
            )
            mad.input(
                "refdmuxkickb2_tcdqc=table(twiss,MKD.H5R6.B2,mux)-table(twiss,TCDQA.C4L6.B2,mux);"
            )
            mad.input(
                "refdmuxkickb2_tcdqb=table(twiss,MKD.H5R6.B2,mux)-table(twiss,TCDQA.B4L6.B2,mux);"
            )
            mad.input(
                "refdmuxkickb2_tcdqa=table(twiss,MKD.H5R6.B2,mux)-table(twiss,TCDQA.A4L6.B2,mux);"
            )
            mad.input("refdmuxkickb2=refdmuxkickb2_tcdqc;")
            mad.input(
                """if(abs(refdmuxkickb2_tcdqa-0.25)>abs(refdmuxkickb2-0.25)){"""
                """refdmuxkickb2=refdmuxkickb2_tcdqa;};"""
            )
            mad.input(
                """if(abs(refdmuxkickb2_tcdqb-0.25)>abs(refdmuxkickb2-0.25)){"""
                """refdmuxkickb2=refdmuxkickb2_tcdqb;};"""
            )

            mad.input("refdxq5r6b2=abs(table(twiss,MQY.5R6.B2,dx));")
            mad.input("refdxq4l6b2=abs(table(twiss,MQY.4L6.B2,dx));")

            mad.input("refdxtcdqb2=table(twiss,TCDQA.A4L6.B2,dx);")
            mad.input("refbetxtcdqb2=table(twiss,TCDQA.A4L6.B2,betx);")
            mad.input("refbetytcdqb2=table(twiss,TCDQA.A4L6.B2,bety);")
            mad.input("refbetxtcdsb2=table(twiss,TCDSA.4R6.B2,betx);")
            mad.input("refbetytcdsb2=table(twiss,TCDSA.4R6.B2,bety);")
            mad.input("refbetxtcsgb2=table(twiss,TCSP.A4L6.B2,betx);")
            mad.input("refbetytcsgb2=table(twiss,TCSP.A4L6.B2,bety);")
            mad.input("refbetxmkdb2 =table(twiss,MKD.H5R6.B2,betx);")
            mad.input("refbetymkdb2 =table(twiss,MKD.H5R6.B2,bety);")

            mad.input(
                "refbxdumpb2=refbetxip6b2 + 2*al_dump*refalfxip6b2 + al_dump^2*(1+refalfxip6b2^2)/refbetxip6b2;"
            )
            mad.input(
                "refbydumpb2=refbetyip6b2 + 2*al_dump*refalfyip6b2 + al_dump^2*(1+refalfyip6b2^2)/refbetyip6b2;"
            )
            mad.input("refbdumpb2=sqrt(refbxdumpb2*refbydumpb2);")

            mad.input("refdmuxkickb2_bds=table(twiss,MKD.O5R6.B2,mux);")
            mad.input("refdmuxkickb2_bdsa=table(twiss,MKD.A5R6.B2,mux);")

            mad.input("kq4.r6b2=kq4.r6b2;")

        mad.input(f"tarir6{beam}=tar;")
        mad.input(
            f"""value,kqt13.l6{beam},kqt12.l6{beam},kqtl11.l6{beam},"""
            f"""kq10.l6{beam},kq9.l6{beam},kq8.l6{beam},kq5.l6{beam},"""
            f"""kq4.l6{beam},kq4.r6{beam},kq5.r6{beam},kq8.r6{beam},"""
            f"""kq9.r6{beam},kq10.r6{beam},kqtl11.r6{beam},kqt12.r6{beam},"""
            f"""kqt13.r6{beam};"""
        )

        mad.input(f"value,tarir6{beam};")

    mad.input("value,tarir6b1,tarir6b2;")

    return mad.globals.tarir6b1, mad.globals.tarir6b2
