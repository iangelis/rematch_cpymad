from rematch_hl import *
import matplotlib.pyplot as plt

# %%
# optics_filename = sys.argv[1]
# optics_filename = "../../strengths/flat/opt_flathv_500_2000.madx"
# optics_filename = "../../strengths/round/opt_round_380_1500.madx"
# optics_filename = '/home/iangelis/Projects/hllhc_optics/summer_studies/SoL/opt_levelling_580_1500_thin.madx'
# optics_filename = '/home/iangelis/Projects/hllhc_optics/summer_studies/SoL/opt_flathv_420_840_1500_thin.madx'
# optics_filename = '/home/iangelis/Projects/hllhc_optics/summer_studies/SoL/opt_flathv_300_1200_1500_thin.madx'
# optics_filename = '/home/iangelis/Projects/hllhc_optics/summer_studies/collapse/opt_flathv_450_1800_1500.madx'
# optics_filename = '/home/iangelis/Projects/hllhc_optics/summer_studies/collapse/opt_flathv_450_1800_1500_thin.madx'
# optics_filename = '/home/iangelis/Projects/hllhc_optics/summer_studies/collapse/opt_collapse_700_1500_thin.madx'
# optics_filename = "/home/iangelis/Projects/hllhc_optics/summer_studies/collapse/opt_collapse_1100_1500_thin.madx"
optics_filename = "/home/iangelis/Projects/hllhc_optics/summer_studies/collapse/opt_collapse_1000_1500_triplet_match3.madx"

full_filename = os.path.abspath(optics_filename)

mad = Madx()
mad.chdir("../")

load_stuff(mad)

mad.input("""exec,mk_beam(7000);""")


# mad.input(f"""call,file="acc-models-lhc/strengths/ramp/{optics_filename}";""")
# %%
mad.call(f"{full_filename}")
# %%
# mad.globals["on_disp"] = 0
# mad.globals["on_x1"] = 0
# mad.globals["on_x5"] = 0
# mad.call("acc-models-lhc/toolkit/rematch_disp.madx")
# %%
rematch_hllhc(mad)

# %%
print_disp_tars(mad)
print_tar_irs(mad)
print_tars(mad)
print_xing15_tars(mad)

# %%
# save_hllhc_optics(mad, "opt_1100_1500_thin_disp.madx")
# save_hllhc_optics(mad, "opt_flathv_450_1800_1500_thin_again1111.madx")
# %%
# mad.input("seqedit,sequence=lhcb1;flatten;cycle,start=IP3;flatten;endedit;")
# mad.input("seqedit,sequence=lhcb2;flatten;cycle,start=IP3;flatten;endedit;")

# %%
mad.globals["on_disp"] = 0
mad.globals["on_x1"] = 0
mad.globals["on_x5"] = 0
mad.input("exec, check_ip(b1);")
mad.input("use, sequence=lhcb1;")
twiss_b1 = mad.twiss()
w_twiss = mad.table.twiss.dframe()
my_df = mad.twiss().dframe()

# %%


plt.plot(my_df["s"], my_df["x"], label="x")
plt.plot(my_df["s"], my_df["y"], label="y")
plt.xlabel("s")
plt.ylabel("x,y")
plt.legend()
plt.show()

# %%
# twiss_b1 = mad.twiss()
# opt_b1 = optics(twiss_b1)
# opt_r1b1 = opt_b1.select(a="s.ds.l1.b1", b="e.ds.r1.b1")
# opt_r1b1.plot("x y")
# plt.show()

# %%
# display(my_df[['betx','bety', 'alfx', 'alfy'] ,'ip.*[1258]'].to_pandas())

# %%
mad.globals["on_disp"] = 0
mad.globals["on_x1"] = 250
mad.globals["on_x5"] = 250
mad.input("exec, check_ip(b1);")
w_twiss = mad.table.twiss.dframe()
my_df = mad.twiss().dframe()
mad.table.summ
plt.plot(my_df["s"], my_df["x"], label="x")
plt.plot(my_df["s"], my_df["y"], label="y")
plt.xlabel("s")
plt.ylabel("x,y")
plt.legend()
plt.ylim(-0.00001, 0.00001)
twiss_b1 = mad.twiss()
opt_b1 = optics(twiss_b1)
opt_b1.plot("x y", ip_label=True)

opt_r1b1 = opt_b1.select(a="s.ds.l1.b1", b="e.ds.r1.b1")
opt_r1b1.plot("x y", ip_label=True)
opt_r5b1 = opt_b1.select(a="s.ds.l5.b1", b="e.ds.r5.b1")
opt_r5b1.plot("x y", ip_label=True)

twiss_b1 = mad.twiss()
opt_b1 = optics(twiss_b1)
opt_r1b1s = opt_b1.select(a="s.ds.l8.b1", b="e.ds.r2.b1")
opt_r1b1s.plot("x y", ip_label=True)
opt_r5b1s = opt_b1.select(a="s.ds.l4.b1", b="e.ds.r6.b1")
opt_r5b1s.plot("x y", ip_label=True)

# plt.ylim(-0.001, 0.001)
# %%
mad.globals["on_disp"] = 1
mad.globals["on_x1"] = 250
mad.globals["on_x5"] = 250
mad.input("exec, check_ip(b1);")
w_twiss = mad.table.twiss.dframe()
my_df = mad.twiss().dframe()
# mad.table.summ
# %%
plt.plot(my_df["s"], my_df["x"], label="x")
plt.plot(my_df["s"], my_df["y"], label="y")
plt.xlabel("s")
plt.ylabel("x,y")
plt.legend()
plt.ylim(-0.00001, 0.00001)
plt.show()

twiss_b1 = mad.twiss()
opt_b1 = optics(twiss_b1)
opt_b1.plot("x y", ip_label=True)
plt.show()

opt_r1b1 = opt_b1.select(a="s.ds.l1.b1", b="e.ds.r1.b1")
opt_r1b1.plot("x y", ip_label=True)
plt.show()

opt_r5b1 = opt_b1.select(a="s.ds.l5.b1", b="e.ds.r5.b1")
opt_r5b1.plot("x y", ip_label=True)
plt.show()

twiss_b1 = mad.twiss()
opt_b1 = optics(twiss_b1)
opt_r1b1s = opt_b1.select(a="s.ds.l8.b1", b="e.ds.r2.b1")
opt_r1b1s.plot("x y", ip_label=True)
plt.show()

opt_r5b1s = opt_b1.select(a="s.ds.l4.b1", b="e.ds.r6.b1")
opt_r5b1s.plot("x y", ip_label=True)
plt.show()


opt_r1b1 = opt_b1.select(a="s.ds.l1.b1", b="e.ds.r1.b1")
opt_r1b1.plot("x y")
plt.show()
# %%
table_summ = mad.table.summ.dframe()
display(table_summ[["q1", "q2", "dq1", "dq2"]])

# %%
