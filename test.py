# %%
from rematch_hl import *
import matplotlib.pyplot as plt
import xdeps as xd


try:
    get_ipython().run_line_magic("matplotlib", "inline")
except:
    pass


def cycle_madx(mad, name="IP1"):
    mad.input(f"cycle,start={name};")


# %%
optics_filename = "/home/iangelis/Projects/hllhc_optics/summer_studies/collapse/opt_collapse_1000_1500_triplet_match3.madx"

full_filename = os.path.abspath(optics_filename)

mad = Madx()
mad.chdir("../")

load_stuff(mad)

mad.input("""exec,mk_beam(7000);""")


# %%
mad.call(f"{full_filename}")
# %%
mad.globals.betx_IP1 = 1.1
mad.globals.bety_IP1 = 1.1
mad.globals.betx_IP5 = 1.1
mad.globals.bety_IP5 = 1.1


mad.globals.betx0_IP1 = 1.1
mad.globals.bety0_IP1 = 1.1
mad.globals.betx0_IP5 = 1.1
mad.globals.bety0_IP5 = 1.1

# %%
rematch_hllhc(
    mad,
    betx_ip1=1.1,
    bety_ip1=1.1,
    betx_ip5=1.1,
    bety_ip5=1.1,
    betx0_ip1=1.1,
    bety0_ip1=1.1,
    betx0_ip5=1.1,
    bety0_ip5=1.1,
)

# %%
print_disp_tars(mad)
print_tar_irs(mad)
print_tars(mad)
print_xing15_tars(mad)


# %%

filename_out = "rematch_cpymad/opt_test_1100_1500_out.madx"
save_optics_hllhc(mad, filename_out)

# %%

cycle_madx(mad, "IP3")

# %%
mad.input("exec, check_ip(b1);")
# mad.input("use, sequence=lhcb1;")
twiss_b1 = mad.twiss()
w_twiss = mad.table.twiss.dframe()
my_df = mad.twiss().dframe()

# %%

twb1 = xd.Table(mad.table.twiss)

# %%
print(twb1[["betx", "bety"], "ip1:1"])
print(twb1[["betx", "bety"], "ip5:1"])

print(twb1[["betx", "bety"], "ip3:1"])
print(twb1[["betx", "bety"], "ip4:1"])

print(twb1[["betx", "bety"], "ip2:1"])
print(twb1[["betx", "bety"], "ip8:1"])
# %%
plt.plot(twb1["s"], twb1["betx"], label="x")
plt.plot(twb1["s"], twb1["bety"], label="y")
plt.xlabel("s")
plt.ylabel("x,y")
plt.legend()
plt.show()

# %%
plt.plot(my_df["s"], my_df["betx"], label="x")
plt.plot(my_df["s"], my_df["bety"], label="y")
plt.xlabel("s")
plt.ylabel("x,y")
plt.legend()
plt.show()


# %%
import xtrack as xt

tt = xt.TwissTable(twb1)
tt[:, "ip.*"]
# %%
plt.plot(
    tt["s", "s.ds.l1.b1:1":"e.ds.r1.b1:1"],
    tt["betx", "s.ds.l1.b1:1":"e.ds.r1.b1:1"],
    label="x",
)
plt.plot(
    tt["s", "s.ds.l1.b1:1":"e.ds.r1.b1:1"],
    tt["bety", "s.ds.l1.b1:1":"e.ds.r1.b1:1"],
    label="y",
)
plt.title("IP1 Beam1")
plt.xlabel("s")
plt.ylabel("x,y")
plt.legend()
plt.show()

plt.plot(
    tt["s", "s.ds.l5.b1:1":"e.ds.r5.b1:1"],
    tt["betx", "s.ds.l5.b1:1":"e.ds.r5.b1:1"],
    label="x",
)
plt.plot(
    tt["s", "s.ds.l5.b1:1":"e.ds.r5.b1:1"],
    tt["bety", "s.ds.l5.b1:1":"e.ds.r5.b1:1"],
    label="y",
)
plt.title("IP5 Beam1")
plt.xlabel("s")
plt.ylabel("x,y")
plt.legend()
plt.show()

# %%
