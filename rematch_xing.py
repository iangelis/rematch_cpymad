def rematch_xing_ir15(mad, on_holdselect=None):
    if on_holdselect is not None:
        mad.input(f"on_holdselect={on_holdselect};")

    mad.input("scale = 23348.89927;")

    mad.input("CLIMmcbxfb = 2.5/scale;")  # 2.5 Tm integrated strength, MCBXFB[HV][12]
    mad.input("CLIMmcbxfa = 4.5/scale;")  # 4.5 Tm integrated strength, MCBXFA[HV]3
    mad.input("CLIMmcbrd  = 5.0/scale;")  # 5.0 Tm integrated strength, MCBXFA[HV]3
    mad.input("CLIMmcby4  = 2.25/scale;")  # 2.5*0.9 Tm at 4.5K
    mad.input("CLIMmcbc   = 2.1/scale;")  # 2.33*0.9 Tm at 4.5K

    if mad.globals.on_holdselect == 0:
        mad.input("exec,selectIR15(5,45,56,b1);")
        mad.input("exec,selectIR15(5,45,56,b2);")
        mad.input("exec,selectIR15(1,81,12,b1);")
        mad.input("exec,selectIR15(1,81,12,b2);")

    mad.input(
        """mktable_orbit15(irn,hv,xy,kkkk): macro={"""
        """create,table=kkkk,"""
        """column=acbxhv1.lirn,acbxhv1.rirn,"""
        """       acbxhv2.lirn,acbxhv2.rirn,"""
        """       acbxhv3.lirn,acbxhv3.rirn,"""
        """       acbrdhv4.lirnb1,acbrdhv4.lirnb2,acbrdhv4.rirnb1,acbrdhv4.rirnb2, """
        """       acbyhvs4.lirnb1,acbyhvs4.lirnb2,acbyhvs4.rirnb1,acbyhvs4.rirnb2, """
        """       acbyhv4.lirnb1,acbyhv4.lirnb2,acbyhv4.rirnb1,acbyhv4.rirnb2, """
        """       acbchv5.lirnb2,acbchv5.rirnb1, """
        """       acbchv6.lirnb1,acbchv6.rirnb2, """
        """       acbchv7.lirnb2,acbchv7.rirnb1, """
        """       xyipirnb1,xyipirnb2, """
        """       pxyipirnb1,pxyipirnb2; """
        """};"""
    )

    mad.input(
        """match_xing(irn,hv,xy,xang,nnn): macro={"""
        """delete,table=knob_on_xirnhvnnn;"""
        """create,table=knob_on_xirnhvnnn,"""
        """       column=acbxhv1.lirn,acbxhv1.rirn,"""
        """              acbxhv2.lirn,acbxhv2.rirn,"""
        """              acbxhv3.lirn,acbxhv3.rirn,"""
        """              acbrdhv4.lirnb1,acbrdhv4.lirnb2,acbrdhv4.rirnb1,acbrdhv4.rirnb2,"""
        """              acbyhvs4.lirnb1,acbyhvs4.lirnb2,acbyhvs4.rirnb1,acbyhvs4.rirnb2,"""
        """              acbyhv4.lirnb1,acbyhv4.lirnb2,acbyhv4.rirnb1,acbyhv4.rirnb2,"""
        """              pxyipirnb1,pxyipirnb2;"""
        """"""
        """setvars_const,table=knob_on_xirnhvnnn;"""
        """"""
        """"""
        """use, sequence= lhcb1,range=s.ds.lirn.b1/e.ds.rirn.b1;"""
        """use, sequence= lhcb2,range=s.ds.lirn.b2/e.ds.rirn.b2;"""
        """xh=1e-6;xv=0;"""
        """yh=0;yv=1e-6;"""
        """"""
        """acbxhv3.lirn=1e-8;"""
        """acbxhv3.rirn=-2e-8;"""
        """"""
        """match, sequence=lhcb1,lhcb2, beta0= birirnb1,birirnb2,"""
        """x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=ipirn, pxy =  xang*xyhv;"""
        """  constraint, sequence=lhcb2,range=ipirn, pxy = -xang*xyhv;"""
        """  constraint, sequence=lhcb1,range=ipirn, xy = 0;"""
        """  constraint, sequence=lhcb2,range=ipirn, xy = 0;"""
        """  constraint, sequence=lhcb1,range=e.ds.rirn.b1,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb2,range=e.ds.rirn.b2,xy = 0.0, pxy = 0.0;"""
        """  vary, name=acbxhv1.lirn,step=1.e-12,lower=-CLIMmcbxfb,upper=CLIMmcbxfb;"""
        """  vary, name=acbxhv1.rirn,step=1.e-12,lower=-CLIMmcbxfb,upper=CLIMmcbxfb;"""
        """  acbxhv2.lirn := cmcb12*acbxhv1.lirn;"""
        """  acbxhv2.rirn := cmcb12*acbxhv1.rirn;"""
        """  vary, name=acbxhv3.lirn,step=1.e-12,lower=-CLIMmcbxfa,upper=CLIMmcbxfa;"""
        """  vary, name=acbxhv3.rirn,step=1.e-12,lower=-CLIMmcbxfa,upper=CLIMmcbxfa;"""
        """"""
        """  vary, name=acbrdhv4.lirnb1aux,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.lirnb2aux,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.rirnb1aux,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.rirnb2aux,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """"""
        """  acbrdhv4.lirnb1:=(1-cd2q4)*acbrdhv4.lirnb1aux;"""
        """  acbrdhv4.lirnb2:=(1-cd2q4)*acbrdhv4.lirnb2aux;"""
        """  acbrdhv4.rirnb1:=(1-cd2q4)*acbrdhv4.rirnb1aux;"""
        """  acbrdhv4.rirnb2:=(1-cd2q4)*acbrdhv4.rirnb2aux;"""
        """"""
        """  acbyhvs4.lirnb1:=cd2q4*acbrdhv4.lirnb1aux;"""
        """  acbyhvs4.lirnb2:=cd2q4*acbrdhv4.lirnb2aux;"""
        """  acbyhvs4.rirnb1:=cd2q4*acbrdhv4.rirnb1aux;"""
        """  acbyhvs4.rirnb2:=cd2q4*acbrdhv4.rirnb2aux;"""
        """"""
        """  acbyhv4.lirnb1 :=acbyhvs4.lirnb1;"""
        """  acbyhv4.lirnb2 :=acbyhvs4.lirnb2;"""
        """  acbyhv4.rirnb1 :=acbyhvs4.rirnb1;"""
        """  acbyhv4.rirnb2 :=acbyhvs4.rirnb2;"""
        """"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=5;"""
        """endmatch;"""
        """ tar_on_xirnhvnnn=tar;"""
        """ pxyipirnb1=  xang*xyhv;"""
        """ pxyipirnb2= -xang*xyhv;"""
        """"""
        """fill,table=knob_on_xirnhvnnn,scale=1/xang;"""
        """setvars_const,table=knob_on_xirnhvnnn;"""
        """};"""
    )

    mad.input(
        """match_psep(irn,hv,xy,psep): macro={"""
        """delete,table=knob_on_sepirnhv;"""
        """create,table=knob_on_sepirnhv,"""
        """       column=acbxhv1.lirn,acbxhv1.rirn,"""
        """              acbxhv2.lirn,acbxhv2.rirn,"""
        """              acbxhv3.lirn,acbxhv3.rirn,"""
        """              acbrdhv4.lirnb1,acbrdhv4.lirnb2,acbrdhv4.rirnb1,acbrdhv4.rirnb2,"""
        """              acbyhvs4.lirnb1,acbyhvs4.lirnb2,acbyhvs4.rirnb1,acbyhvs4.rirnb2,"""
        """              acbyhv4.lirnb1,acbyhv4.lirnb2,acbyhv4.rirnb1,acbyhv4.rirnb2,"""
        """              xyipirnb1,xyipirnb2;"""
        """"""
        """setvars_const,table=knob_on_sepirnhv;"""
        """"""
        """xh=1e-3;xv=0;"""
        """yh=0;yv=1e-3;"""
        """"""
        """use, sequence= lhcb1,range=s.ds.lirn.b1/e.ds.rirn.b1;"""
        """use, sequence= lhcb2,range=s.ds.lirn.b2/e.ds.rirn.b2;"""
        """"""
        """match, sequence=lhcb1,lhcb2, beta0= birirnb1,birirnb2,"""
        """x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=ipirn, xy  =  psep*xyhv, pxy=0;"""
        """  constraint, sequence=lhcb2,range=ipirn, xy  = -psep*xyhv, pxy=0;"""
        """  constraint, sequence=lhcb1,range=e.ds.rirn.b1,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb2,range=e.ds.rirn.b2,xy = 0.0, pxy = 0.0;"""
        """  vary, name=acbxhv1.lirn,step=1.e-12,lower=-climmcbxfb,upper=climmcbxfb;"""
        """  acbxhv2.lirn := cmcb12*acbxhv1.lirn;"""
        """  vary, name=acbxhv3.lirn,step=1.e-12,lower=-climmcbxfa,upper=climmcbxfa;"""
        """  vary, name=acbxhv1.rirn,step=1.e-12,lower=-climmcbxfb,upper=climmcbxfb;"""
        """  acbxhv2.rirn := cmcb12*acbxhv1.rirn;"""
        """  vary, name=acbxhv3.rirn,step=1.e-12,lower=-climmcbxfa,upper=climmcbxfa;"""
        """  vary, name=acbrdhv4.lirnb1,step=1.e-12,lower=-climmcbrd,upper=climmcbrd;"""
        """  vary, name=acbrdhv4.lirnb2,step=1.e-12,lower=-climmcbrd,upper=climmcbrd;"""
        """  vary, name=acbrdhv4.rirnb1,step=1.e-12,lower=-climmcbrd,upper=climmcbrd;"""
        """  vary, name=acbrdhv4.rirnb2,step=1.e-12,lower=-climmcbrd,upper=climmcbrd;"""
        """  acbyhvs4.lirnb1:=cd2q4*acbrdhv4.lirnb1;"""
        """  acbyhvs4.lirnb2:=cd2q4*acbrdhv4.lirnb2;"""
        """  acbyhvs4.rirnb1:=cd2q4*acbrdhv4.rirnb1;"""
        """  acbyhvs4.rirnb2:=cd2q4*acbrdhv4.rirnb2;"""
        """  acbyhv4.lirnb1 :=acbyhvs4.lirnb1;"""
        """  acbyhv4.lirnb2 :=acbyhvs4.lirnb2;"""
        """  acbyhv4.rirnb1 :=acbyhvs4.rirnb1;"""
        """  acbyhv4.rirnb2 :=acbyhvs4.rirnb2;"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """  tar_on_sepirnhv=tar;"""
        """  xyipirnb1=  psep*xyhv;"""
        """  xyipirnb2= -psep*xyhv;"""
        """"""
        """fill,table=knob_on_sepirnhv,scale=1/psep;"""
        """setvars_const,table=knob_on_sepirnhv;"""
        """};"""
    )

    mad.input(
        """match_off(irn,hv,xy,off): macro={"""
        """xh=1e-3;xv=0;"""
        """yh=0;yv=1e-3;"""
        """"""
        """delete,table=knob_on_oirnhv;"""
        """"""
        """if (xhv>0){"""
        """create,table=knob_on_oirnhv,"""
        """       column=acbxhv1.lirn,acbxhv1.rirn,"""
        """              acbxhv2.lirn,acbxhv2.rirn,"""
        """              acbxhv3.lirn,acbxhv3.rirn,"""
        """              acbrdhv4.lirnb1,acbrdhv4.lirnb2,acbrdhv4.rirnb1,acbrdhv4.rirnb2,"""
        """              acbyhvs4.lirnb1,acbyhvs4.lirnb2,acbyhvs4.rirnb1,acbyhvs4.rirnb2,"""
        """              acbyhv4.lirnb1,acbyhv4.lirnb2,acbyhv4.rirnb1,acbyhv4.rirnb2,"""
        """              acbchv5.lirnb2,acbchv5.rirnb1,"""
        """              acbchv6.lirnb1,acbchv6.rirnb2,"""
        """              acbchv7.lirnb2,acbchv7.rirnb1,"""
        """              xipirnb1,xipirnb2;"""
        """};"""
        """  if (yhv>0){"""
        """create,table=knob_on_oirnhv,"""
        """       column=acbxhv1.lirn,acbxhv1.rirn,"""
        """              acbxhv2.lirn,acbxhv2.rirn,"""
        """              acbxhv3.lirn,acbxhv3.rirn,"""
        """              acbrdhv4.lirnb1,acbrdhv4.lirnb2,acbrdhv4.rirnb1,acbrdhv4.rirnb2,"""
        """              acbyhvs4.lirnb1,acbyhvs4.lirnb2,acbyhvs4.rirnb1,acbyhvs4.rirnb2,"""
        """              acbyhv4.lirnb1,acbyhv4.lirnb2,acbyhv4.rirnb1,acbyhv4.rirnb2,"""
        """              acbcv5.lirnb1,acbcv5.rirnb2,"""
        """              acbcv6.lirnb2,acbcv6.rirnb1,"""
        """              acbcv7.lirnb1,acbcv7.rirnb2,"""
        """              yipirnb1,yipirnb2;"""
        """};"""
        """"""
        """"""
        """setvars_const,table=knob_on_oirnhv;"""
        """"""
        """brho=23348.89927;"""
        """limitMCBXH1 := 2.5/brho *.40;"""
        """limitMCBXH2 := 2.5/brho *.15;"""
        """limitMCBXH3 := 4.5/brho *.05;"""
        """limitMCBY   := 2.8/brho *.33;"""
        """limitMCBC   := 2.1/brho *.33;"""
        """"""
        """use, sequence= lhcb1,range=s.ds.lirn.b1/e.ds.rirn.b1;"""
        """use, sequence= lhcb2,range=s.ds.lirn.b2/e.ds.rirn.b2;"""
        """"""
        """match, sequence=lhcb1,lhcb2, beta0= birirnb1,birirnb2,"""
        """        x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=ipirn, xy  =  off*xyhv, pxy=0;"""
        """  constraint, sequence=lhcb2,range=ipirn, xy  =  off*xyhv, pxy=0;"""
        """  constraint, sequence=lhcb1,range=e.ds.rirn.b1,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb2,range=e.ds.rirn.b2,xy = 0.0, pxy = 0.0;"""
        """  vary,name=acbxhv1.lirn, step=1.0E-15,lower=-limitMCBXH1,upper=limitMCBXH1;"""
        """  vary,name=acbxhv1.rirn, step=1.0E-15,lower=-limitMCBXH1,upper=limitMCBXH1;"""
        """  vary,name=acbxhv2.lirn, step=1.0E-15,lower=-limitMCBXH2,upper=limitMCBXH2;"""
        """  vary,name=acbxhv2.rirn, step=1.0E-15,lower=-limitMCBXH2,upper=limitMCBXH2;"""
        """  vary,name=acbxhv3.lirn, step=1.0E-15,lower=-limitMCBXH3,upper=limitMCBXH3;"""
        """  vary,name=acbxhv3.rirn, step=1.0E-15,lower=-limitMCBXH3,upper=limitMCBXH3;"""
        """  vary,name=acbyhvs4.lirnb1, step=1.0E-12,lower=-limitMCBY,upper=limitMCBY;"""
        """  vary,name=acbyhvs4.rirnb1, step=1.0E-12,lower=-limitMCBY,upper=limitMCBY;"""
        """  vary,name=acbyhvs4.lirnb2, step=1.0E-12,lower=-limitMCBY,upper=limitMCBY;"""
        """  vary,name=acbyhvs4.rirnb2, step=1.0E-12,lower=-limitMCBY,upper=limitMCBY;"""
        """  acbyhv4.lirnb1 :=acbyhvs4.lirnb1;"""
        """  acbyhv4.lirnb2 :=acbyhvs4.lirnb2;"""
        """  acbyhv4.rirnb1 :=acbyhvs4.rirnb1;"""
        """  acbyhv4.rirnb2 :=acbyhvs4.rirnb2;"""
        """  if (xhv>0){"""
        """  vary,name=acbch5.lirnb2, step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbch5.rirnb1, step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbch6.lirnb1 , step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbch6.rirnb2 , step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbch7.lirnb2, step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbch7.rirnb1, step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  };"""
        """  if (yhv>0){"""
        """  vary,name=acbcv6.lirnb2 , step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbcv6.rirnb1 , step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbcv7.lirnb1, step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbcv7.rirnb2, step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbcv5.lirnb1, step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  vary,name=acbcv5.rirnb2, step=1.0E-12,lower=-limitMCBC,upper=limitMCBC;"""
        """  };"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """  tar_on_oirnhv=tar;"""
        """  xyipirnb1=  off*xyhv;"""
        """  xyipirnb2=  off*xyhv;"""
        """"""
        """fill,table=knob_on_oirnhv,scale=1/off;"""
        """setvars_const,table=knob_on_oirnhv;"""
        """};"""
    )

    mad.input(
        """match_aoff(irn,hv,xy,aoff): macro={"""
        """delete,table=knob_on_airnhv;"""
        """create,table=knob_on_airnhv,"""
        """       column=acbxhv1.lirn,acbxhv1.rirn,"""
        """              acbxhv2.lirn,acbxhv2.rirn,"""
        """              acbxhv3.lirn,acbxhv3.rirn,"""
        """              acbrdhv4.lirnb1,acbrdhv4.lirnb2,acbrdhv4.rirnb1,acbrdhv4.rirnb2,"""
        """              acbyhvs4.lirnb1,acbyhvs4.lirnb2,acbyhvs4.rirnb1,acbyhvs4.rirnb2,"""
        """              acbyhv4.lirnb1,acbyhv4.lirnb2,acbyhv4.rirnb1,acbyhv4.rirnb2,"""
        """              pxyipirnb1,pxyipirnb2;"""
        """"""
        """setvars_const,table=knob_on_airnhv;"""
        """"""
        """pxh=1e-6;pxv=0;"""
        """pyh=0;pyv=1e-6;"""
        """"""
        """use, sequence= lhcb1,range=s.ds.lirn.b1/e.ds.rirn.b1;"""
        """use, sequence= lhcb2,range=s.ds.lirn.b2/e.ds.rirn.b2;"""
        """"""
        """match, sequence=lhcb1,lhcb2, beta0= birirnb1,birirnb2,"""
        """        x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=ipirn, pxy  =  aoff*pxyhv, xy=0;"""
        """  constraint, sequence=lhcb2,range=ipirn, pxy  =  aoff*pxyhv, xy=0;"""
        """  constraint, sequence=lhcb1,range=e.ds.rirn.b1,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb2,range=e.ds.rirn.b2,xy = 0.0, pxy = 0.0;"""
        """  vary, name=acbxhv1.lirn,step=1.e-12,lower=-CLIMmcbxfb,upper=CLIMmcbxfb;"""
        """  acbxhv2.lirn:=cmcb12*acbxhv1.lirn;"""
        """  vary, name=acbxhv3.lirn,step=1.e-12,lower=-CLIMmcbxfa,upper=CLIMmcbxfa;"""
        """  vary, name=acbxhv1.rirn,step=1.e-12,lower=-CLIMmcbxfb,upper=CLIMmcbxfb;"""
        """  acbxhv2.rirn:=cmcb12*acbxhv1.rirn;"""
        """  vary, name=acbxhv3.rirn,step=1.e-12,lower=-CLIMmcbxfa,upper=CLIMmcbxfa;"""
        """  vary, name=acbrdhv4.lirnb1,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.lirnb2,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.rirnb1,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.rirnb2,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  !acbyhvs4.lirnb1:=cd2q4*acbrdhv4.lirnb1;"""
        """  !acbyhvs4.lirnb2:=cd2q4*acbrdhv4.lirnb2;"""
        """  !acbyhvs4.rirnb1:=cd2q4*acbrdhv4.rirnb1;"""
        """  !acbyhvs4.rirnb2:=cd2q4*acbrdhv4.rirnb2;"""
        """  !acbyhv4.lirnb1 :=acbyhvs4.lirnb1;"""
        """  !acbyhv4.lirnb2 :=acbyhvs4.lirnb2;"""
        """  !acbyhv4.rirnb1 :=acbyhvs4.rirnb1;"""
        """  !acbyhv4.rirnb2 :=acbyhvs4.rirnb2;"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """  tar_on_airnhv=tar;"""
        """  pxyipirnb1=  aoff*pxyhv;"""
        """  pxyipirnb2=  aoff*pxyhv;"""
        """"""
        """fill,table=knob_on_airnhv;"""
        """setvars_const,table=knob_on_airnhv;"""
        """};"""
    )

    mad.input(
        """match_ccp(irn,hv,xy): macro={"""
        """delete,table=knob_on_ccplirnhv;"""
        """create,table=knob_on_ccplirnhv,"""
        """       column=acbxhv1.lirn,"""
        """              acbxhv2.lirn,"""
        """              acbxhv3.lirn,"""
        """              acbrdhv4.lirnb1,"""
        """              acbrdhv4.lirnb2,"""
        """              acbyhvs4.lirnb1,"""
        """              acbyhvs4.lirnb2,"""
        """              acbchv5.lirnb1,"""
        """              acbchv5.lirnb2,"""
        """              acbchv6.lirnb1,"""
        """              acbchv6.lirnb2;"""
        """;"""
        """delete,table=knob_on_ccprirnhv;"""
        """create,table=knob_on_ccprirnhv,"""
        """       column=acbxhv1.rirn,"""
        """              acbxhv2.rirn,"""
        """              acbxhv3.rirn,"""
        """              acbrdhv4.rirnb1,"""
        """              acbrdhv4.rirnb2,"""
        """              acbyhvs4.rirnb1,"""
        """              acbyhvs4.rirnb2,"""
        """              acbchv5.lirnb1,"""
        """              acbchv5.lirnb2,"""
        """              acbchv6.lirnb1,"""
        """              acbchv6.lirnb2;"""
        """"""
        """"""
        """setvars_const,table=knob_on_ccplirnhv;"""
        """setvars_const,table=knob_on_ccprirnhv;"""
        """"""
        """occp=0.5e-3;"""
        """"""
        """use, sequence= lhcb1,range=s.ds.lirn.b1/e.ds.rirn.b1;"""
        """use, sequence= lhcb2,range=s.ds.lirn.b2/e.ds.rirn.b2;"""
        """"""
        """match, sequence=lhcb1,lhcb2, beta0= birirnb1,birirnb2,"""
        """        x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=ipirn, pxy  =  0, xy=0;"""
        """  constraint, sequence=lhcb2,range=ipirn, pxy  =  0, xy=0;"""
        """  constraint, sequence=lhcb1,range=e.ds.rirn.b1,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb2,range=e.ds.rirn.b2,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb1,range=acfca.4blirn.b1, xy  =  occp, pxy=0.0;"""
        """  constraint, sequence=lhcb2,range=acfca.4blirn.b2, xy  =  ocpp, pxy=0.0;"""
        """  constraint, sequence=lhcb1,range=acfca.4brirn.b1, xy  =  ocpp, pxy=0.0;"""
        """  constraint, sequence=lhcb2,range=acfca.4brirn.b2, xy  =  ocpp, pxy=0.0;"""
        """"""
        """  vary, name=acbxhv1.lirn,step=1.e-12,lower=-CLIMmcbxfb,upper=CLIMmcbxfb;"""
        """  vary, name=acbxhv1.rirn,step=1.e-12,lower=-CLIMmcbxfb,upper=CLIMmcbxfb;"""
        """  acbxhv2.lirn:=cmcb12*acbxhv1.lirn;"""
        """  acbxhv2.rirn:=cmcb12*acbxhv1.rirn;"""
        """  vary, name=acbxhv3.lirn,step=1.e-12,lower=-CLIMmcbxfa,upper=CLIMmcbxfa;"""
        """  vary, name=acbxhv3.rirn,step=1.e-12,lower=-CLIMmcbxfa,upper=CLIMmcbxfa;"""
        """  vary, name=acbrdhv4.lirnb1,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.lirnb2,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.rirnb1,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.rirnb2,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary,name=acbyhvs4.lirnb1, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  vary,name=acbyhvs4.rirnb1, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  vary,name=acbyhvs4.lirnb2, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  vary,name=acbyhvs4.rirnb2, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  acbyhv4.lirnb1 :=acbyhvs4.lirnb1;"""
        """  acbyhv4.lirnb2 :=acbyhvs4.lirnb2;"""
        """  acbyhv4.rirnb1 :=acbyhvs4.rirnb1;"""
        """  acbyhv4.rirnb2 :=acbyhvs4.rirnb2;"""
        """  vary,name=acbchv5.lirnb1, step=1.0e-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv5.lirnb2, step=1.0e-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv5.rirnb1, step=1.0e-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv5.rirnb2, step=1.0e-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.lirnb1, step=1.0e-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.lirnb2, step=1.0e-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.rirnb1, step=1.0e-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.rirnb2, step=1.0e-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """  tar_on_ccpirnhv=tar;"""
        """"""
        """fill,table=knob_on_ccplirnhv;"""
        """fill,table=knob_on_ccprirnhv;"""
        """setvars_const,table=knob_on_ccplirnhv,scale=1/occp;"""
        """setvars_const,table=knob_on_ccprirnhv,scale=1/occp;"""
        """};"""
    )

    mad.input(
        """match_ccpm(irn,hv,xy,ccp): macro={"""
        """delete,table=knob_on_ccplirnhv;"""
        """create,table=knob_on_ccplirnhv,"""
        """       column=acbxhv1.lirn,"""
        """              acbxhv2.lirn,"""
        """              acbxhv3.lirn,"""
        """              acbrdhv4.lirnb1,"""
        """              acbrdhv4.lirnb2,"""
        """              acbyhvs4.lirnb1,"""
        """              acbyhvs4.lirnb2,"""
        """              acbyhv4.lirnb1,"""
        """              acbyhv4.lirnb2,"""
        """              acbchv5.lirnb1,"""
        """              acbchv5.lirnb2,"""
        """              acbchv6.lirnb1,"""
        """              acbchv6.lirnb2;"""
        """"""
        """delete,table=knob_on_ccprirnhv;"""
        """create,table=knob_on_ccprirnhv,"""
        """       column=acbxhv1.rirn,"""
        """              acbxhv2.rirn,"""
        """              acbxhv3.rirn,"""
        """              acbrdhv4.rirnb1,"""
        """              acbrdhv4.rirnb2,"""
        """              acbyhvs4.rirnb1,"""
        """              acbyhvs4.rirnb2,"""
        """              acbyhv4.rirnb1,"""
        """              acbyhv4.rirnb2,"""
        """              acbchv5.rirnb1,"""
        """              acbchv5.rirnb2,"""
        """              acbchv6.rirnb1,"""
        """              acbchv6.rirnb2;"""
        """"""
        """"""
        """setvars_const,table=knob_on_ccplirnhv;"""
        """setvars_const,table=knob_on_ccprirnhv;"""
        """"""
        """off=0.5;"""
        """occm=1;"""
        """occp=-1;"""
        """"""
        """use, sequence= lhcb1,range=s.ds.lirn.b1/e.ds.rirn.b1;"""
        """use, sequence= lhcb2,range=s.ds.lirn.b2/e.ds.rirn.b2;"""
        """"""
        """match, sequence=lhcb1,lhcb2, beta0= birirnb1,birirnb2,"""
        """        x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=ipirn, pxy  =  0, xy=0;"""
        """  constraint, sequence=lhcb2,range=ipirn, pxy  =  0, xy=0;"""
        """  constraint, sequence=lhcb1,range=e.ds.rirn.b1,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb2,range=e.ds.rirn.b2,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb1,range=acfca.4blirn.b1, xy  =  off*1e-3, pxy=0.0;"""
        """  constraint, sequence=lhcb2,range=acfca.4blirn.b2, xy  = occm*off*1e-3, pxy=0.0;"""
        """  constraint, sequence=lhcb1,range=acfca.4brirn.b1, xy  =  off*1e-3, pxy=0.0;"""
        """  constraint, sequence=lhcb2,range=acfca.4brirn.b2, xy  = occm*off*1e-3, pxy=0.0;"""
        """"""
        """  vary, name=acbxhv1.lirn,step=1.e-12,lower=-CLIMmcbxfb,upper=CLIMmcbxfb;"""
        """  vary, name=acbxhv1.rirn,step=1.e-12,lower=-CLIMmcbxfb,upper=CLIMmcbxfb;"""
        """  acbxhv2.lirn:=cmcb12*acbxhv1.lirn;"""
        """  acbxhv2.rirn:=cmcb12*acbxhv1.rirn;"""
        """  vary, name=acbxhv3.lirn,step=1.e-12,lower=-CLIMmcbxfa,upper=CLIMmcbxfa;"""
        """  vary, name=acbxhv3.rirn,step=1.e-12,lower=-CLIMmcbxfa,upper=CLIMmcbxfa;"""
        """  vary, name=acbrdhv4.lirnb1,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.lirnb2,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.rirnb1,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary, name=acbrdhv4.rirnb2,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary,name=acbyhvs4.lirnb1, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  vary,name=acbyhvs4.rirnb1, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  vary,name=acbyhvs4.lirnb2, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  vary,name=acbyhvs4.rirnb2, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  acbyhv4.lirnb1 :=acbyhvs4.lirnb1;"""
        """  acbyhv4.lirnb2 :=acbyhvs4.lirnb2;"""
        """  acbyhv4.rirnb1 :=acbyhvs4.rirnb1;"""
        """  acbyhv4.rirnb2 :=acbyhvs4.rirnb2;"""
        """  vary,name=acbchv5.lirnb1, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv5.lirnb2, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv5.rirnb1, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv5.rirnb2, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.lirnb1, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.lirnb2, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.rirnb1, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.rirnb2, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """  tar_on_ccpirnhv=tar;"""
        """"""
        """fill,table=knob_on_ccplirnhv,scale=1/off;"""
        """fill,table=knob_on_ccprirnhv,scale=1/off;"""
        """setvars_const,table=knob_on_ccplirnhv;"""
        """setvars_const,table=knob_on_ccprirnhv;"""
        """};"""
    )

    mad.input(
        """match_ccs(irn,hv,xy): macro={"""
        """delete,table=knob_on_ccslirnhvb1;"""
        """delete,table=knob_on_ccslirnhvb2;"""
        """delete,table=knob_on_ccsrirnhvb1;"""
        """delete,table=knob_on_ccsrirnhvb2;"""
        """create,table=knob_on_ccslirnhvb1,"""
        """       column=acbrdhv4.lirnb1, acbyhvs4.lirnb1, acbyhv4.lirnb1, acbyhvs5.lirnb1;"""
        """create,table=knob_on_ccslirnhvb2,"""
        """       column=acbrdhv4.lirnb2, acbyhvs4.lirnb2, acbyhv4.lirnb2, acbyhvs5.lirnb2;"""
        """create,table=knob_on_ccsrirnhvb1,"""
        """       column=acbrdhv4.rirnb1, acbyhvs4.rirnb1, acbyhv4.rirnb1, acbyhvs5.rirnb1;"""
        """create,table=knob_on_ccsrirnhvb2,"""
        """       column=acbrdhv4.rirnb2, acbyhvs4.rirnb2, acbyhv4.rirnb2, acbyhvs5.rirnb2;"""
        """"""
        """"""
        """setvars_const,table=knob_on_ccslirnhvb1;"""
        """setvars_const,table=knob_on_ccsrirnhvb1;"""
        """setvars_const,table=knob_on_ccslirnhvb2;"""
        """setvars_const,table=knob_on_ccsrirnhvb2;"""
        """"""
        """off=0.2;"""
        """"""
        """use, sequence= lhcb1,range=s.ds.lirn.b1/e.ds.rirn.b1;"""
        """use, sequence= lhcb2,range=s.ds.lirn.b2/e.ds.rirn.b2;"""
        """"""
        """match, sequence=lhcb1,lhcb2, beta0= birirnb1,birirnb2,"""
        """        x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=ipirn, pxy  =  0, xy=0;"""
        """  constraint, sequence=lhcb2,range=ipirn, pxy  =  0, xy=0;"""
        """  constraint, sequence=lhcb1,range=e.ds.rirn.b1,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb2,range=e.ds.rirn.b2,xy = 0.0, pxy = 0.0;"""
        """  constraint, sequence=lhcb1,range=acfca.4blirn.b1, xy  =  off*1e-3;"""
        """  constraint, sequence=lhcb2,range=acfca.4blirn.b2, xy  =  off*1e-3;"""
        """  constraint, sequence=lhcb1,range=acfca.4brirn.b1, xy  =  off*1e-3;"""
        """  constraint, sequence=lhcb2,range=acfca.4brirn.b2, xy  =  off*1e-3;"""
        """"""
        """  vary,name=acbrdhv4.lirnb1,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary,name=acbrdhv4.lirnb2,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary,name=acbrdhv4.rirnb1,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary,name=acbrdhv4.rirnb2,step=1.e-12,lower=-CLIMmcbrd,upper=CLIMmcbrd;"""
        """  vary,name=acbyhvs4.lirnb1, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  vary,name=acbyhvs4.rirnb1, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  vary,name=acbyhvs4.lirnb2, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  vary,name=acbyhvs4.rirnb2, step=1.0E-12,lower=-CLIMmcby4,upper=CLIMmcby4;"""
        """  acbyhv4.lirnb1 :=acbyhvs4.lirnb1;"""
        """  acbyhv4.lirnb2 :=acbyhvs4.lirnb2;"""
        """  acbyhv4.rirnb1 :=acbyhvs4.rirnb1;"""
        """  acbyhv4.rirnb2 :=acbyhvs4.rirnb2;"""
        """  vary,name=acbchv5.lirnb1, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv5.lirnb2, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv5.rirnb1, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv5.rirnb2, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.lirnb1, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.lirnb2, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.rirnb1, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """  vary,name=acbchv6.rirnb2, step=1.0E-12,lower=-CLIMmcbc,upper=CLIMmcbc;"""
        """"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """  tar_on_ccsirnhv=tar;"""
        """"""
        """fill,table=knob_on_ccslirnhvb1,scale=1/off;"""
        """fill,table=knob_on_ccsrirnhvb1,scale=1/off;"""
        """fill,table=knob_on_ccslirnhvb2,scale=1/off;"""
        """fill,table=knob_on_ccsrirnhvb2,scale=1/off;"""
        """setvars_const,table=knob_on_ccslirnhvb1;"""
        """setvars_const,table=knob_on_ccsrirnhvb1;"""
        """setvars_const,table=knob_on_ccslirnhvb2;"""
        """setvars_const,table=knob_on_ccsrirnhvb2;"""
        """};"""
    )

    mad.input("exec,mktable_orbit15(1,h,x,orbit_ir1h);")
    mad.input("exec,mktable_orbit15(1,v,y,orbit_ir1v);")
    mad.input("exec,mktable_orbit15(5,h,x,orbit_ir5h);")
    mad.input("exec,mktable_orbit15(5,v,y,orbit_ir5v);")

    mad.input("setvars_const,table=orbit_ir1h;")
    mad.input("setvars_const,table=orbit_ir1v;")
    mad.input("setvars_const,table=orbit_ir5h;")
    mad.input("setvars_const,table=orbit_ir5v;")

    mad.input("cd2q4=1;")
    mad.input("cmcb12=1;")
    mad.input("exec,match_xing(1,h,x,295,l);")
    mad.input("exec,match_xing(1,v,y,295,l);")
    mad.input("exec,match_xing(5,h,x,295,l);")
    mad.input("exec,match_xing(5,v,y,295,l);")

    mad.input("cd2q4=0;")
    mad.input("cmcb12=1;")
    mad.input("exec,match_xing(1,h,x,295,s);")
    mad.input("exec,match_xing(1,v,y,295,s);")
    mad.input("exec,match_xing(5,h,x,295,s);")
    mad.input("exec,match_xing(5,v,y,295,s);")

    mad.input("cd2q4=1;")
    mad.input("cmcb12=1;")
    mad.input("exec,match_psep(1,h,x,2);")
    mad.input("exec,match_psep(1,v,y,2);")
    mad.input("exec,match_psep(5,h,x,2);")
    mad.input("exec,match_psep(5,v,y,2);")

    mad.input("exec,match_aoff(1,h,x,1);")
    mad.input("exec,match_aoff(1,v,y,1);")
    mad.input("exec,match_aoff(5,h,x,1);")
    mad.input("exec,match_aoff(5,v,y,1);")

    mad.input("exec,match_ccpm(1,h,x,ccp);")
    mad.input("exec,match_ccpm(1,v,y,ccp);")
    mad.input("exec,match_ccpm(5,h,x,ccp);")
    mad.input("exec,match_ccpm(5,v,y,ccp);")

    mad.input("exec,match_ccpm(1,h,x,ccm);")
    mad.input("exec,match_ccpm(1,v,y,ccm);")
    mad.input("exec,match_ccpm(5,h,x,ccm);")
    mad.input("exec,match_ccpm(5,v,y,ccm);")

    mad.input("exec,match_ccs(1,h,x);")
    mad.input("exec,match_ccs(1,v,y);")
    mad.input("exec,match_ccs(5,h,x);")
    mad.input("exec,match_ccs(5,v,y);")

    mad.input("if (q4x==0){q4x=-0.00075;};")
    mad.input("if (q4y==0){q4y=-0.00075;};")
    mad.input("ipoff=0.5;")

    mad.input("exec,match_off(1,h,x,ipoff);")
    mad.input("exec,match_off(1,v,y,ipoff);")
    mad.input("exec,match_off(5,h,x,ipoff);")
    mad.input("exec,match_off(5,v,y,ipoff);")

    mad.input("setvars_knob, table=knob_on_x1hs, knob=on_x1hs;")
    mad.input("setvars_knob, table=knob_on_x1vs, knob=on_x1vs;")
    mad.input("setvars_knob, table=knob_on_x1hl, knob=on_x1hl;")
    mad.input("setvars_knob, table=knob_on_x1vl, knob=on_x1vl;")
    mad.input("setvars_knob, table=knob_on_x5hs, knob=on_x5hs;")
    mad.input("setvars_knob, table=knob_on_x5vs, knob=on_x5vs;")
    mad.input("setvars_knob, table=knob_on_x5hl, knob=on_x5hl;")
    mad.input("setvars_knob, table=knob_on_x5vl, knob=on_x5vl;")

    mad.input("setvars_knob, table=knob_on_sep1h, knob=on_sep1h;")
    mad.input("setvars_knob, table=knob_on_sep1v, knob=on_sep1v;")
    mad.input("setvars_knob, table=knob_on_sep5h, knob=on_sep5h;")
    mad.input("setvars_knob, table=knob_on_sep5v, knob=on_sep5v;")

    mad.input("setvars_knob, table=knob_on_o1h, knob=on_o1h;")
    mad.input("setvars_knob, table=knob_on_o1v, knob=on_o1v;")
    mad.input("setvars_knob, table=knob_on_o5h, knob=on_o5h;")
    mad.input("setvars_knob, table=knob_on_o5v, knob=on_o5v;")

    mad.input("setvars_knob, table=knob_on_a1h, knob=on_a1h;")
    mad.input("setvars_knob, table=knob_on_a1v, knob=on_a1v;")
    mad.input("setvars_knob, table=knob_on_a5h, knob=on_a5h;")
    mad.input("setvars_knob, table=knob_on_a5v, knob=on_a5v;")

    mad.input("setvars_knob, table=knob_on_ccpl1h, knob=on_ccpl1h;")
    mad.input("setvars_knob, table=knob_on_ccpl1v, knob=on_ccpl1v;")
    mad.input("setvars_knob, table=knob_on_ccpl5h, knob=on_ccpl5h;")
    mad.input("setvars_knob, table=knob_on_ccpl5v, knob=on_ccpl5v;")
    mad.input("setvars_knob, table=knob_on_ccpr1h, knob=on_ccpr1h;")
    mad.input("setvars_knob, table=knob_on_ccpr1v, knob=on_ccpr1v;")
    mad.input("setvars_knob, table=knob_on_ccpr5h, knob=on_ccpr5h;")
    mad.input("setvars_knob, table=knob_on_ccpr5v, knob=on_ccpr5v;")

    mad.input("setvars_knob, table=knob_on_ccml1h, knob=on_ccml1h;")
    mad.input("setvars_knob, table=knob_on_ccml1v, knob=on_ccml1v;")
    mad.input("setvars_knob, table=knob_on_ccml5h, knob=on_ccml5h;")
    mad.input("setvars_knob, table=knob_on_ccml5v, knob=on_ccml5v;")
    mad.input("setvars_knob, table=knob_on_ccmr1h, knob=on_ccmr1h;")
    mad.input("setvars_knob, table=knob_on_ccmr1v, knob=on_ccmr1v;")
    mad.input("setvars_knob, table=knob_on_ccmr5h, knob=on_ccmr5h;")
    mad.input("setvars_knob, table=knob_on_ccmr5v, knob=on_ccmr5v;")

    mad.input("setvars_knob, table=knob_on_ccsl1hb1, knob=on_ccsl1hb1;")
    mad.input("setvars_knob, table=knob_on_ccsl1vb1, knob=on_ccsl1vb1;")
    mad.input("setvars_knob, table=knob_on_ccsl5hb1, knob=on_ccsl5hb1;")
    mad.input("setvars_knob, table=knob_on_ccsl5vb1, knob=on_ccsl5vb1;")
    mad.input("setvars_knob, table=knob_on_ccsr1hb1, knob=on_ccsr1hb1;")
    mad.input("setvars_knob, table=knob_on_ccsr1vb1, knob=on_ccsr1vb1;")
    mad.input("setvars_knob, table=knob_on_ccsr5hb1, knob=on_ccsr5hb1;")
    mad.input("setvars_knob, table=knob_on_ccsr5vb1, knob=on_ccsr5vb1;")

    mad.input("setvars_knob, table=knob_on_ccsl1hb2, knob=on_ccsl1hb2;")
    mad.input("setvars_knob, table=knob_on_ccsl1vb2, knob=on_ccsl1vb2;")
    mad.input("setvars_knob, table=knob_on_ccsl5hb2, knob=on_ccsl5hb2;")
    mad.input("setvars_knob, table=knob_on_ccsl5vb2, knob=on_ccsl5vb2;")
    mad.input("setvars_knob, table=knob_on_ccsr1hb2, knob=on_ccsr1hb2;")
    mad.input("setvars_knob, table=knob_on_ccsr1vb2, knob=on_ccsr1vb2;")
    mad.input("setvars_knob, table=knob_on_ccsr5hb2, knob=on_ccsr5hb2;")
    mad.input("setvars_knob, table=knob_on_ccsr5vb2, knob=on_ccsr5vb2;")

    mad.input("show, xip1b1, xip1b2, xip5b1, xip5b2;")
    mad.input("show, pxip1b1, pxip1b2, pxip5b1, pxip5b2;")
    mad.input("show, yip1b1, yip1b2, yip5b1, yip5b2;")
    mad.input("show, pyip1b1, pyip1b2, pyip5b1, pyip5b2;")

    mad.input("exec,_save_optics_orbconf15(1);")
    mad.input("exec,_save_optics_orbconf15(5);")

    mad.input("value, tar_on_x1hs, tar_on_x1vs, tar_on_x1hl, tar_on_x1vl;")
    mad.input("value, tar_on_x5hs, tar_on_x5vs, tar_on_x5hl, tar_on_x5vl;")
    mad.input("value, tar_on_sep1h, tar_on_sep1v, tar_on_sep5h, tar_on_sep5v;")
    mad.input("value, tar_on_a1h, tar_on_a1v, tar_on_a5h, tar_on_a5v;")
    mad.input("value, tar_on_o1h, tar_on_o1v, tar_on_o5h, tar_on_o5v;")
    mad.input("value, tar_on_ccp1h, tar_on_ccp1v, tar_on_ccp5h, tar_on_ccp5v;")
    mad.input("value, tar_on_ccm1h, tar_on_ccm1v, tar_on_ccm5h, tar_on_ccm5v;")
    mad.input("value, tar_on_ccs1h, tar_on_ccs1v, tar_on_ccs5h, tar_on_ccs5v;")

    mad.input(
        """tar_xing_ir15=tar_on_sep1h+tar_on_sep1v+tar_on_sep5h+tar_on_sep5v+"""
        """tar_on_x1hs+tar_on_x1vs+tar_on_x5hs+tar_on_x5vs+"""
        """tar_on_x1hl+tar_on_x1vl+tar_on_x5hl+tar_on_x5vl+"""
        """tar_on_a1h+tar_on_a1v+tar_on_a5h+tar_on_a5v+"""
        """tar_on_o1h+tar_on_o1v+tar_on_o5h+tar_on_o5v+"""
        """tar_on_ccp1h+tar_on_ccp1v+tar_on_ccp5h+tar_on_ccp5v+"""
        """tar_on_ccm1h+tar_on_ccm1v+tar_on_ccm5h+tar_on_ccm5v+"""
        """tar_on_ccs1h+tar_on_ccs1v+tar_on_ccs5h+tar_on_ccs5v;"""
    )

    mad.input("value, tar_xing_ir15;")

    return mad.globals.tar_xing_ir15


def rematch_xing_ir28(mad, on_holdselect=None):
    if on_holdselect is not None:
        mad.input(f"on_holdselect={on_holdselect};")

    mad.input("scale = 23348.89927;")

    mad.input("limitMCBXH:=  63.5988e-6;")
    mad.input("limitMCBXV:=  67.0164e-6;")
    mad.input("limitMCBX :=  67.0164e-6;")
    mad.input("limitMCBY :=  96.3000e-6;")
    mad.input("limitMCB  :=  80.8000e-6;")
    mad.input("limitMCBC :=  89.8700e-6;")
    mad.input("limitMCBW :=  80.1400e-6;")

    if mad.globals.on_holdselect == 0:
        mad.input("exec,select(2,12,23,b1);")
        mad.input("exec,select(2,12,23,b2);")
        mad.input("exec,select(8,78,81,b1);")
        mad.input("exec,select(8,78,81,b2);")

    mad.input(
        """mktable_orbit2h(kkkk): macro={"""
        """delete,table=kkkk;"""
        """create,table=kkkk,"""
        """       column=acbxh1.l2,acbxh1.r2,"""
        """              acbxh2.l2,acbxh2.r2,"""
        """              acbxh3.l2,acbxh3.r2,"""
        """              acbyhs4.l2b1,acbyhs4.l2b2,acbyhs4.r2b1,acbyhs4.r2b2,"""
        """              acbyh4.l2b2,acbyh4.r2b1,"""
        """              acbyhs5.l2b1,acbyhs5.l2b2,acbchs5.r2b1,acbchs5.r2b2,"""
        """              acbyh5.l2b1,acbch5.r2b2,"""
        """              xip2b1,xip2b2,pxip2b1,pxip2b2;"""
        """};"""
    )

    mad.input(
        """mktable_orbit2v(kkkk): macro={"""
        """delete,table=kkkk;"""
        """create,table=kkkk,"""
        """       column=acbxv1.l2,acbxv1.r2,"""
        """              acbxv2.l2,acbxv2.r2,"""
        """              acbxv3.l2,acbxv3.r2,"""
        """              acbyvs4.l2b1,acbyvs4.l2b2,acbyvs4.r2b1,acbyvs4.r2b2,"""
        """              acbyv4.l2b1,acbyv4.r2b2,"""
        """              acbyvs5.l2b1,acbyvs5.l2b2,acbcvs5.r2b1,acbcvs5.r2b2,"""
        """              acbcv5.r2b1,acbyv5.l2b2,"""
        """              yip2b1,yip2b2,pyip2b1,pyip2b2;"""
        """};"""
    )

    mad.input(
        """mktable_orbit8h(kkkk): macro={"""
        """delete,table=kkkk;"""
        """create,table=kkkk,"""
        """       column=acbxh1.l8,acbxh1.r8,"""
        """              acbxh2.l8,acbxh2.r8,"""
        """              acbxh3.l8,acbxh3.r8,"""
        """              acbyhs4.l8b1,acbyhs4.l8b2,acbyhs4.r8b1,acbyhs4.r8b2,"""
        """              acbyh4.l8b2,acbyh4.r8b1,"""
        """              acbchs5.l8b1,acbchs5.l8b2,acbyhs5.r8b1,acbyhs5.r8b2,"""
        """              acbch5.l8b1,acbyh5.r8b2,"""
        """              xip8b1,xip8b2,pxip8b1,pxip8b2;"""
        """};"""
    )

    mad.input(
        """mktable_orbit8v(kkkk): macro={"""
        """delete,table=kkkk;"""
        """create,table=kkkk,"""
        """       column=acbxv1.l8,acbxv1.r8,"""
        """              acbxv2.l8,acbxv2.r8,"""
        """              acbxv3.l8,acbxv3.r8,"""
        """              acbyvs4.l8b1,acbyvs4.l8b2,acbyvs4.r8b1,acbyvs4.r8b2,"""
        """              acbyv4.l8b1,acbyv4.r8b2,"""
        """              acbcvs5.l8b1,acbcvs5.l8b2,acbyvs5.r8b1,acbyvs5.r8b2,"""
        """              acbyv5.r8b1,acbcv5.l8b2,"""
        """              yip8b1,yip8b2,pyip8b1,pyip8b2;"""
        """};"""
    )

    mad.input(
        """match_orbit_ir2h(xip2b1,xip2b2,pxip2b1,pxip2b2): macro={"""
        """acbyhs4.l2b1=0; acbyhs4.r2b2=0; acbyhs4.l2b2=0; acbyhs4.r2b1=0;"""
        """acbyhs5.l2b2=0; acbyhs5.l2b1=0; acbchs5.r2b1=0; acbchs5.r2b2=0;"""
        """use, sequence= lhcb1,range=s.ds.l2.b1/e.ds.r2.b1;"""
        """use, sequence= lhcb2,range=s.ds.l2.b2/e.ds.r2.b2;"""
        """match, sequence=lhcb1,lhcb2, beta0= bir2b1,bir2b2,"""
        """        x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=IP2,  x = xip2b1, px = pxip2b1;"""
        """  constraint, sequence=lhcb1,range=E.DS.R2.B1,x = 0.0, px = 0.0;"""
        """  constraint, sequence=lhcb2,range=IP2,  x = xip2b2, px = pxip2b2;"""
        """  constraint, sequence=lhcb2,range=E.DS.R2.B2,x = 0.0, px = 0.0;"""
        """  vary,name=acbyhs4.l2b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyhs4.r2b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyhs4.l2b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyhs4.r2b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyhs5.l2b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyhs5.l2b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbchs5.r2b1,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """  vary,name=acbchs5.r2b2,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """};"""
    )

    mad.input(
        """match_orbit_ir2v(yip2b1,yip2b2,pyip2b1,pyip2b2): macro={"""
        """acbyvs4.l2b1=0; acbyvs4.r2b2=0; acbyvs4.l2b2=0; acbyvs4.r2b1=0;"""
        """acbyvs5.l2b2=0; acbyvs5.l2b1=0; acbcvs5.r2b1=0; acbcvs5.r2b2=0;"""
        """use, sequence= lhcb1,range=s.ds.l2.b1/e.ds.r2.b1;"""
        """use, sequence= lhcb2,range=s.ds.l2.b2/e.ds.r2.b2;"""
        """match, sequence=lhcb1,lhcb2, beta0= bir2b1,bir2b2,"""
        """        x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=IP2,  y = yip2b1, py = pyip2b1;"""
        """  constraint, sequence=lhcb1,range=E.DS.R2.B1,y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb2,range=IP2,  y = yip2b2, py = pyip2b2;"""
        """  constraint, sequence=lhcb2,range=E.DS.R2.B2,y = 0.0, py = 0.0;"""
        """  vary,name=acbyvs4.l2b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyvs4.r2b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyvs4.l2b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyvs4.r2b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyvs5.l2b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyvs5.l2b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbcvs5.r2b1,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """  vary,name=acbcvs5.r2b2,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """};"""
    )

    mad.input(
        """match_orbit_ir8h(xip8b1,xip8b2,pxip8b1,pxip8b2): macro={"""
        """acbyhs4.l8b1=0; acbyhs4.r8b2=0; acbyhs4.l8b2=0; acbyhs4.r8b1=0;"""
        """acbchs5.l8b2=0; acbchs5.l8b1=0; acbyhs5.r8b1=0; acbyhs5.r8b2=0;"""
        """use, sequence= lhcb1,range=s.ds.l8.b1/e.ds.r8.b1;"""
        """use, sequence= lhcb2,range=s.ds.l8.b2/e.ds.r8.b2;"""
        """match, sequence=lhcb1,lhcb2, beta0= bir8b1,bir8b2,"""
        """        x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=IP8,  x = xip8b1, px = pxip8b1;"""
        """  constraint, sequence=lhcb1,range=E.DS.R8.B1,x = 0.0, px = 0.0;"""
        """  constraint, sequence=lhcb2,range=IP8,  x = xip8b2, px = pxip8b2;"""
        """  constraint, sequence=lhcb2,range=E.DS.R8.B2,x = 0.0, px = 0.0;"""
        """  vary,name=acbyhs4.l8b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyhs4.r8b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyhs4.l8b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyhs4.r8b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbchs5.l8b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbchs5.l8b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyhs5.r8b1,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """  vary,name=acbyhs5.r8b2,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """  if (tar>1e-10){"""
        """    ACBXX.IR8=0.0e-6/170;exec,set_mcbx8;"""
        """    acbyhs4.l8b1=0; acbyhs4.r8b2=0; acbyhs4.l8b2=0; acbyhs4.r8b1=0;"""
        """    acbchs5.l8b2=0; acbchs5.l8b1=0; acbyhs5.r8b1=0; acbyhs5.r8b2=0;"""
        """    use, sequence= lhcb1,range=s.ds.l8.b1/e.ds.r8.b1;"""
        """    use, sequence= lhcb2,range=s.ds.l8.b2/e.ds.r8.b2;"""
        """    match, sequence=lhcb1,lhcb2, beta0= bir8b1,bir8b2,"""
        """          x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """      constraint, sequence=lhcb1,range=IP8,  x = xip8b1, px = pxip8b1;"""
        """      constraint, sequence=lhcb1,range=E.DS.R8.B1,x = 0.0, px = 0.0;"""
        """      constraint, sequence=lhcb2,range=IP8,  x = xip8b2, px = pxip8b2;"""
        """      constraint, sequence=lhcb2,range=E.DS.R8.B2,x = 0.0, px = 0.0;"""
        """      vary,name=acbyhs4.l8b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """      vary,name=acbyhs4.r8b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """      vary,name=acbyhs4.l8b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """      vary,name=acbyhs4.r8b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """      vary,name=acbchs5.l8b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """      vary,name=acbchs5.l8b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """      vary,name=acbyhs5.r8b1,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """      vary,name=acbyhs5.r8b2,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """      ACBXH3.L8:=ACBXH1.L8;"""
        """      ACBXH2.L8:=ACBXH1.L8;"""
        """      ACBXH3.R8:=ACBXH1.R8;"""
        """      ACBXH2.R8:=ACBXH1.R8;"""
        """      ACBXH1.R8:=-ACBXH1.L8;"""
        """      !vary,name=ACBXH1.R8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """      !vary,name=ACBXH2.R8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """      !vary,name=ACBXH3.R8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """      vary,name=ACBXH1.L8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """      !vary,name=ACBXH2.L8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """      !vary,name=ACBXH3.L8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """      jacobian, calls = 20, tolerance=1.e-30,bisec=3;"""
        """    endmatch;"""
        """  };"""
        """};"""
    )

    mad.input(
        """match_orbit_ir8v(yip8b1,yip8b2,pyip8b1,pyip8b2): macro={"""
        """acbyvs4.l8b1=0; acbyvs4.r8b2=0; acbyvs4.l8b2=0; acbyvs4.r8b1=0;"""
        """acbcvs5.l8b2=0; acbcvs5.l8b1=0; acbyvs5.r8b1=0; acbyvs5.r8b2=0;"""
        """use, sequence= lhcb1,range=s.ds.l8.b1/e.ds.r8.b1;"""
        """use, sequence= lhcb2,range=s.ds.l8.b2/e.ds.r8.b2;"""
        """match, sequence=lhcb1,lhcb2, beta0= bir8b1,bir8b2,"""
        """        x = 0.0, px = 0.0, y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb1,range=IP8,  y = yip8b1, py = pyip8b1;"""
        """  constraint, sequence=lhcb1,range=E.DS.R8.B1,y = 0.0, py = 0.0;"""
        """  constraint, sequence=lhcb2,range=IP8,  y = yip8b2, py = pyip8b2;"""
        """  constraint, sequence=lhcb2,range=E.DS.R8.B2,y = 0.0, py = 0.0;"""
        """  vary,name=acbyvs4.l8b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyvs4.r8b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyvs4.l8b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyvs4.r8b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbcvs5.l8b2,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """  vary,name=acbcvs5.l8b1,step=1.0e-15,lower=-limitmcbc,upper=limitmcbc;"""
        """  vary,name=acbyvs5.r8b1,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  vary,name=acbyvs5.r8b2,step=1.0e-15,lower=-limitmcby,upper=limitmcby;"""
        """  jacobian, calls = 10, tolerance=1.e-30,bisec=3;"""
        """  vary,name=ACBXV1.R8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """  vary,name=ACBXV2.R8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """  vary,name=ACBXV3.R8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """  vary,name=ACBXV1.L8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """  vary,name=ACBXV2.L8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """  vary,name=ACBXV3.L8,step=1.0e-15,lower=-limitmcbx,upper=limitmcbx;"""
        """  jacobian, calls = 20, tolerance=1.e-30,bisec=3;"""
        """endmatch;"""
        """};"""
    )

    mad.input(
        """mkknob(ssss,kkkk): macro={"""
        """  exec,match_orbit_ssss;"""
        """  tar_kkkk=tar;"""
        """  fill,table=knob_kkkk,scale=1/kkkk;"""
        """  kkkk=0;"""
        """};"""
    )

    mad.input(
        """set_mcbx2(ACBXX.IR2,ACBXS.IR2): macro={"""
        """ACBXH1.L2 := ACBXS.IR2 * on_sep2h + ACBXX.IR2 * on_x2h  ;"""
        """ACBXH2.L2 := ACBXS.IR2 * on_sep2h + ACBXX.IR2 * on_x2h  ;"""
        """ACBXH3.L2 := ACBXS.IR2 * on_sep2h + ACBXX.IR2 * on_x2h  ;"""
        """ACBXV1.L2 := ACBXX.IR2 * on_x2v   + ACBXS.IR2 * on_sep2v;"""
        """ACBXV2.L2 := ACBXX.IR2 * on_x2v   + ACBXS.IR2 * on_sep2v;"""
        """ACBXV3.L2 := ACBXX.IR2 * on_x2v   + ACBXS.IR2 * on_sep2v;"""
        """ACBXH1.R2 := ACBXS.IR2 * on_sep2h - ACBXX.IR2 * on_x2h  ;"""
        """ACBXH2.R2 := ACBXS.IR2 * on_sep2h - ACBXX.IR2 * on_x2h  ;"""
        """ACBXH3.R2 := ACBXS.IR2 * on_sep2h - ACBXX.IR2 * on_x2h  ;"""
        """ACBXV1.R2 :=-ACBXX.IR2 * on_x2v   + ACBXS.IR2 * on_sep2v;"""
        """ACBXV2.R2 :=-ACBXX.IR2 * on_x2v   + ACBXS.IR2 * on_sep2v;"""
        """ACBXV3.R2 :=-ACBXX.IR2 * on_x2v   + ACBXS.IR2 * on_sep2v;"""
        """};"""
    )

    mad.input(
        """set_mcbx8(ACBXX.IR8,ACBXS.IR8): macro={"""
        """ACBXH1.L8 := ACBXX.IR8 * on_x8h   + ACBXS.IR8 * on_sep8h;"""
        """ACBXH2.L8 := ACBXX.IR8 * on_x8h   + ACBXS.IR8 * on_sep8h;"""
        """ACBXH3.L8 := ACBXX.IR8 * on_x8h   + ACBXS.IR8 * on_sep8h;"""
        """ACBXV1.L8 := ACBXS.IR8 * on_sep8v + ACBXX.IR8 * on_x8v  ;"""
        """ACBXV2.L8 := ACBXS.IR8 * on_sep8v + ACBXX.IR8 * on_x8v  ;"""
        """ACBXV3.L8 := ACBXS.IR8 * on_sep8v + ACBXX.IR8 * on_x8v  ;"""
        """ACBXH1.R8 :=-ACBXX.IR8 * on_x8h   + ACBXS.IR8 * on_sep8h;"""
        """ACBXH2.R8 :=-ACBXX.IR8 * on_x8h   + ACBXS.IR8 * on_sep8h;"""
        """ACBXH3.R8 :=-ACBXX.IR8 * on_x8h   + ACBXS.IR8 * on_sep8h;"""
        """ACBXV1.R8 := ACBXS.IR8 * on_sep8v - ACBXX.IR8 * on_x8v  ;"""
        """ACBXV2.R8 := ACBXS.IR8 * on_sep8v - ACBXX.IR8 * on_x8v  ;"""
        """ACBXV3.R8 := ACBXS.IR8 * on_sep8v - ACBXX.IR8 * on_x8v  ;"""
        """};"""
    )

    mad.input(
        """delete,table=knob_on_x2h;"""
        """create,table=knob_on_x2h,"""
        """       column=acbxh1.l2,acbxh1.r2,"""
        """              acbxh2.l2,acbxh2.r2,"""
        """              acbxh3.l2,acbxh3.r2,"""
        """              acbyhs4.l2b1,acbyhs4.l2b2,acbyhs4.r2b1,acbyhs4.r2b2,"""
        """              acbyhs5.l2b1,acbyhs5.l2b2,acbchs5.r2b1,acbchs5.r2b2,"""
        """              pxip2b1,pxip2b2;"""
    )

    mad.input(
        """delete,table=knob_on_a2h;"""
        """create,table=knob_on_a2h,"""
        """       column=acbyhs4.l2b1,acbyhs4.l2b2,acbyhs4.r2b1,acbyhs4.r2b2,"""
        """              acbyhs5.l2b1,acbyhs5.l2b2,acbchs5.r2b1,acbchs5.r2b2,"""
        """              pxip2b1,pxip2b2;"""
    )

    mad.input(
        """delete,table=knob_on_sep2h;"""
        """create,table=knob_on_sep2h,"""
        """       column=acbxh1.l2,acbxh1.r2,"""
        """              acbxh2.l2,acbxh2.r2,"""
        """              acbxh3.l2,acbxh3.r2,"""
        """              acbyhs4.l2b1,acbyhs4.l2b2,acbyhs4.r2b1,acbyhs4.r2b2,"""
        """              acbyhs5.l2b1,acbyhs5.l2b2,acbchs5.r2b1,acbchs5.r2b2,"""
        """              xip2b1,xip2b2;"""
    )

    mad.input(
        """delete,table=knob_on_o2h;"""
        """create,table=knob_on_o2h,"""
        """       column=acbyhs4.l2b1,acbyhs4.l2b2,acbyhs4.r2b1,acbyhs4.r2b2,"""
        """              acbyhs5.l2b1,acbyhs5.l2b2,acbchs5.r2b1,acbchs5.r2b2,"""
        """              xip2b1,xip2b2;"""
    )

    mad.input(
        """delete,table=knob_on_x2v;"""
        """create,table=knob_on_x2v,"""
        """       column=acbxv1.l2,acbxv1.r2,"""
        """              acbxv2.l2,acbxv2.r2,"""
        """              acbxv3.l2,acbxv3.r2,"""
        """              acbyvs4.l2b1,acbyvs4.l2b2,acbyvs4.r2b1,acbyvs4.r2b2,"""
        """              acbyvs5.l2b1,acbyvs5.l2b2,acbcvs5.r2b1,acbcvs5.r2b2,"""
        """              pyip2b1,pyip2b2;"""
    )

    mad.input(
        """delete,table=knob_on_a2v;"""
        """create,table=knob_on_a2v,"""
        """       column=acbyvs4.l2b1,acbyvs4.l2b2,acbyvs4.r2b1,acbyvs4.r2b2,"""
        """              acbyvs5.l2b1,acbyvs5.l2b2,acbcvs5.r2b1,acbcvs5.r2b2,"""
        """              pyip2b1,pyip2b2;"""
    )

    mad.input(
        """delete,table=knob_on_sep2v;"""
        """create,table=knob_on_sep2v,"""
        """       column=acbxv1.l2,acbxv1.r2,"""
        """              acbxv2.l2,acbxv2.r2,"""
        """              acbxv3.l2,acbxv3.r2,"""
        """              acbyvs4.l2b1,acbyvs4.l2b2,acbyvs4.r2b1,acbyvs4.r2b2,"""
        """              acbyvs5.l2b1,acbyvs5.l2b2,acbcvs5.r2b1,acbcvs5.r2b2,"""
        """              yip2b1,yip2b2;"""
    )

    mad.input(
        """delete,table=knob_on_o2v;"""
        """create,table=knob_on_o2v,"""
        """       column=acbyvs4.l2b1,acbyvs4.l2b2,acbyvs4.r2b1,acbyvs4.r2b2,"""
        """              acbyvs5.l2b1,acbyvs5.l2b2,acbcvs5.r2b1,acbcvs5.r2b2,"""
        """              yip2b1,yip2b2;"""
    )

    mad.input(
        """delete,table=knob_on_x8h;"""
        """create,table=knob_on_x8h,"""
        """       column=acbxh1.l8,acbxh1.r8,"""
        """              acbxh2.l8,acbxh2.r8,"""
        """              acbxh3.l8,acbxh3.r8,"""
        """              acbyhs4.l8b1,acbyhs4.l8b2,acbyhs4.r8b1,acbyhs4.r8b2,"""
        """              acbchs5.l8b1,acbchs5.l8b2,acbyhs5.r8b1,acbyhs5.r8b2,"""
        """              pxip8b1,pxip8b2;"""
    )

    mad.input(
        """delete,table=knob_on_a8h;"""
        """create,table=knob_on_a8h,"""
        """       column=acbyhs4.l8b1,acbyhs4.l8b2,acbyhs4.r8b1,acbyhs4.r8b2,"""
        """              acbchs5.l8b1,acbchs5.l8b2,acbyhs5.r8b1,acbyhs5.r8b2,"""
        """              pxip8b1,pxip8b2;"""
    )

    mad.input(
        """delete,table=knob_on_sep8h;"""
        """create,table=knob_on_sep8h,"""
        """       column=acbxh1.l8,acbxh1.r8,"""
        """              acbxh2.l8,acbxh2.r8,"""
        """              acbxh3.l8,acbxh3.r8,"""
        """              acbyhs4.l8b1,acbyhs4.l8b2,acbyhs4.r8b1,acbyhs4.r8b2,"""
        """              acbchs5.l8b1,acbchs5.l8b2,acbyhs5.r8b1,acbyhs5.r8b2,"""
        """              xip8b1,xip8b2;"""
    )

    mad.input(
        """delete,table=knob_on_o8h;"""
        """create,table=knob_on_o8h,"""
        """       column=acbyhs4.l8b1,acbyhs4.l8b2,acbyhs4.r8b1,acbyhs4.r8b2,"""
        """              acbchs5.l8b1,acbchs5.l8b2,acbyhs5.r8b1,acbyhs5.r8b2,"""
        """              xip8b1,xip8b2;"""
    )

    mad.input(
        """delete,table=knob_on_x8v;"""
        """create,table=knob_on_x8v,"""
        """       column=acbxv1.l8,acbxv1.r8,"""
        """              acbxv2.l8,acbxv2.r8,"""
        """              acbxv3.l8,acbxv3.r8,"""
        """              acbyvs4.l8b1,acbyvs4.l8b2,acbyvs4.r8b1,acbyvs4.r8b2,"""
        """              acbcvs5.l8b1,acbcvs5.l8b2,acbyvs5.r8b1,acbyvs5.r8b2,"""
        """              pyip8b1,pyip8b2;"""
    )

    mad.input(
        """delete,table=knob_on_a8v;"""
        """create,table=knob_on_a8v,"""
        """       column=acbyvs4.l8b1,acbyvs4.l8b2,acbyvs4.r8b1,acbyvs4.r8b2,"""
        """              acbcvs5.l8b1,acbcvs5.l8b2,acbyvs5.r8b1,acbyvs5.r8b2,"""
        """              pyip8b1,pyip8b2;"""
    )

    mad.input(
        """delete,table=knob_on_sep8v;"""
        """create,table=knob_on_sep8v,"""
        """       column=acbxv1.l8,acbxv1.r8,"""
        """              acbxv2.l8,acbxv2.r8,"""
        """              acbxv3.l8,acbxv3.r8,"""
        """              acbyvs4.l8b1,acbyvs4.l8b2,acbyvs4.r8b1,acbyvs4.r8b2,"""
        """              acbcvs5.l8b1,acbcvs5.l8b2,acbyvs5.r8b1,acbyvs5.r8b2,"""
        """              yip8b1,yip8b2;"""
    )

    mad.input(
        """delete,table=knob_on_o8v;"""
        """create,table=knob_on_o8v,"""
        """       column=acbyvs4.l8b1,acbyvs4.l8b2,acbyvs4.r8b1,acbyvs4.r8b2,"""
        """              acbcvs5.l8b1,acbcvs5.l8b2,acbyvs5.r8b1,acbyvs5.r8b2,"""
        """              yip8b1,yip8b2;"""
    )

    # reset values
    mad.input("exec, mktable_orbit2h(orbit_ir2h);")
    mad.input("exec, mktable_orbit2v(orbit_ir2v);")
    mad.input("exec, mktable_orbit8h(orbit_ir8h);")
    mad.input("exec, mktable_orbit8v(orbit_ir8v);")

    mad.input("setvars_const, table=orbit_ir2h;")
    mad.input("setvars_const, table=orbit_ir2v;")
    mad.input("setvars_const, table=orbit_ir8h;")
    mad.input("setvars_const, table=orbit_ir8v;")

    mad.input("testkqx2=abs(kqx.l2)*7000./0.3;")
    mad.input("testkqx8=abs(kqx.l8)*7000./0.3;")

    mad.input("xip2b1  := 1e-3*(on_o2h + on_sep2h);")
    mad.input("xip2b2  := 1e-3*(on_o2h - on_sep2h);")
    mad.input("yip2b1  := 1e-3*(on_o2v + on_sep2v);")
    mad.input("yip2b2  := 1e-3*(on_o2v - on_sep2v);")
    mad.input("pxip2b1 := 1e-6*(on_a2h + on_x2h  );")
    mad.input("pxip2b2 := 1e-6*(on_a2h - on_x2h  );")
    mad.input("pyip2b1 := 1e-6*(on_a2v + on_x2v  );")
    mad.input("pyip2b2 := 1e-6*(on_a2v - on_x2v  );")

    mad.input("xip8b1  := 1e-3*(on_o8h + on_sep8h);")
    mad.input("xip8b2  := 1e-3*(on_o8h - on_sep8h);")
    mad.input("yip8b1  := 1e-3*(on_o8v + on_sep8v);")
    mad.input("yip8b2  := 1e-3*(on_o8v - on_sep8v);")
    mad.input("pxip8b1 := 1e-6*(on_a8h + on_x8h  );")
    mad.input("pxip8b2 := 1e-6*(on_a8h - on_x8h  );")
    mad.input("pyip8b1 := 1e-6*(on_a8v + on_x8v  );")
    mad.input("pyip8b2 := 1e-6*(on_a8v - on_x8v  );")

    mad.input("if(testkqx2> 210.) {acbxx.ir2= 1.0e-6/170;acbxs.ir2= 18.0e-6/2;};")
    mad.input("if(testkqx8> 210.) {acbxx.ir8= 1.0e-6/170;acbxs.ir8= 18.0e-6/2;};")

    mad.input("if(testkqx2< 210.) {acbxx.ir2= 11.0e-6/170;acbxs.ir2= 16.0e-6/2;};")
    mad.input("if(testkqx8< 210.) {acbxx.ir8= 11.0e-6/170;acbxs.ir8= 16.0e-6/2;};")

    mad.input("exec,set_mcbx2;")

    mad.input("xang=170;")
    mad.input("psep=2;")
    mad.input("off=0.5;")
    mad.input("aoff=30;")

    mad.input("on_x2h=xang;")
    mad.input("exec,mkknob(ir2h,on_x2h);")

    mad.input("on_a2h=aoff;")
    mad.input("exec,mkknob(ir2h,on_a2h);")

    mad.input("on_x2v=xang;")
    mad.input("exec,mkknob(ir2v,on_x2v);")

    mad.input("on_a2v=aoff;")
    mad.input("exec,mkknob(ir2v,on_a2v);")

    mad.input("on_sep2h=psep;")
    mad.input("exec,mkknob(ir2h,on_sep2h);")

    mad.input("on_o2h=off;")
    mad.input("exec,mkknob(ir2h,on_o2h);")

    mad.input("on_sep2v=psep;")
    mad.input("exec,mkknob(ir2v,on_sep2v);")

    mad.input("on_o2v=off;")
    mad.input("exec,mkknob(ir2v,on_o2v);")

    mad.input("if (betxip8b1<5){xang=300;};")

    mad.input("exec,set_mcbx8;")
    mad.input("on_x8h=xang;")
    mad.input("exec,mkknob(ir8h,on_x8h);")

    mad.input("exec,set_mcbx8;")
    mad.input("on_a8h=aoff;")
    mad.input("exec,mkknob(ir8h,on_a8h);")

    mad.input("exec,set_mcbx8;")
    mad.input("on_x8v=xang;")
    mad.input("exec,mkknob(ir8v,on_x8v);")

    mad.input("exec,set_mcbx8; ")
    mad.input("on_a8v=aoff;")
    mad.input("exec,mkknob(ir8v,on_a8v);")

    mad.input("exec,set_mcbx8; ")
    mad.input("on_sep8h=psep;")
    mad.input("exec,mkknob(ir8h,on_sep8h);")

    mad.input("exec,set_mcbx8;")
    mad.input("on_o8h=off;")
    mad.input("exec,mkknob(ir8h,on_o8h);")

    mad.input("exec,set_mcbx8;")
    mad.input("on_sep8v=psep;")
    mad.input("exec,mkknob(ir8v,on_sep8v);")

    mad.input("exec,set_mcbx8;")
    mad.input("on_o8v=off;")
    mad.input("exec,mkknob(ir8v,on_o8v);")

    # setting knobs
    mad.input("setvars_const, table=orbit_ir2h;")
    mad.input("setvars_const, table=orbit_ir2v;")
    mad.input("setvars_const, table=orbit_ir8h;")
    mad.input("setvars_const, table=orbit_ir8v;")

    mad.input("setvars_knob, table=knob_on_x2h,   knob=on_x2h;")
    mad.input("setvars_knob, table=knob_on_x2v,   knob=on_x2v;")
    mad.input("setvars_knob, table=knob_on_x8h,   knob=on_x8h;")
    mad.input("setvars_knob, table=knob_on_x8v,   knob=on_x8v;")
    mad.input("setvars_knob, table=knob_on_sep2h, knob=on_sep2h;")
    mad.input("setvars_knob, table=knob_on_sep2v, knob=on_sep2v;")
    mad.input("setvars_knob, table=knob_on_sep8h, knob=on_sep8h;")
    mad.input("setvars_knob, table=knob_on_sep8v, knob=on_sep8v;")
    mad.input("setvars_knob, table=knob_on_o2h,   knob=on_o2h;")
    mad.input("setvars_knob, table=knob_on_o2v,   knob=on_o2v;")
    mad.input("setvars_knob, table=knob_on_o8h,   knob=on_o8h;")
    mad.input("setvars_knob, table=knob_on_o8v,   knob=on_o8v;")
    mad.input("setvars_knob, table=knob_on_a2h,   knob=on_a2h;")
    mad.input("setvars_knob, table=knob_on_a2v,   knob=on_a2v;")
    mad.input("setvars_knob, table=knob_on_a8h,   knob=on_a8h;")
    mad.input("setvars_knob, table=knob_on_a8v,   knob=on_a8v;")

    mad.input("value,tar_on_x2h,tar_on_x2v,tar_on_x8h,tar_on_x8v;")
    mad.input("value,tar_on_sep2h,tar_on_sep2v,tar_on_sep8h,tar_on_sep8v;")
    mad.input("value,tar_on_a2h,tar_on_a2v,tar_on_a8h,tar_on_a8v;")
    mad.input("value,tar_on_o2h,tar_on_o2v,tar_on_o8h,tar_on_o8v;")

    mad.input(
        """tar_xing_ir28=tar_on_sep2h+tar_on_sep2v+tar_on_sep8h+tar_on_sep8v+"""
        """tar_on_x2h+tar_on_x2v+tar_on_x8h+tar_on_x8v+"""
        """tar_on_a2h+tar_on_a2v+tar_on_a8h+tar_on_a8v+"""
        """tar_on_o2h+tar_on_o2v+tar_on_o8h+tar_on_o8v+"""
        """tar_on_ccp2h+tar_on_ccp2v+tar_on_ccp8h+tar_on_ccp8v;"""
    )

    mad.input("value, tar_xing_ir28;")

    mad.input("show, xip2b1, xip2b2, xip8b1, xip8b2;")
    mad.input("show, pxip2b1, pxip2b2, pxip8b1, pxip8b2;")
    mad.input("show, yip2b1, yip2b2, yip8b1, yip8b2;")
    mad.input("show, pyip2b1, pyip2b2, pyip8b1, pyip8b2;")

    mad.input("exec, _save_optics_orbconf28(2);")
    mad.input("exec, _save_optics_orbconf28(8);")

    return mad.globals.tar_xing_ir28
