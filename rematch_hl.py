# %%
from cpymad.madx import Madx
import sys
import os
import rematch_irs
import rematch_xing
import rematch_disp
from pyoptics import optics

# %%
ARC_NAMES = ["12", "23", "34", "45", "56", "67", "78", "81"]
BEAMS = ["lhcb1", "lhcb2"]


def rematch_hllhc(
    mad,
    betx_ip1=None,
    bety_ip1=None,
    betx_ip5=None,
    bety_ip5=None,
    betx0_ip1=None,
    bety0_ip1=None,
    betx0_ip5=None,
    bety0_ip5=None,
):
    mad.input("match_optics_only = 0;")

    if betx_ip1 is None and bety_ip1 is None and betx_ip5 is None and bety_ip5 is None:
        betx_ip1 = mad.globals.betx_IP1
        bety_ip1 = mad.globals.bety_IP1
        betx_ip5 = mad.globals.betx_IP5
        bety_ip5 = mad.globals.bety_IP5
    else:
        mad.globals.betx_IP1 = betx_ip1
        mad.globals.bety_IP1 = bety_ip1
        mad.globals.betx_IP5 = betx_ip5
        mad.globals.bety_IP5 = bety_ip5

    if (
        betx0_ip1 is None
        and bety0_ip1 is None
        and betx0_ip5 is None
        and bety0_ip5 is None
    ):
        betx0_ip1 = mad.globals.betx_IP1
        bety0_ip1 = mad.globals.bety_IP1
        betx0_ip5 = mad.globals.betx_IP5
        bety0_ip5 = mad.globals.bety_IP5
    else:
        mad.globals.betx0_IP1 = betx0_ip1
        mad.globals.bety0_IP1 = bety0_ip1
        mad.globals.betx0_IP5 = betx0_ip5
        mad.globals.bety0_IP5 = bety0_ip5

    mad.input("value, betx_IP1, bety_IP1, betx_IP5, bety_IP5;")
    mad.input("value, betx0_IP1, bety0_IP1, betx0_IP5, bety0_IP5;")

    mad.input("exec, round_phases;")

    for arc in ARC_NAMES:
        mad.input(f"exec,rematch_arc({arc[0]},{arc[1]},{arc});")

    mad.input("scxir1=betx_IP1/betx0_IP1;")
    mad.input("scyir1=bety_IP1/bety0_IP1;")
    mad.input("scxir5=betx_IP5/betx0_IP5;")
    mad.input("scyir5=bety_IP5/bety0_IP5;")

    mad.input("value,scxir1,scyir1,scxir5,scyir5;")

    mad.input("exec,selectIR15(5,45,56,b1);")
    mad.input("exec,selectIR15(5,45,56,b2);")
    mad.input("exec,selectIR15(1,81,12,b1);")
    mad.input("exec,selectIR15(1,81,12,b2);")

    mad.input("on_holdselect=1;")
    mad.input("jac_calls=25;")
    mad.input("jac_tol=1e-20; ")
    mad.input("jac_bisec=5;")

    mad.input("if (sc79==0){sc79=0.999;};")
    mad.input("if (sch==0) {sch =0.99; };")
    mad.input("if (imb==0) {imb=1.50;};")
    mad.input("if (scl==0) {scl=0.06;};")
    mad.input("grad=132.2; bmaxds=500;")
    mad.input("betxip5b1=betx0_IP5; betyip5b1=bety0_IP5;")
    mad.input("betxip5b2=betx0_IP5; betyip5b2=bety0_IP5;")
    mad.input("betxip1b1=betx0_IP1; betyip1b1=bety0_IP1;")
    mad.input("betxip1b2=betx0_IP1; betyip1b2=bety0_IP1;")

    # print(mad.globals.betxip5b1)
    # print(mad.globals.betyip5b1)
    # print(mad.globals.betxip1b1)
    # print(mad.globals.betyip1b1)
    # print(mad.globals.betxip5b2)
    # print(mad.globals.betyip5b2)
    # print(mad.globals.betxip1b2)
    # print(mad.globals.betyip1b2)

    tar_ir15_b12 = rematch_irs.rematch_ir15b12(mad, match_on_triplet=0)
    if tar_ir15_b12 > 1e-15:
        print("Failed to match IR15")
        return

    tar_ir15_b12_r2 = rematch_irs.rematch_ir15b12(mad)
    if tar_ir15_b12_r2 > 1e-15:
        print("Failed to match IR15")
        return

    mad.input("betxip5b1=betx_IP5; betyip5b1=bety_IP5;")
    mad.input("betxip5b2=betx_IP5; betyip5b2=bety_IP5;")
    mad.input("betxip1b1=betx_IP1; betyip1b1=bety_IP1;")
    mad.input("betxip1b2=betx_IP1; betyip1b2=bety_IP1;")

    mad.input("exec,select(3,23,34,b1);")
    mad.input("exec,select(3,23,34,b2);")
    mad.input("exec,select(7,67,78,b1);")
    mad.input("exec,select(7,67,78,b2);")

    tar_ir3_b12 = rematch_irs.rematch_ir3b12(mad)
    if tar_ir3_b12[0] > 1e-10:
        print("Failed to match IR3")
        return
    tar_ir7_b12 = rematch_irs.rematch_ir7b12(mad)
    if tar_ir7_b12[0] > 1e-10:
        print("Failed to match IR7")
        return

    mad.input("exec,selectIRAUX(7,8,1,2,3,b1,scxir1,scyir1,betx0_ip1,bety0_ip1);")
    mad.input("exec,selectIRAUX(7,8,1,2,3,b2,scxir1,scyir1,betx0_ip1,bety0_ip1);")
    mad.input("exec,selectIRAUX(3,4,5,6,7,b1,scxir5,scyir5,betx0_ip5,bety0_ip5);")
    mad.input("exec,selectIRAUX(3,4,5,6,7,b2,scxir5,scyir5,betx0_ip5,bety0_ip5);")

    tar_ir2_b12 = rematch_irs.rematch_ir2b12(mad)
    if tar_ir2_b12 > 1e-10:
        print("Failed to match IR2")
        return

    tar_ir8_b12 = rematch_irs.rematch_ir8b12(mad)
    if tar_ir8_b12 > 1e-10:
        print("Failed to match IR8")
        return

    tar_ir4_b1, tar_ir4_b2 = rematch_irs.rematch_ir4b12(mad)
    if tar_ir4_b1 > 1e-10 or tar_ir4_b2 > 1e-10:
        print("Failed to match IR4")
        return

    # mad.input("jac_calls=15;")
    # mad.input("dxip6b1=0;dpxip6b1=0; dxip6b2=0;dpxip6b2=0;")
    # mad.call("acc-models-lhc/toolkit/rematch_ir6b1m.madx")
    # mad.call("acc-models-lhc/toolkit/rematch_ir6b2m.madx")
    # mad.input("dxip6b1=0;dpxip6b1=0; dxip6b2=0;dpxip6b2=0;")

    tar_ir6_b1, tar_ir6_b2 = rematch_irs.rematch_ir6b12(
        mad, nomatch_dx=1, nomatch_dpx=1
    )
    if tar_ir6_b1 > 1e-10 or tar_ir6_b2 > 1e-10:
        print("Failed to match IR6")
        return

    # mad.input("nomatch_dx=0;")
    # mad.input("nomatch_dpx=0;")
    tar_ir6_b1, tar_ir6_b2 = rematch_irs.rematch_ir6b12(
        mad, nomatch_dx=0, nomatch_dpx=0
    )
    if tar_ir6_b1 > 1e-10 or tar_ir6_b1 > 1e-10:
        print("Failed to match IR6")
        return

    mad.input("value,tarir2b1,tarir4b1,tarir6b1,tarir8b1;")
    mad.input("value,tarir2b2,tarir4b2,tarir6b2,tarir8b2;")
    mad.input("value,tarir3b1,tarir7b1,tarir3b2,tarir7b2;")
    mad.input("value,tarir1b1,tarir5b1,tarir1b2,tarir5b2;")

    mad.input(
        """tarsqueeze=tarir2b1+tarir4b1+tarir6b1+tarir8b1+"""
        """tarir2b2+tarir4b2+tarir6b2+tarir8b2+"""
        """tarir3b1+tarir7b1+tarir3b2+tarir7b2+"""
        """tarir1b1+tarir5b1+tarir1b2+tarir5b2;"""
    )

    mad.input("exec, check_ip(b1);")
    mad.input("exec, check_ip(b2);")
    mad.input("value,tarsqueeze;")

    if mad.globals.tarsqueeze > 1e-15 or mad.globals.match_optics_only == 1:
        return

    # mad.input("if (tarsqueeze>1e-15 || match_optics_only==1){return;};")

    tar_xing_ir15 = rematch_xing.rematch_xing_ir15(mad)

    tar_xing_ir28 = rematch_xing.rematch_xing_ir28(mad)

    mad.input("value,tar_xing_ir15,tar_xing_ir28;")

    mad.input("ksf=0.06")
    mad.input("ksd=-0.099;")

    mad.input("exec,set_sext_all(ksf,ksd,ksf,ksd);")
    mad.input("k2max=0.38;")

    # if mad.globals["match_w2"] == 0:
    #     mad.call("acc-models-lhc/toolkit/rematch_w.madx")
    #     mad.call("acc-models-lhc/toolkit/rematch_disp.madx")
    # if mad.globals["match_w2"] == 2:
    #     mad.call("acc-models-lhc/toolkit/rematch_w2.madx")
    #     mad.call("acc-models-lhc/toolkit/rematch_disp2.madx")

    mad.input(
        """
        if (match_w2==0){
        call,file="acc-models-lhc/toolkit/rematch_w.madx";
        call,file="acc-models-lhc/toolkit/rematch_disp.madx";
        };
        if (match_w2==2){
        call,file="acc-models-lhc/toolkit/rematch_w2.madx";
        call,file="acc-models-lhc/toolkit/rematch_disp2.madx";
        };
    """
    )

    mad.call("acc-models-lhc/toolkit/rematch_crabs.madx")
    mad.call("acc-models-lhc/toolkit/mk_arc_trims.madx")

    mad.input("value,tarsqueeze, tar_xing_ir15, tar_xing_ir28, tar_on_disp;")


def load_stuff(mad):
    mad.input(
        """
            option,-echo,-info;
            call,file="acc-models-lhc/lhc.seq";
            call,file="acc-models-lhc/hllhc_sequence.madx";
            call,file="acc-models-lhc/toolkit/macro.madx";
            """
    )


def print_disp_tars(mad):
    print("Dispersion tars")
    mad.input(
        "value,tar_disp_x1hs_sep1v,tar_disp_x1vs_sep1h,tar_disp_x1hl_sep1v,tar_disp_x1vl_sep1h,"
        "tar_disp_x5hs_sep5v,tar_disp_x5vs_sep5h,tar_disp_x5hl_sep5v,tar_disp_x5vl_sep5h;"
    )

    mad.input("value,tar_on_disp;")
    print("\n\n")


def print_tars(mad):
    print("Tars")
    mad.input("value,tarsqueeze, tar_xing_ir15, tar_xing_ir28, tar_on_disp;")
    print("\n\n")


def print_tar_irs(mad):
    print("IR Tars")
    mad.input("value,tarir2b1,tarir4b1,tarir6b1,tarir8b1;")
    mad.input("value,tarir2b2,tarir4b2,tarir6b2,tarir8b2;")
    mad.input("value,tarir3b1,tarir7b1,tarir3b2,tarir7b2;")
    mad.input("value,tarir1b1,tarir5b1,tarir1b2,tarir5b2;")
    print("\n\n")


def print_xing15_tars(mad):
    print("xing 15 tars")
    mad.input(
        "value,tar_on_x1hs,tar_on_x1vs,tar_on_x1hl,tar_on_x1vl,tar_on_x5hs,tar_on_x5vs,tar_on_x5hl,tar_on_x5vl;"
    )
    mad.input("value,tar_on_sep1h,tar_on_sep1v,tar_on_sep5h,tar_on_sep5v;")
    mad.input("value,tar_on_a1h,tar_on_a1v,tar_on_a5h,tar_on_a5v;")
    mad.input("value,tar_on_o1h,tar_on_o1v,tar_on_o5h,tar_on_o5v;")
    mad.input("value,tar_on_ccp1h,tar_on_ccp1v,tar_on_ccp5h,tar_on_ccp5v;")
    mad.input("value,tar_on_ccm1h,tar_on_ccm1v,tar_on_ccm5h,tar_on_ccm5v;")
    mad.input("value,tar_on_ccs1h,tar_on_ccs1v,tar_on_ccs5h,tar_on_ccs5v;")
    print("\n\n")


def print_betas(mad):
    print("Printing betas")

    mad.input("value, betx_IP1, betx0_IP1;")
    mad.input("value, bety_IP1, bety0_IP1;")
    mad.input("value, betx_IP5, betx0_IP5;")
    mad.input("value, bety_IP5, bety0_IP5;")

    mad.input("value, betx_IP2, bety_IP2;")
    mad.input("value, betx_IP8, bety_IP8;")

    print("\n\n")


def match_phase_ip15(mad):
    pass


def rematch_tune(mad):
    tartunes = {"b1": 0, "b2": 0}
    for beam in ["b1", "b2"]:
        mad.input(f"use,sequence=lhc{beam};")
        mad.input("match;")
        mad.input(f"global, q1=Qx{beam}, q2=Qy{beam};")
        mad.input(f"vary,   name=kqtf.{beam}, step=1.0E-7;")
        mad.input(f"vary,   name=kqtd.{beam}, step=1.0E-7;")
        mad.input("jacobian,  calls=15, tolerance=1.0E-21;")
        mad.input("endmatch;")
        tartunes[beam] = mad.globals.tar

    mad.input(f'tartuneb1 = {tartunes["b1"]};')
    mad.input(f'tartuneb2 = {tartunes["b2"]};')
    mad.input("tartune=tartuneb1+tartuneb2;")

    mad.input("value, tartuneb1,tartuneb2,tartune;")
    mad.input("value, kqtf.b1,kqtd.b1,kqtf.b2,kqtd.b2;")


def rematch_chroma(mad):
    tarchromas = {"b1": 0, "b2": 0}
    for beam in ["b1", "b2"]:
        mad.input(f"use,sequence=lhc{beam};")
        mad.input("match;")
        mad.input(f"global, dq1=Qpx{beam}, dq2=Qpy{beam};")
        mad.input(f"vary,   name=ksf.{beam}, step=1.0E-7;")
        mad.input(f"vary,   name=ksd.{beam}, step=1.0E-7;")
        mad.input("jacobian,  calls=15, tolerance=1.0E-21;")
        mad.input("endmatch;")
        tarchromas[beam] = mad.globals.tar

    mad.input(f'tarchromab1 = {tarchromas["b1"]};')
    mad.input(f'tarchromab2 = {tarchromas["b2"]};')
    mad.input("tarchroma=tarchromab1+tarchromab2;")

    mad.input("value,tarchromab1,tarchromab2,tarchroma;")
    mad.input("value,ksf.b1,ksd.b1,ksf.b2,ksd.b2;")


def save_optics_hllhc(mad, filename):
    mad.input("exec, check_opt(b1);")
    mad.input("exec, check_opt(b2);")
    mad.input(f'system,"test -f {filename} && rm {filename}";')
    mad.input(f'print,text="printing {filename}";')
    mad.input("set,format=28.16g;")
    mad.input(f"option,-echo, -info ;assign, echo={filename};")
    mad.input("exec,_save_optics_hllhc;")
    mad.input("assign,echo=terminal ;")
    mad.input("option,-echo;")
    mad.input("set,format=24.12g;")


# %%
